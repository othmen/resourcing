<?php

class Resourcing_Form_Model_Competence extends Centurion_Form_Model_Abstract
{
    
    public function __construct($options = array())
    {
        $this->_model = Centurion_Db::getSingleton('resourcing/competence');
        
        $this->_elementLabels = array(
             'competence'         => $this->_translate('Compétence'),
        );
        
        $this->setLegend($this->_translate('Compétences'));        
        
        parent::__construct($options);
       
    }

}
