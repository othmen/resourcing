<?php

class Resourcing_Form_Model_Rate extends Centurion_Form_Model_Abstract
{
    
    public function __construct($options = array())
    {
        $this->_model = Centurion_Db::getSingleton('resourcing/rate');
        
        $this->_elementLabels = array(
             'id_operationnel'          => $this->_translate('Opérationnel'),
             'candidat_id'              => $this->_translate('Candidat'),
             'value'                    => $this->_translate('Valeur'),
        );
        
        $this->setLegend($this->_translate('Votes'));        
        
        parent::__construct($options);
       
    }

}
