<?php

class Resourcing_Form_Model_Operationnel extends Centurion_Form_Model_Abstract
{
    
    public function __construct($options = array())
    {
        $this->_model = Centurion_Db::getSingleton('resourcing/operationnel');
        
        $this->_elementLabels = array(
             'operationnel'         => $this->_translate('Operationnel'),
        );
        
        $this->setLegend($this->_translate('Operationnels'));        
        
        parent::__construct($options);
       
    }

}
