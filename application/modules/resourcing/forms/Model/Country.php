<?php

class Resourcing_Form_Model_Country extends Centurion_Form_Model_Abstract
{
    
    public function __construct($options = array())
    {
        $this->_model = Centurion_Db::getSingleton('resourcing/country');
        
        $this->_elementLabels = array(
             'fr'         => $this->_translate('Pays'),
        );
        
        $this->setLegend($this->_translate('Pays'));        
        
        parent::__construct($options);
       
    }

}
