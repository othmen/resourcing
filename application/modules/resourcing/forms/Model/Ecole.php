<?php

class Resourcing_Form_Model_Ecole extends Centurion_Form_Model_Abstract
{
    
    public function __construct($options = array())
    {
        $this->_model = Centurion_Db::getSingleton('resourcing/ecole');
        
        $this->_elementLabels = array(
             'ecole'         => $this->_translate('Ecole'),
        );
        
        $this->setLegend($this->_translate('Ecoles'));        
        
        parent::__construct($options);
       
    }

}
