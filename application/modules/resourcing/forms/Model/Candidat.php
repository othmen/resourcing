<?php

class Resourcing_Form_Model_Candidat extends Centurion_Form_Model_Abstract {

    const PATH_CV = '/files/cv/';
    const PATH_PHOTO = '/files/photo/';

    public function __construct($options = array()) {
        $this->_model = Centurion_Db::getSingleton('resourcing/candidat');

        $this->_elementLabels = array(
            'civilite' => $this->_translate('Civilité'),
            'nationalite' => $this->_translate('Nationalité'),
            'last_name' => $this->_translate('Nom'),
            'email' => $this->_translate('Email'),
            'first_name' => $this->_translate('Prénom'),
            'tel' => $this->_translate('Téléphone'),
            'tel2' => $this->_translate('Téléphone 2'),
            'residence' => $this->_translate('Résidence'),
            'id_domaine' => $this->_translate('Domaine'),
            'poste' => $this->_translate('Poste'),
            'ecole' => $this->_translate('Ecole'),
            'societe' => $this->_translate('Société actuelle'),
            'societe_precedente' => $this->_translate('Société Précédente'),
            'competence' => $this->_translate('Compétences principales'),
            'competences_secondaires' => $this->_translate('Comp. secondaires'),
            'id_debut_carriere' => $this->_translate('Début carrière'),
            'id_experience' => $this->_translate('Expérience'),
            'statut' => $this->_translate('Statut'),
            'type_candidat' => $this->_translate('Type'),
            'id_etude' => $this->_translate('Etudes'),
            'certifie' => $this->_translate('Certifié'),
            'certification' => $this->_translate('Certification'),
            'id_sourcecv' => $this->_translate('Source CV'),
            'metier' => $this->_translate('Métiers'),
            'technologie' => $this->_translate('Technologies'),
            'cv' => $this->_translate('CV'),
            'photo' => $this->_translate('Photo'),
            'commentaire' => $this->_translate('Commentaires'),
        );

        $this->setLegend($this->_translate('Candidats'));
        
                
        
        parent::__construct($options);
    }
    
    public function init(){
        parent::init();
        
        $cv = new Zend_Form_Element_File('cv');
        $cv->setLabel('CV')
           ->setRequired(false)
           ->setDestination(realpath(PUBLIC_PATH . Resourcing_Form_Model_Candidat::PATH_CV))
           ->clearDecorators()
           ->setDecorators(array('File', array('ViewScript', array('viewScript' => '/decorators/file.phtml', 'placement' => false))))
           ->setAttribs(array('class-tag' => 'col-md-12',
                              'class-label' => 'col-md-12',
                              'class-input' => 'col-md-6'))
           ->addValidator('Size', false, 1024000);
         $cv->addValidator('Extension', true, 'doc,docx,pdf');
        $this->addElement($cv);
        
        $photo = new Zend_Form_Element_File('photo');
        $photo->setLabel('Photo')
           ->setRequired(false)     
           ->setDestination(realpath(PUBLIC_PATH . Resourcing_Form_Model_Candidat::PATH_PHOTO))
           ->clearDecorators()
           ->setDecorators(array('File', array('ViewScript', array('viewScript' => '/decorators/image.phtml', 'placement' => false))))
           ->setAttribs(array('class-tag' => 'col-md-12',
                              'class-label' => 'col-md-12',
                              'class-input' => 'col-md-7'));
        $photo->addValidator('Size', false, 1024000)
              ->addValidator('Extension', true, 'jpeg,jpg,png');
        $this->addElement($photo);
        
//        $hash = new Zend_Form_Element_Hash('_XSRF');
//        $hash->setSalt(get_class($this))->setTimeout(0);
//        $hash->getSession()->setExpirationSeconds(3600);
//        //$hash->setSalt(get_class($this))->setExpirationSeconds(3600);
//        $this->addElement($hash);
        $hash = new Zend_Form_Element_Hash('_XSRF');
        $hash->getSession()->setExpirationSeconds(900000);
        $hash->setSalt(get_class($this))
             ->setRequired(false)
             ->setTimeout(900000)
             ->setTimeout(0)  
             ->setAllowEmpty(true)
                //->setIgnore(true)
                ;
        $this->addElement($hash);
        
        //var_dump($hash);die;
        
        $formId = new Zend_Form_Element_Hidden('formId');
        $formId->setValue($this->getFormId());
        $this->addElement($formId);
        
        
        $this->getElement('email')
             ->setRequired()   
             ->clearDecorators()
             ->addDecorator('ViewScript', array('viewScript' => '/decorators/text.phtml'))
             ->setAttribs(array('data-type' => 'email', 
                                'data-trigger' => 'change', 
                                'class' => 'parsley-validated',
                                'class-tag' => 'col-md-6',
                                'class-label' => 'col-md-5',
                                'class-input' => 'col-md-7'));
        if('new' == Zend_Controller_Front::getInstance()->getRequest()->getActionName()){
             $this->getElement('email')
                  ->addValidator(new Centurion_Form_Model_Validator_AlreadyTaken('resourcing/candidat', 'email'));
        }
        $this->getElement('civilite')
             ->setRequired()   
             ->setAttribs(array('class-tag' => 'form-row col-md-6',
                                'class-label' => 'col-md-5',
                                'class-input' => 'col-md-7'));
        
        $this->getElement('type_candidat')
             ->setRequired()   
             ->setRequired(true)
             ->setAttribs(array('class-tag' => 'col-md-8 form-row float-none',
                                'class-label' => 'col-md-3',
                                'class-input' => 'col-md-7'));
        
        $this->getElement('nationalite')
             ->setRequired()   
             ->setAttribs(array('class-tag' => 'col-md-6',
                                'class-label' => 'col-md-5',
                                'class-input' => 'col-md-7'));
        
        $this->getElement('last_name')
             ->setRequired()   
             ->setAttribs(array('data-trigger' => 'change', 
                                'class' => 'parsley-validated',
                                'class-tag' => 'col-md-6',
                                'class-label' => 'col-md-5',
                                'class-input' => 'col-md-7'));
        
        $this->getElement('first_name')
             ->setRequired()
             ->setAttribs(array('data-trigger' => 'change', 
                                'class' => 'parsley-validated',
                                'class-tag' => 'col-md-6',
                                'class-label' => 'col-md-5',
                                'class-input' => 'col-md-7'));
        
        $this->getElement('tel')
             ->setRequired()
             ->setAttribs(array('data-type' => 'number',
                                'data-minlength' => 8,
                                'data-maxlength' => 15,
                                'class' => 'parsley-validated',
                                'data-trigger' => 'change', 
                                'class' => 'input-mask',
                                'class-tag' => 'col-md-6',
                                'class-label' => 'col-md-5',
                                'class-input' => 'col-md-7'));
        
        $this->getElement('tel2')
             ->setRequired(false)
             ->setAttribs(array('data-type' => 'number',
                                'data-minlength' => 8,
                                'data-maxlength' => 15,
                                'class' => 'parsley-validated',
                                'data-trigger' => 'change', 
                                'class' => 'input-mask',
                                'class-tag' => 'col-md-6',
                                'class-label' => 'col-md-5',
                                'class-input' => 'col-md-7'));
        
        $this->getElement('residence')
             ->setAttribs(array('class-tag' => 'col-md-6',
                                'class-label' => 'col-md-5',
                                'class-input' => 'col-md-7'));
        
        $this->getElement('statut')
             ->setRequired()   
             ->setAttribs(array('class' => 'col-md-9',
                                'class-tag' => 'col-md-12',
                                'class-label' => 'col-md-4',
                                'class-input' => 'col-md-7'));

        $this->getElement('poste')
             ->setRequired(false)   
             ->setAttribs(array('class-tag' => 'col-md-12',
                                'class-label' => 'col-md-5',
                                'class-input' => 'col-md-7'));

        $this->getElement('id_domaine')
             ->setRequired(false)   
             ->setAttribs(array('class' => 'col-md-9',
                                'class-tag' => 'col-md-12',
                                'class-label' => 'col-md-4',
                                'class-input' => 'col-md-7'));

        $this->getElement('societe')
             ->setAttribs(array('class-tag' => 'col-md-12',
                                'class-label' => 'col-md-5',
                                'class-input' => 'col-md-7',
                                'data-required' => true));


        $this->getElement('id_etude')
             ->setRequired(false)   
             ->setAttribs(array('class' => 'col-md-9',
                                'class-tag' => 'col-md-12',
                                'class-label' => 'col-md-4',
                                'class-input' => 'col-md-7'));

        $this->getElement('societe_precedente')
             ->setRequired(false)
             ->setAttribs(array('class-tag' => 'col-md-12',
                                'class-label' => 'col-md-5',
                                'class-input' => 'col-md-7'));

        $this->getElement('ecole')
             ->setRequired(false)   
             ->setAttribs(array('class' => 'col-md-9',
                                'class-tag' => 'col-md-12',
                                'class-label' => 'col-md-4',
                                'class-input' => 'col-md-7 autocomplete-input ui-autocomplete-input',
                                'autocomplete' => 'off'));

        $this->getElement('id_debut_carriere')
             ->setRequired()   
             ->setAttribs(array('class' => 'col-md-9',
                                'class-tag' => 'col-md-12',
                                'class-label' => 'col-md-5',
                                'class-input' => 'col-md-7'));

        $this->getElement('id_experience')
             ->setRequired()   
             ->setAttribs(array('disabled' => 'disabled',
                                'class' => 'col-md-9',
                                'class-tag' => 'col-md-12 float-none',
                                'class-label' => 'col-md-4',
                                'class-input' => 'col-md-7'));

        $this->getElement('certifie')
             ->setRequired()   
             ->setAttribs(array( 'class-tag' => 'form-row col-md-6',
                                 'class-label' => 'col-md-4',
                                 'class-input' => 'col-md-7'))
             ->setValue('Non');

        $this->getElement('certification')
             ->setRequired(false)   
             ->setAttribs(array('class-tag' => 'col-md-6',
                                'class-label' => 'col-md-4',
                                'class-input' => 'col-md-7'));

        $this->getElement('id_sourcecv')
             ->setRequired(false)   
             ->setAttribs(array('class' => 'col-md-9',
                                'class-tag' => 'col-md-12',
                                'class-label' => 'col-md-5',
                                'class-input' => 'col-md-7'));
        
        $this->getElement('metier')
             ->setRequired()   
             ->setAttribs(array('data-placeholder' => 'Métiers',
                                'class' => 'col-md-9',
                                'multiple' => 'multiple',
                                'class-tag' => 'col-md-12',
                                'class-label' => 'col-md-4',
                                'class-input' => 'col-md-7',
                                'style' => 'height: 50px'));
    
        $this->getElement('technologie')
             ->setRequired()   
             ->setAttribs(array('class-tag' => 'col-md-12',
                                'class-label' => 'col-md-2',
                                'class-input' => 'col-md-10'))
             ->setIsArray(true);

        $this->getElement('competence')
             ->setRequired()   
             ->setAttribs(array('class-tag' => 'col-md-12',
                                'class-label' => 'col-md-2',
                                'class-input' => 'col-md-10'))
             ->setIsArray(true);

        $this->getElement('competences_secondaires')
             ->setAttribs(array('class-tag' => 'col-md-12',
                                'class-label' => 'col-md-2',
                                'class-input' => 'col-md-5'));

        
        $this->getElement('commentaire')
             ->setRequired(false)   
             ->setAttribs(array('class-tag' => 'col-md-12 float-none',
                                'class-label' => 'col-md-2',
                                'class-input' => 'col-md-5'));
        
    }
    
}
