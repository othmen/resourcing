<?php

class Resourcing_Form_Model_Operation extends Centurion_Form_Model_Abstract {

    public function __construct($options = array()) {
        $this->_model = Centurion_Db::getSingleton('resourcing/operation');
        $hide = $disabled = '';
        $this->_elementLabels = array(
            'date_entretien'        => $this->_translate('Date entretien'),
            'id_operationnel'       => $this->_translate('Opérationnel'),
            'verdict'               => $this->_translate('Verdict'),
            'points_positifs'       => $this->_translate('Points positifs'),
            'points_negatifs'       => $this->_translate('Points négatifs'),
            'date_decision'         => $this->_translate('Date décision'),
            'commentaire'           => $this->_translate('Commentaire'),
        );

        $this->setLegend($this->_translate('Entretien Opérationnel'));
        
        $hash = new Zend_Form_Element_Hash('_XSRF');
//        $hash->setSalt(get_class($this))
//             ->setTimeout(60000);
        $hash->getSession()->setExpirationSeconds(900000);
        $hash->setSalt(get_class($this))
             ->setRequired(false)
             ->setTimeout(900000)
             ->setTimeout(0)
             ->setAllowEmpty(true);
        $this->addElement($hash);
        
        $formId = new Zend_Form_Element_Hidden('formId');
        $formId->setValue($this->getFormId());
        $this->addElement($formId);
        
        $candidatId = new Zend_Form_Element_Hidden('id_candidat');
        $candidatId->setValue(Zend_Controller_Front::getInstance()->getRequest()->getParam('candidat-id'));
        $this->addElement($candidatId);
        
        parent::__construct($options);
        
        if(Zend_Controller_Front::getInstance()->getRequest()->getActionName() == 'new' 
                || (Zend_Controller_Front::getInstance()->getRequest()->getActionName() == 'get' 
                && Centurion_Auth::getInstance()->getIdentity()->username != 'admin')) {
            $hide = 'hide';
        }  else {
            $disabled = 'disabled';
        }
        $this->getElement('id_operationnel')
             ->setAttribs(array('class-tag' => 'col-md-6 float-none hide',
                                'class-label' => 'col-md-4',
                                'class-input' => 'col-md-7'))
             ->setValue(Centurion_Auth::getInstance()->getIdentity()->id);
        
        $this->getElement('date_entretien')
             ->setRequired()   
             ->setAttribs(array('class-tag' => 'col-md-6 float-none hide',
                                'class-label' => 'col-md-4',
                                'class-input' => 'col-md-7'))
             ->setValue(date('d-m-Y'));

        $this->getElement('verdict')
             ->setRequired()   
             ->setAttribs(array('class-tag' => 'form-row col-md-6 float-none',
                                'class-label' => 'col-md-4',
                                'class-input' => 'col-md-7 smile'))
             ->setValue('Oui');

        $this->getElement('date_decision')
             ->setAttribs(array('class-tag' => 'form-row col-md-6 float-none',
                                'class-label' => 'col-md-4',
                                'class-input' => 'col-md-7'));

        $this->getElement('points_positifs')
             ->setAttribs(array('class-tag' => 'col-md-6 float-none',
                                'class-label' => 'col-md-4',
                                'class-input' => 'col-md-7'));
        
        $this->getElement('points_negatifs')
             ->setAttribs(array('class-tag' => 'col-md-6 float-none',
                                'class-label' => 'col-md-4',
                                'class-input' => 'col-md-7'));
        
        $this->getElement('commentaire')
             ->setAttribs(array('class-tag' => 'col-md-6 float-none',
                                'class-label' => 'col-md-4',
                                'class-input' => 'col-md-7'));
    }
    
}
