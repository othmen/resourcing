<?php

class Resourcing_Form_Model_DebutCarriere extends Centurion_Form_Model_Abstract
{
    
    public function __construct($options = array())
    {
        $this->_model = Centurion_Db::getSingleton('resourcing/debut_carriere');
        
        $this->_elementLabels = array(
             'annee'         => $this->_translate('Début Carrière'),
        );
        
        $this->setLegend($this->_translate('Début Carrière'));        
        
        parent::__construct($options);
       
    }

}
