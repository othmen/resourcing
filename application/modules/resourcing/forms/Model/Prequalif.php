<?php

class Resourcing_Form_Model_Prequalif extends Centurion_Form_Model_Abstract {

    public function __construct($options = array()) {
        $this->_model = Centurion_Db::getSingleton('resourcing/prequalif');

        $this->_elementLabels = array(
            'id_operationnel' => $this->_translate('Opérationnel'),
            'date_entretien' => $this->_translate('Date entretien'),
            'ponctualite' => $this->_translate('Ponctualité'),
            'changer' => $this->_translate('Pourquoi changer?'),
            'recherche' => $this->_translate('Poste recherché'),
            'infos_keyrus' => $this->_translate('Infos Keyrus'),
            'info_societe_actuelle' => $this->_translate('Infos société actuelle'),
            'piste' => $this->_translate('Pistes en cours'),
            'mission_etranger' => $this->_translate('Missions réalisées à l’étranger ?'),
            'countries' => $this->_translate('Où ?'),
            'duree_mission' => $this->_translate('Durée mission'),
            'visa' => $this->_translate('Visa'),
            'date_exp_visa' => $this->_translate('Date Exp VISA'),
            'id_test_technique' => $this->_translate('Test technique'),
            'note_test' => $this->_translate('Note test'),
            'mobilite' => $this->_translate('Mobilité internationale'),
            'disponibilite' => $this->_translate('Disponibilité'),
            'id_type_contrat' => $this->_translate('Type de contrat'),
            'preavis' => $this->_translate('Durée du préavis'),
            'cin' => $this->_translate('CIN'),
            'cin_date_livraison' => $this->_translate('CIN Délivrée le'),
            'salaire_actuel' => $this->_translate('Salaire actuel Fixe'),
            'salaire_variable' => $this->_translate('Variables'),
            'avantages' => $this->_translate('Avantages'),
            'pretention_salariale_min' => $this->_translate('Prétentions Salariales Min'),
            'pretention_salariale_max' => $this->_translate('Prétentions Salariales Max'),
            'niveau_francais' => $this->_translate('Niveau Français'),
            'niveau_anglais' => $this->_translate('Niveau anglais'),
            'adequation' => $this->_translate('Adéquation poste'),
            'fluidite' => $this->_translate('Fluidité du disours'),
            'vocabulaire' => $this->_translate('Vocabulaire/Syntaxe'),
            'aisance' => $this->_translate('Aisance verbale'),
            'ecoute' => $this->_translate('Ecoute'),
            'motivation' => $this->_translate('Motivation à changer de poste'),
            'avis' => $this->_translate('Avis'),
            'commentaire' => $this->_translate('Commentaires'),
        );

        //$disabled = Zend_Controller_Front::getInstance()->getRequest()->getActionName() == 'get' ? 'disabled' : '';

        $this->setLegend($this->_translate('Entretien Préqualif'));

//        $hash = new Zend_Form_Element_Hash('_XSRF');
//        $hash->setSalt(get_class($this))
//                ->setTimeout(60000);
        $hash = new Zend_Form_Element_Hash('_XSRF');
        $hash->getSession()->setExpirationSeconds(900000);
        $hash->setSalt(get_class($this))
             ->setRequired(false)
             ->setTimeout(900000)
             ->setTimeout(0)
             ->setAllowEmpty(true);
        $this->addElement($hash);

        $formId = new Zend_Form_Element_Hidden('formId');
        $formId->setValue($this->getFormId());
        $this->addElement($formId);

        $candidatId = new Zend_Form_Element_Hidden('id_candidat');
        $candidatId->setValue(Zend_Controller_Front::getInstance()->getRequest()->getParam('candidat-id'));
        $this->addElement($candidatId);

        parent::__construct($options);
        
        $this->getElement('id_operationnel')
                ->setAttribs(array('class-tag' => 'col-md-6 float-none hide',
                    'class-label' => 'col-md-4',
                    'class-input' => 'col-md-7'))
                ->setValue(Centurion_Auth::getInstance()->getIdentity()->id);

        $this->getElement('date_entretien')
                ->setRequired()
                ->clearValidators()
                ->setAttribs(array('class-tag' => 'col-md-6 float-none hide',
                    'class-label' => 'col-md-5',
                    'class-input' => 'col-md-7'))
                ->setValue(date('Y-m-d'));

        $this->getElement('changer')
                ->setRequired(false)
                ->setAttribs(array('class-tag' => 'col-md-12',
                    'class-label' => 'col-md-2 mrg20R',
                    'class-input' => 'col-md-9 mrg25L'));

        $this->getElement('ponctualite')
                ->setRequired(false)
                ->setAttribs(array('class-tag' => 'form-row col-md-6 float-none',
                    'class-label' => 'col-md-6',
                    'class-input' => 'col-md-6'))
                ->setValue('Oui');

        $this->getElement('recherche')
                ->setAttribs(array('class-tag' => 'col-md-6',
                    'class-label' => 'col-md-5',
                    'class-input' => 'col-md-7'));

        $this->getElement('mobilite')
                ->setRequired(false)
                ->setAttribs(array('class-tag' => 'form-row col-md-6 float-none',
                    'class-label' => 'col-md-6',
                    'class-input' => 'col-md-6'))
                ->setValue('Non');

        $this->getElement('infos_keyrus')
                ->setAttribs(array('class-tag' => 'col-md-12',
                    'class-label' => 'col-md-2 mrg20R',
                    'class-input' => 'col-md-9 mrg25L'));

        $this->getElement('piste')
                ->setRequired(false)
                ->setAttribs(array('class-tag' => 'col-md-6',
                    'class-label' => 'col-md-5',
                    'class-input' => 'col-md-7'));

        $this->getElement('info_societe_actuelle')
                ->setRequired(false)
                ->setAttribs(array('class-tag' => 'col-md-6',
                    'class-label' => 'col-md-5',
                    'class-input' => 'col-md-7'));

        $this->getElement('disponibilite')
                ->setAttribs(array('class-tag' => 'col-md-6',
                    'class-label' => 'col-md-5',
                    'class-input' => 'col-md-7'));

        $this->getElement('salaire_actuel')
                ->setAttribs(array('data-type' => 'number',
                    'class' => 'parsley-validated',
                    'class-tag' => 'col-md-6',
                    'class-label' => 'col-md-5',
                    'class-input' => 'col-md-7'));

        $this->getElement('salaire_variable')
                ->setRequired(false)
                ->setAttribs(array('class-tag' => 'col-md-6',
                    'class-label' => 'col-md-5',
                    'class-input' => 'col-md-7'));

        $this->getElement('avantages')
                ->setAttribs(array('class-tag' => 'col-md-6',
                    'class-label' => 'col-md-5',
                    'class-input' => 'col-md-7',
                    'style' => 'height: 80px'));

        $this->getElement('pretention_salariale_min')
                ->setAttribs(array('data-type' => 'number',
                    'class' => 'parsley-validated',
                    'class-tag' => 'col-md-6',
                    'class-label' => 'col-md-5',
                    'class-input' => 'col-md-7'))
                ->setValue(0);

        $this->getElement('pretention_salariale_max')
                ->setAttribs(array('data-type' => 'number',
                    'class' => 'parsley-validated',
                    'class-tag' => 'col-md-6',
                    'class-label' => 'col-md-5',
                    'class-input' => 'col-md-7'));

        $this->getElement('id_type_contrat')
                ->setRequired(false)
                ->setAttribs(array('class-tag' => 'col-md-6',
                    'class-label' => 'col-md-5',
                    'class-input' => 'col-md-7'));

        $this->getElement('preavis')
                ->setRequired()
                ->setAttribs(array('class-tag' => 'col-md-6',
                    'class-label' => 'col-md-5',
                    'class-input' => 'col-md-7'));

        $this->getElement('mission_etranger')
                ->setRequired(false)
                ->setAttribs(array('class-tag' => 'form-row col-md-6',
                    'class-label' => 'col-md-6',
                    'class-input' => 'col-md-6'))
                ->setValue('Non');

        $this->getElement('countries')
                ->setRequired(false)
                ->setAttribs(array('class' => 'chosen-select',
                    'autocomplete' => 'autocomplete',
                    'class-tag' => 'col-md-6',
                    'class-label' => 'col-md-5',
                    'class-input' => 'col-md-7',
                    'style' => 'display: none',
                    'multiple' => 'multiple',
                    'data-placeholder' => 'Choisir un pays...'));

        $this->getElement('duree_mission')
                ->setAttribs(array('class-tag' => 'col-md-6 float-none',
                    'class-label' => 'col-md-5',
                    'class-input' => 'col-md-7'));

        $this->getElement('niveau_francais')
                ->setAttribs(array('class-tag' => 'col-md-6',
                    'class-label' => 'col-md-5',
                    'class-input' => 'col-md-7'));

        $this->getElement('niveau_anglais')
                ->setRequired(false)
                ->setAttribs(array('class-tag' => 'col-md-6',
                    'class-label' => 'col-md-5',
                    'class-input' => 'col-md-7'));

        $this->getElement('visa')
                ->setRequired(false)
                ->setAttribs(array('class-tag' => 'form-row col-md-6',
                    'class-label' => 'col-md-6',
                    'class-input' => 'col-md-6'))
                ->setValue('Non');

        $this->getElement('date_exp_visa')
                ->setAttribs(array('class-tag' => 'col-md-6 hide',
                    'class-label' => 'col-md-5',
                    'class-input' => 'col-md-7',
                    'disabled' => 'disabled'));

        $this->getElement('id_test_technique')
                ->setRequired(false)
                ->setAttribs(array('class-tag' => 'col-md-6',
                    'class-label' => 'col-md-5',
                    'class-input' => 'col-md-7'));

        $this->getElement('note_test')
                ->setAttribs(array('data-type' => 'number',
                    'class' => 'parsley-validated',
                    'class-tag' => 'col-md-6',
                    'class-label' => 'col-md-5',
                    'class-input' => 'col-md-7'));

        $this->getElement('cin')
                ->setRequired(false)
                ->setAttribs(array('data-type' => 'number',
                    'class' => 'parsley-validated',
                    'data-minlength' => 8,
                    'class-tag' => 'col-md-6',
                    'class-label' => 'col-md-5',
                    'class-input' => 'col-md-7'));

        $this->getElement('cin_date_livraison')
                ->setRequired(false)
                ->setAttribs(array('class-tag' => 'col-md-6',
                    'class-label' => 'col-md-5',
                    'class-input' => 'col-md-7',));

        $this->getElement('adequation')
                ->setRequired(false)
                ->setAttribs(array('class-tag' => 'form-row col-md-6 float-none',
                    'class-label' => 'col-md-6',
                    'class-input' => 'col-md-6'))
                ->setValue('Oui');

        $this->getElement('fluidite')
                ->setAttribs(array('class-tag' => 'col-md-6',
                    'class-label' => 'col-md-5',
                    'class-input' => 'col-md-7'));

        $this->getElement('vocabulaire')
                ->setAttribs(array('class-tag' => 'col-md-6',
                    'class-label' => 'col-md-5',
                    'class-input' => 'col-md-7'));

        $this->getElement('aisance')
                ->setAttribs(array('class-tag' => 'col-md-6',
                    'class-label' => 'col-md-5',
                    'class-input' => 'col-md-7'));

        $this->getElement('ecoute')
                ->setAttribs(array('class-tag' => 'col-md-6',
                    'class-label' => 'col-md-5',
                    'class-input' => 'col-md-7'));

        $this->getElement('motivation')
                ->setAttribs(array('class-tag' => 'col-md-6',
                    'class-label' => 'col-md-5',
                    'class-input' => 'col-md-7'));

        $this->getElement('avis')
                ->setRequired()
                ->setAttribs(array('class-tag' => 'form-row col-md-6 float-none',
                    'class-label' => 'col-md-6',
                    'class-input' => 'col-md-6 smile'));

        $this->getElement('commentaire')
                ->setAttribs(array('class-tag' => 'col-md-6',
                    'class-label' => 'col-md-5',
                    'class-input' => 'col-md-7'));
    }

}
