<?php

class Resourcing_Form_Model_Holidays extends Centurion_Form_Model_Abstract
{
    
    public function __construct($options = array())
    {
        $this->_model = Centurion_Db::getSingleton('resourcing/holidays');
        
        $this->_elementLabels = array(
             'title'         => $this->_translate('Titre'),
             'date'         => $this->_translate('Date'),
        );
        
        $this->setLegend($this->_translate('Jours fériés'));        
        
        parent::__construct($options);
       
    }
    
}
