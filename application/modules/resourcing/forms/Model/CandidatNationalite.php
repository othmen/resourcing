<?php

class Resourcing_Form_Model_CandidatNatonalite extends Centurion_Form_Model_Abstract
{
    
    public function __construct($options = array())
    {
        $this->_model = Centurion_Db::getSingleton('resourcing/candidatNationalite');
        
        $this->_elementLabels = array(
             'nationalite'         => $this->_translate('Nationalité'),
        );
        
        $this->setLegend($this->_translate('Nationalités Candidat'));        
        
        parent::__construct($options);
       
    }

}
