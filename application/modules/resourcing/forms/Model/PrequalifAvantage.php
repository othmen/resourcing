<?php

class Resourcing_Form_Model_PrequalifAvantage extends Centurion_Form_Model_Abstract
{
    
    public function __construct($options = array())
    {
        $this->_model = Centurion_Db::getSingleton('resourcing/prequalifAvantage');
        
        $this->_elementLabels = array(
             'avantage'         => $this->_translate('Avantage'),
        );
        
        
        $this->setLegend($this->_translate('Avantages'));        
        
        parent::__construct($options);
       
    }

}
