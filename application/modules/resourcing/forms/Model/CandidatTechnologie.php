<?php

class Resourcing_Form_Model_CandidatTechnology extends Centurion_Form_Model_Abstract
{
    
    public function __construct($options = array())
    {
        $this->_model = Centurion_Db::getSingleton('resourcing/candidatTechnology');
        
        $this->_elementLabels = array(
             'technologie'         => $this->_translate('Technologie'),
        );
        
        $this->setLegend($this->_translate('Technologies Candidats'));        
        
        parent::__construct($options);
       
    }

}
