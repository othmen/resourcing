<?php

class Resourcing_Form_Model_Technologie extends Centurion_Form_Model_Abstract
{
    
    public function __construct($options = array())
    {
        $this->_model = Centurion_Db::getSingleton('resourcing/technologie');
        
        $this->_elementLabels = array(
             'technologie'         => $this->_translate('Technologie'),
        );
        
        $this->setLegend($this->_translate('Technologie'));        
        
        parent::__construct($options);
       
    }

}
