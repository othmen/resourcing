<?php

class Resourcing_Form_Model_Domaine extends Centurion_Form_Model_Abstract
{
    
    public function __construct($options = array())
    {
        $this->_model = Centurion_Db::getSingleton('resourcing/domaine');
        
        $this->_elementLabels = array(
             'domaine'         => $this->_translate('Domaine'),
        );
        
        $this->setLegend($this->_translate('Domaines'));        
        
        parent::__construct($options);
       
    }

}
