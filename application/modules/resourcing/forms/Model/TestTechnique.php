<?php

class Resourcing_Form_Model_TestTechnique extends Centurion_Form_Model_Abstract
{
    
    public function __construct($options = array())
    {
        $this->_model = Centurion_Db::getSingleton('resourcing/test_technique');
        
        $this->_elementLabels = array(
             'test_technique'         => $this->_translate('Test Technique'),
        );
        
        $this->setLegend($this->_translate('Tests Techniques'));        
        
        parent::__construct($options);
       
    }

}
