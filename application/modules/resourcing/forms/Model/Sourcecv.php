<?php

class Resourcing_Form_Model_Sourcecv extends Centurion_Form_Model_Abstract
{
    
    public function __construct($options = array())
    {
        $this->_model = Centurion_Db::getSingleton('resourcing/sourcecv');
        
        $this->_elementLabels = array(
             'sourcecv'         => $this->_translate('Source CV'),
        );
        
        $this->setLegend($this->_translate('Source CV'));        
        
        parent::__construct($options);
       
    }

}
