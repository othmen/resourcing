<?php

class Resourcing_Form_Model_Etudes extends Centurion_Form_Model_Abstract
{
    
    public function __construct($options = array())
    {
        $this->_model = Centurion_Db::getSingleton('resourcing/etudes');
        
        $this->_elementLabels = array(
             'etude'         => $this->_translate('Etude'),
        );
        
        $this->setLegend($this->_translate('Etudes'));        
        
        parent::__construct($options);
       
    }

}
