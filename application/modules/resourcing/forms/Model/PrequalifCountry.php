<?php

class Resourcing_Form_Model_PrequalifCountry extends Centurion_Form_Model_Abstract
{
    
    public function __construct($options = array())
    {
        $this->_model = Centurion_Db::getSingleton('resourcing/prequalifCountry');
        
        $this->_elementLabels = array(
             'country'         => $this->_translate('Pays'),
        );
        
        $this->setLegend($this->_translate('Emplacement mission'));        
        
        parent::__construct($options);
       
    }

}
