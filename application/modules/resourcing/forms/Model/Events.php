<?php

class Resourcing_Form_Model_Events extends Centurion_Form_Model_Abstract {

    public function __construct($options = array()) {
        $this->_model = Centurion_Db::getSingleton('resourcing/events');

        $this->_elementLabels = array(
            'id_user'    => $this->_translate('Identifiant'),
            'event'      => $this->_translate('Nom de l\'événement'),
            'start'      => $this->_translate('Date de début'),
            'end'        => $this->_translate('Date de fin'),
        );
    }
        
}
