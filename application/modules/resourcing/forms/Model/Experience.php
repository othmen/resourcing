<?php

class Resourcing_Form_Model_Experience extends Centurion_Form_Model_Abstract
{
    
    public function __construct($options = array())
    {
        $this->_model = Centurion_Db::getSingleton('resourcing/experience');
        
        $this->_elementLabels = array(
             'min'         => $this->_translate('De'),
             'max'         => $this->_translate('à'),
             'experience'         => $this->_translate('Expérience'),
        );
        
        $this->setLegend($this->_translate('Experiences'));        
        
        parent::__construct($options);
       
    }

}
