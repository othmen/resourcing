<?php

class Resourcing_Form_Model_Metier extends Centurion_Form_Model_Abstract
{
    
    public function __construct($options = array())
    {
        $this->_model = Centurion_Db::getSingleton('resourcing/metier');
        
        $this->_elementLabels = array(
             'metier'         => $this->_translate('Métier'),
        );
        
        $this->setLegend($this->_translate('Metiers'));        
        
        parent::__construct($options);
       
    }

}
