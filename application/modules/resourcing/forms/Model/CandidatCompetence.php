<?php

class Resourcing_Form_Model_CandidatCompetence extends Centurion_Form_Model_Abstract
{
    
    public function __construct($options = array())
    {
        $this->_model = Centurion_Db::getSingleton('resourcing/candidatCompetence');
        
        $this->_elementLabels = array(
             'competence'         => $this->_translate('Compétence'),
        );
        
        
        $this->setLegend($this->_translate('Compétences Candidats'));        
        
        parent::__construct($options);
       
    }

}
