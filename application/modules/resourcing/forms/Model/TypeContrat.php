<?php

class Resourcing_Form_Model_TypeContrat extends Centurion_Form_Model_Abstract
{
    
    public function __construct($options = array())
    {
        $this->_model = Centurion_Db::getSingleton('resourcing/type_contrat');
        
        $this->_elementLabels = array(
             'type_contrat'         => $this->_translate('Type Contrat'),
        );
        
        $this->setLegend($this->_translate('Types Contrats'));        
        
        parent::__construct($options);
       
    }

}
