<?php

class Resourcing_AdminSourcecvController extends Centurion_Controller_CRUD
{

    public function preDispatch()
    {
        $this->_helper->authCheck();
        $this->_helper->aclCheck();
        $this->_helper->layout->setLayout('admin');
        parent::preDispatch();
    }
    
    public function init()
    {
        $this->_formClassName = 'Resourcing_Form_Model_Sourcecv';
        $this->_displays = array(
            'sourcecv'        => $this->view->translate('Sources CV'),
        );

        $this->view->placeholder('headling_1_content')->set($this->view->translate('Sources CV'));
        $this->view->placeholder('headling_1_add_buttonadd')->set($this->view->translate('Sources CV'));
        $this->_filters = array(
                'sourcecv' => array(
                    'behavior' => self::FILTER_BEHAVIOR_CALLBACK,
                    'callback' => array($this, 'filterSourcecvName'),
                    'label' => $this->view->translate('Source CV'),
                ),
            );
        parent::init();
        
    }
    
   public function filterSourcecvName($value, &$sqlFilter)
    {
        $sqlFilter[] = new Zend_Db_Expr(
            Centurion_Db_Table_Abstract::getDefaultAdapter()
            ->quoteInto('sourcecv like ? COLLATE utf8_general_ci', '%' . $value . '%'));
    }
  
  
}

