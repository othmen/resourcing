<?php

class Resourcing_AdminTestTechniqueController extends Centurion_Controller_CRUD
{

    public function preDispatch()
    {
        $this->_helper->authCheck();
        $this->_helper->aclCheck();
        $this->_helper->layout->setLayout('admin');
        parent::preDispatch();
    }
    
    public function init()
    {
        $this->_formClassName = 'Resourcing_Form_Model_TestTechnique';
        $this->_displays = array(
            'test_technique'        => $this->view->translate('Tests Techniques'),
        );

        $this->view->placeholder('headling_1_content')->set($this->view->translate('Test Technique'));
        $this->view->placeholder('headling_1_add_buttonadd')->set($this->view->translate('Tests Techniques'));
        $this->_filters = array(
                'test_technique' => array(
                    'behavior' => self::FILTER_BEHAVIOR_CALLBACK,
                    'callback' => array($this, 'filterTestTechniqueName'),
                    'label' => $this->view->translate('Tests Techniques'),
                ),
            );
        parent::init();
        
    }
    
   public function filterTestTechniqueName($value, &$sqlFilter)
    {
        $sqlFilter[] = new Zend_Db_Expr(
            Centurion_Db_Table_Abstract::getDefaultAdapter()
            ->quoteInto('test_technique like ? COLLATE utf8_general_ci', '%' . $value . '%'));
    }
  
  
}

