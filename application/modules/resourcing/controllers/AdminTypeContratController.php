<?php

class Resourcing_AdminTypeContratController extends Centurion_Controller_CRUD
{

    public function preDispatch()
    {
        $this->_helper->authCheck();
        $this->_helper->aclCheck();
        $this->_helper->layout->setLayout('admin');
        parent::preDispatch();
    }
    
    public function init()
    {
        $this->_formClassName = 'Resourcing_Form_Model_TypeContrat';
        $this->_displays = array(
            'type_contrat'        => $this->view->translate('Type Contrat'),
        );

        $this->view->placeholder('headling_1_content')->set($this->view->translate('Type Contrat'));
        $this->view->placeholder('headling_1_add_buttonadd')->set($this->view->translate('Types Contrats'));
        $this->_filters = array(
                'type_contrat' => array(
                    'behavior' => self::FILTER_BEHAVIOR_CALLBACK,
                    'callback' => array($this, 'filterTypeContratName'),
                    'label' => $this->view->translate('Types Contrats'),
                ),
            );
        parent::init();
        
    }
    
   public function filterTypeContratName($value, &$sqlFilter)
    {
        $sqlFilter[] = new Zend_Db_Expr(
            Centurion_Db_Table_Abstract::getDefaultAdapter()
            ->quoteInto('type_contrat like ? COLLATE utf8_general_ci', '%' . $value . '%'));
    }
  
  
}

