<?php

class Resourcing_AdminHolidaysController extends Centurion_Controller_CRUD
{

    public function preDispatch()
    {
        $this->_helper->authCheck();
        $this->_helper->aclCheck();
        $this->_helper->layout->setLayout('admin');
        parent::preDispatch();
    }
    
    public function init()
    {
        $this->_formClassName = 'Resourcing_Form_Model_Holidays';
        $this->_displays = array(
            'title'        => $this->view->translate('Titre'),
            'date'        => $this->view->translate('Date'),
            'desc'        => $this->view->translate('Desc'),
        );

        $this->view->placeholder('headling_1_content')->set($this->view->translate('Jours fériés'));
        $this->view->placeholder('headling_1_add_buttonadd')->set($this->view->translate('Jours fériés'));
        $this->_filters = array(
                'title' => array(
                    'behavior' => self::FILTER_BEHAVIOR_CALLBACK,
                    'callback' => array($this, 'filterHolidaysName'),
                    'label' => $this->view->translate('Titre'),
                ),
            );
        parent::init();
        
    }
    
   public function filterHolidaysName($value, &$sqlFilter)
    {
        $sqlFilter[] = new Zend_Db_Expr(
            Centurion_Db_Table_Abstract::getDefaultAdapter()
            ->quoteInto('title like ? COLLATE utf8_general_ci', '%' . $value . '%'));
    }
  
  
}

