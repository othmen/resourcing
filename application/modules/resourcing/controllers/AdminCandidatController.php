<?php

class Resourcing_AdminCandidatController extends Centurion_Controller_CRUD {

    public function preDispatch() {
        $this->_helper->authCheck();
        $this->_helper->aclCheck();
        $this->_helper->layout->setLayout('admin');
        parent::preDispatch();
    }

    public function init() {
        $this->_formClassName = 'Resourcing_Form_Model_Candidat';

        $this->_displays = array(
            'type_candidat' => $this->view->translate('Type'),
            'civilite' => $this->view->translate('Civilité'),
            'last_name' => $this->view->translate('Nom'),
            'first_name' => $this->view->translate('Prénom'),
            'email' => $this->view->translate('Email'),
            'poste' => $this->view->translate('Poste'),
            'societe' => $this->view->translate('Société'),
            'societe_precedente' => $this->view->translate('Société précédente'),
            'statut' => $this->view->translate('Statut'),
        );

        $experiences = Centurion_Db::getSingleton('resourcing/experience');
        foreach ($experiences->fetchAll() as $experience) {
            $experiencesArray[$experience->pk] = $experience->experience;
        }

        $technologies = Centurion_Db::getSingleton('resourcing/technologie');
        foreach ($technologies->fetchAll() as $technologie) {
            $technologiesArray[$technologie->pk] = $technologie->technologie;
        }
        
        //print '<pre>';print_r($dateEntretiensArray);die;
        $this->_filters = array(
            'last_name' => array(
                'behavior' => self::FILTER_BEHAVIOR_CALLBACK,
                'callback' => array($this, 'filterCandidatName'),
                'label' => $this->view->translate('Nom'),
            ),
//            'Candidat' => array(
//                'type' => self::FILTER_TYPE_BETWEEN_DATE,
//                'behavior' => self::FILTER_BEHAVIOR_BETWEEN,
//                //'data' => $dateEntretiensArray,
//                'label' => $this->view->translate('Dernier entretien'),
//            ),
            'technologie' => array(
                'type' => self::FILTER_TYPE_CHECKBOX,
                'data' => $technologiesArray,
                'behavior' => self::FILTER_BEHAVIOR_IN,
                //'callback' => array($this, 'filterCandidatName'),
                'label' => $this->view->translate('Technologie'),
            ),
//            'id_experience' => array(
//                'type' => self::FILTER_TYPE_SELECT,
//                'behavior' => self::FILTER_BEHAVIOR_IN,
//                'data' => $experiencesArray,
//                'label' => $this->view->translate('Experience'),
//            ),
        );

        parent::init();
    }

    public function filterCandidatName($value, &$sqlFilter) {
        $sqlFilter[] = new Zend_Db_Expr(
                Centurion_Db_Table_Abstract::getDefaultAdapter()
                        ->quoteInto('last_name like ? OR first_name like ? COLLATE utf8_general_ci', '%' . $value . '%', '%' . $value . '%'));
    }

}
