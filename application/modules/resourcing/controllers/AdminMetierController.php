<?php

class Resourcing_AdminMetierController extends Centurion_Controller_CRUD
{

    public function preDispatch()
    {
        $this->_helper->authCheck();
        $this->_helper->aclCheck();
        $this->_helper->layout->setLayout('admin');
        parent::preDispatch();
    }
    
    public function init()
    {
        $this->_formClassName = 'Resourcing_Form_Model_Metier';
        $this->_displays = array(
            'metier'        => $this->view->translate('Métiers'),
        );

        $this->view->placeholder('headling_1_content')->set($this->view->translate('Métiers'));
        $this->view->placeholder('headling_1_add_buttonadd')->set($this->view->translate('Métier'));
        $this->_filters = array(
                'metier' => array(
                    'behavior' => self::FILTER_BEHAVIOR_CALLBACK,
                    'callback' => array($this, 'filterMetierName'),
                    'label' => $this->view->translate('Métier'),
                ),
            );
        parent::init();
        
    }
    
   public function filterEcoleName($value, &$sqlFilter)
    {
        $sqlFilter[] = new Zend_Db_Expr(
            Centurion_Db_Table_Abstract::getDefaultAdapter()
            ->quoteInto('metier like ? COLLATE utf8_general_ci', '%' . $value . '%'));
    }
  
  
}

