<?php

class Resourcing_UsersController extends Centurion_Controller_CRUD
{
    public function preDispatch()
       {
           $this->_helper->authCheck();
           $this->_helper->aclCheck();
           $this->_helper->layout->setLayout('admin');
           parent::preDispatch();
       }

    public function init()
    {
        $this->_formClassName = 'Resourcing_Form_Model_User';

        $this->_displays = array(
            'username'      =>  array(
                                    'type'  => self::COL_TYPE_FIRSTCOL,
                                    'label' => $this->view->translate('Username'),
                                    'param' => array(
                                                    'title' => 'username',
                                                    'cover' => null,
                                                    'subtitle' => null,
                                                ),
                                ),
            'created_at'    =>  array('filters' => self::COL_DISPLAY_DATE, 'label' => $this->view->translate('Created at')),
            'last_login'    =>  array('filters' => self::COL_DISPLAY_DATE, 'label' => $this->view->translate('Last login')),
        );

        $this->_toolbarActions['Activate'] = $this->view->translate('Activate');
        $this->_toolbarActions['Desactivate'] = $this->view->translate('Desactivate');

        $this->view->placeholder('headling_1_content')->set($this->view->translate('Manage users'));

        parent::init();
    }

    public function getSelectFiltred()
    {
        $sql = parent::getSelectFiltred();
        $sql->where('id = ?', Centurion_Auth::getInstance()->getIdentity()->id);
        return $sql;
    }
    
    public function activateAction($rowset = null)
    {
        if (null===$rowset) {
            return;
        }

        foreach ($rowset as $key => $row) {
            $row->is_active = 1;
            $row->save();
        }

        $this->_cleanCache();
        $this->getHelper('redirector')->gotoRoute(array_merge(array(
            'controller' => $this->_request->getControllerName(),
            'module'     => $this->_request->getModuleName(),
            'action'         => 'index'
        ), $this->_extraParam), null, true);
    }

    public function desactivateAction($rowset = null)
    {
        if (null===$rowset) {
            return;
        }

        foreach ($rowset as $key => $row) {
            $row->is_active = 0;
            $row->save();
        }

        $this->_cleanCache();
        $this->getHelper('redirector')->gotoRoute(array_merge(array(
            'controller' => $this->_request->getControllerName(),
            'module'     => $this->_request->getModuleName(),
            'action'         => 'index'
        ), $this->_extraParam), null, true);
    }

    public function deleteAction($rowset = null)
    {
        if ($this->_getParam('id', 0) === Centurion_Auth::getInstance()->getIdentity()->id) {
            Zend_Controller_Action_HelperBroker::getStaticHelper('redirector')
                ->gotoSimple('unauthorized', 'error', 'admin');
        } else {
            parent::deleteAction($rowset);
        }
    }
    
    public function putAction() {
        $listGroups = Centurion_Auth::getInstance()->getIdentity()->groups->toArray();
        foreach($listGroups as $key => $value){
            $groups[] = $value['id'];
        }
        sort($groups);
        if ($this->_getParam('id', 0) !== Centurion_Auth::getInstance()->getIdentity()->id && !in_array(1, $groups)) {
            Zend_Controller_Action_HelperBroker::getStaticHelper('redirector')
                ->gotoSimple('unauthorized', 'error', 'admin');
        } else {
            parent::putAction();
        }
    }
    
    public function getAction() {
        $listGroups = Centurion_Auth::getInstance()->getIdentity()->groups->toArray();
        foreach($listGroups as $key => $value){
            $groups[] = $value['id'];
        }
        sort($groups);
        if ($this->_getParam('id', 0) !== Centurion_Auth::getInstance()->getIdentity()->id && !in_array(1, $groups)) {
            Zend_Controller_Action_HelperBroker::getStaticHelper('redirector')
                ->gotoSimple('unauthorized', 'error', 'admin');
        } else {
            parent::getAction();
        }
    }
}

