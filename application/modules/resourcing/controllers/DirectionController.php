<?php

class Resourcing_DirectionController extends Centurion_Controller_CRUD
{

    public function preDispatch()
    {
        $this->_helper->authCheck();
        $this->_helper->aclCheck();
//        $this->_helper->layout->setLayout('default');
//        parent::preDispatch();
    }
    
    public function init()
    {
        $this->_formClassName = 'Resourcing_Form_Model_Direction';
        $this->_displays = array(
            'date_entretien'        => $this->view->translate('Date entretien'),
            'id_operationnel'       => $this->view->translate('Opérationnel'),
            'verdict'               => $this->view->translate('Verdict'),
            'motifs_refus'           => $this->view->translate('Motif refus'),
            'date_decision'         => $this->view->translate('Date décision'),
            'salaire_propose'       => $this->view->translate('Piste en cours'),
            'commentaire'           => $this->view->translate('Commentaire'),
        );
        
        $candidat = Centurion_Db::getSingleton('resourcing/candidat')
                                ->findBy('id', intval($this->_request->getParam('candidat-id')))
                                ->current();
        $this->view->candidat = $candidat;
        
        $entretiens = Centurion_Db::getSingleton('resourcing/direction')
                ->findBy('id_candidat', intval($this->_request->getParam('candidat-id')));
        $this->view->entretiens = $entretiens;
        if ($this->_request->getParam('q')) {
            $this->view->destination = $this->_request->getParam('q');
        } elseif ($this->_request->getParam('saving') == 'done') {
            if(empty($entretiens))
                $this->view->destination = 'prequalif';
            else
                $this->view->destination = 'direction';
        } else {
            $this->view->destination = 'direction';
        }
        
        parent::init();
        
    }
    
    public function postAction()
    {
        $this->_extraParam = array('candidat-id' => $this->_request->getParam('id_candidat'));
        parent::postAction();
    }
    
    public function putAction()
    {
        //print_r($this->_request->getParam('id_candidat'));die;
        $this->_extraParam = array('candidat-id' => $this->_request->getParam('id_candidat'));
        parent::putAction();

    }
    
}

