<?php

class Resourcing_RateController extends Centurion_Controller_CRUD
{

    public function preDispatch()
    {
        $this->_helper->authCheck();
        $this->_helper->aclCheck();
        $this->_helper->layout->setLayout('admin');
        parent::preDispatch();
    }
    
    public function init()
    {
        $this->_formClassName = 'Resourcing_Form_Model_Rate';
        $this->_displays = array(
            'id_operationnel'       => $this->view->translate('Operationnel'),
            'candidat_id'           => $this->view->translate('Candidat'),
            'value'                 => $this->view->translate('Valeur'),
        );

        $this->view->placeholder('headling_1_content')->set($this->view->translate('Vote'));
        $this->view->placeholder('headling_1_add_buttonadd')->set($this->view->translate('Votes'));
        
        parent::init();
        
    }
}

