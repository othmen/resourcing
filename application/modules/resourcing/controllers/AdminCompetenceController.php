<?php

class Resourcing_AdminCompetenceController extends Centurion_Controller_CRUD
{

    public function preDispatch()
    {
        $this->_helper->authCheck();
        $this->_helper->aclCheck();
        $this->_helper->layout->setLayout('admin');
        parent::preDispatch();
    }
    
    public function init()
    {
        $this->_formClassName = 'Resourcing_Form_Model_Competence';
        $this->_displays = array(
            'competence'        => $this->view->translate('Compétences'),
        );

        $this->view->placeholder('headling_1_content')->set($this->view->translate('Compétence'));
        $this->view->placeholder('headling_1_add_buttonadd')->set($this->view->translate('Compétences'));
        $this->_filters = array(
                'competence' => array(
                    'behavior' => self::FILTER_BEHAVIOR_CALLBACK,
                    'callback' => array($this, 'filterCompetenceName'),
                    'label' => $this->view->translate('Compétences'),
                ),
            );
        parent::init();
        
    }
    
   public function filterCompetenceName($value, &$sqlFilter)
    {
        $sqlFilter[] = new Zend_Db_Expr(
            Centurion_Db_Table_Abstract::getDefaultAdapter()
            ->quoteInto('competence like ? COLLATE utf8_general_ci', '%' . $value . '%'));
    }
  
  
}

