<?php

class Resourcing_AdminOperationnelController extends Centurion_Controller_CRUD
{

    public function preDispatch()
    {
        $this->_helper->authCheck();
        $this->_helper->aclCheck();
        $this->_helper->layout->setLayout('admin');
        parent::preDispatch();
    }
    
    public function init()
    {
        $this->_formClassName = 'Resourcing_Form_Model_Operationnel';
        $this->_displays = array(
            'operationnel'        => $this->view->translate('Operationnels'),
        );

        $this->view->placeholder('headling_1_content')->set($this->view->translate('Operationnel'));
        $this->view->placeholder('headling_1_add_buttonadd')->set($this->view->translate('Operationnels'));
        $this->_filters = array(
                'operationnel' => array(
                    'behavior' => self::FILTER_BEHAVIOR_CALLBACK,
                    'callback' => array($this, 'filterOperationnelName'),
                    'label' => $this->view->translate('Operationnel'),
                ),
            );
        parent::init();
        
    }
    
   public function filterOperationnelName($value, &$sqlFilter)
    {
        $sqlFilter[] = new Zend_Db_Expr(
            Centurion_Db_Table_Abstract::getDefaultAdapter()
            ->quoteInto('operationnel like ? COLLATE utf8_general_ci', '%' . $value . '%'));
    }
  
  
}

