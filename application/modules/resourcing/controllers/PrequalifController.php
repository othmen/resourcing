<?php

class Resourcing_PrequalifController extends Centurion_Controller_CRUD {

    public function preDispatch() {
        $this->_helper->authCheck();
        $this->_helper->aclCheck();
//        $this->_helper->layout->setLayout('default');
//        parent::preDispatch();
    }

    public function init() {
        $this->_formClassName = 'Resourcing_Form_Model_Prequalif';
        $this->_displays = array(
            'id_operationnel' => $this->view->translate('Opérationnel'),
            'date_entretien' => $this->view->translate('Date entretien'),
            'ponctualite' => $this->view->translate('Ponctualité'),
            'changer' => $this->view->translate('Pourquoi changer?'),
            'recherche' => $this->view->translate('Poste recherché'),
            'infos_keyrus' => $this->view->translate('Infos Keyrus'),
            'info_societe_actuelle' => $this->view->translate('Infos société actuelle'),
            'piste' => $this->view->translate('Piste en cours'),
            'mission_etranger' => $this->view->translate('Missions réalisées à l’étranger ?'),
            'id_mission_country' => $this->view->translate('Où ?'),
            'duree_mission' => $this->view->translate('Durée mission'),
            'visa' => $this->view->translate('Visa'),
            'date_exp_visa' => $this->view->translate('Date Exp VISA'),
            'id_test_technique' => $this->view->translate('Test technique'),
            'note_test' => $this->view->translate('Note test'),
            'mobilite' => $this->view->translate('Mobilité internationale'),
            'disponibilite' => $this->view->translate('Disponibilité'),
            'id_type_contrat' => $this->view->translate('Type de contrat'),
            'preavis' => $this->view->translate('Durée du préavis'),
            'cin' => $this->view->translate('CIN'),
            'cin_date_livraison' => $this->view->translate('CIN Délivrée le'),
            'salaire_actuel' => $this->view->translate('Salaire actuel Fixe'),
            'salaire_variable' => $this->view->translate('Variables'),
            'avantages' => $this->view->translate('Avantages'),
            'pretention_salariale_min' => $this->view->translate('Prétentions Salariales Min'),
            'pretention_salariale_max' => $this->view->translate('Prétentions Salariales Max'),
            'niveau_francais' => $this->view->translate('Niveau Français'),
            'niveau_anglais' => $this->view->translate('Niveau anglais'),
            'adequation' => $this->view->translate('Adéquation poste'),
            'fluidite' => $this->view->translate('Fluidité du disours'),
            'vocabulaire' => $this->view->translate('Vocabulaire/Syntaxe'),
            'aisance' => $this->view->translate('Aisance verbale'),
            'ecoute' => $this->view->translate('Ecoute'),
            'motivation' => $this->view->translate('Motivation à changer de poste'),
            'avis' => $this->view->translate('Avis'),
            'commentaire' => $this->view->translate('Commentaires'),
        );

        $candidat = Centurion_Db::getSingleton('resourcing/candidat')
                ->findBy('id', intval($this->_request->getParam('candidat-id')))
                ->current();
        $this->view->candidat = $candidat;
        $entretiens = Centurion_Db::getSingleton('resourcing/prequalif')
                ->findBy('id_candidat', intval($this->_request->getParam('candidat-id')));
        $this->view->entretiens = $entretiens;
        
        if ($this->_request->getParam('q')) {
            $this->view->destination = $this->_request->getParam('q');
        } else {
            $this->view->destination = 'prequalif';
        }
        
        parent::init();
    }

    public function postAction() {
        $this->_extraParam = array('candidat-id' => $this->_request->getParam('id_candidat'));
        parent::postAction();
    }

    public function putAction() {
        $this->_extraParam = array('candidat-id' => $this->_request->getParam('id_candidat'));
        parent::putAction();
    }

}
