<?php

class Resourcing_AdminExperienceController extends Centurion_Controller_CRUD
{

    public function preDispatch()
    {
        $this->_helper->authCheck();
        $this->_helper->aclCheck();
        $this->_helper->layout->setLayout('admin');
        parent::preDispatch();
    }
    
    public function init()
    {
        $this->_formClassName = 'Resourcing_Form_Model_Experience';
        $this->_displays = array(
            'min'        => $this->view->translate('De'),
            'max'        => $this->view->translate('à'),
            'experience'        => $this->view->translate('Expériences'),
        );

        $this->view->placeholder('headling_1_content')->set($this->view->translate('Expériences'));
        $this->view->placeholder('headling_1_add_buttonadd')->set($this->view->translate('Expériences'));
        $this->_filters = array(
                'experience' => array(
                    'behavior' => self::FILTER_BEHAVIOR_CALLBACK,
                    'callback' => array($this, 'filterEcoleName'),
                    'label' => $this->view->translate('Expériences'),
                ),
            );
        parent::init();
        
    }
    
   public function filterEcoleName($value, &$sqlFilter)
    {
        $sqlFilter[] = new Zend_Db_Expr(
            Centurion_Db_Table_Abstract::getDefaultAdapter()
            ->quoteInto('experience like ? COLLATE utf8_general_ci', '%' . $value . '%'));
    }
  
  
}

