<?php

class Resourcing_AdminEcoleController extends Centurion_Controller_CRUD
{

    public function preDispatch()
    {
        $this->_helper->authCheck();
        $this->_helper->aclCheck();
        $this->_helper->layout->setLayout('admin');
        parent::preDispatch();
    }
    
    public function init()
    {
        $this->_formClassName = 'Resourcing_Form_Model_Ecole';
        $this->_displays = array(
            'ecole'        => $this->view->translate('Ecoles'),
        );

        $this->view->placeholder('headling_1_content')->set($this->view->translate('Ecole'));
        $this->view->placeholder('headling_1_add_buttonadd')->set($this->view->translate('Ecole'));
        $this->_filters = array(
                'ecole' => array(
                    'behavior' => self::FILTER_BEHAVIOR_CALLBACK,
                    'callback' => array($this, 'filterEcoleName'),
                    'label' => $this->view->translate('Ecole'),
                ),
            );
        parent::init();
        
    }
    
   public function filterEcoleName($value, &$sqlFilter)
    {
        $sqlFilter[] = new Zend_Db_Expr(
            Centurion_Db_Table_Abstract::getDefaultAdapter()
            ->quoteInto('ecole like ? COLLATE utf8_general_ci', '%' . $value . '%'));
    }
  
  
}

