<?php

class Resourcing_AdminCountryController extends Centurion_Controller_CRUD
{

    public function preDispatch()
    {
        $this->_helper->authCheck();
        $this->_helper->aclCheck();
        $this->_helper->layout->setLayout('admin');
        parent::preDispatch();
    }
    
    public function init()
    {
        $this->_formClassName = 'Resourcing_Form_Model_Country';
        $this->_displays = array(
            'fr'        => $this->view->translate('Pays'),
        );

        $this->view->placeholder('headling_1_content')->set($this->view->translate('Pays'));
        $this->view->placeholder('headling_1_add_buttonadd')->set($this->view->translate('Pays'));
        $this->_filters = array(
                'fr' => array(
                    'behavior' => self::FILTER_BEHAVIOR_CALLBACK,
                    'callback' => array($this, 'filterPaysName'),
                    'label' => $this->view->translate('Pays'),
                ),
            );
        parent::init();
        
    }
    
   public function filterPaysName($value, &$sqlFilter)
    {
        $sqlFilter[] = new Zend_Db_Expr(
            Centurion_Db_Table_Abstract::getDefaultAdapter()
            ->quoteInto('fr like ? COLLATE utf8_general_ci', '%' . $value . '%'));
    }
  
  
}

