<?php

class Resourcing_DashboardController extends Centurion_Controller_CRUD {

    public function preDispatch() {
        $this->_helper->authCheck();
        $this->_helper->aclCheck();
//        $this->_helper->layout->setLayout('default');
//        parent::preDispatch();
    }

    public function init() {
        $this->_formClassName = 'Resourcing_Form_Model_Dashboard';
        $this->_displays = array(
            'id_user' => $this->view->translate('Identifiant'),
            'event' => $this->view->translate('Nom de l\'événement'),
            'start' => $this->view->translate('Date de début'),
            'end' => $this->view->translate('Poste recherché'),
            'infos_keyrus' => $this->view->translate('Date de fin'),
        );

        parent::init();
    }
    
    public function indexAction($param) {
        
    }

//    public function postAction() {
//        $this->_extraParam = array('candidat-id' => $this->_request->getParam('id_candidat'));
//        parent::postAction();
//    }
//
//    public function putAction() {
//        //print '<pre>';print_r($this->_request->getParams());die;
//        $this->_extraParam = array('candidat-id' => $this->_request->getParam('id_candidat'));
//        parent::putAction();
//    }

}
