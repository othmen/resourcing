<?php

class Resourcing_CandidatController extends Centurion_Controller_CRUD {

    const PATH_IMPORT = '/files/import/';
    const PREQUALIF_RESPONSABLE_ID = 7;

    public function preDispatch() {
        if($this->_request->getControllerName() == 'candidat' && $this->_request->getActionName() != 'lucene-index'){
            $this->_helper->authCheck();
            $this->_helper->aclCheck();
        }
    }

    public function init() {
        $this->_formClassName = 'Resourcing_Form_Model_Candidat';
        $this->_displays = array(
            'id' => $this->view->translate('Id'),
            'last_name' => array('label' => $this->view->translate('Candidat'),
                'type' => self::COLS_CALLBACK,
                'callback' => array($this, 'getFullName')
            ),
            'poste' => $this->view->translate('Poste'),
            'Experience' => $this->view->translate('Expérience'),
            'societe' => $this->view->translate('Entreprise'),
            'likes' => array('label' => $this->view->translate('Liked'),
                'type' => self::COLS_CALLBACK,
                'callback' => array($this, '_getCountLikes')),
            'prequalif' => array('label' => $this->view->translate('Pré-Qual'),
                'type' => self::COLS_CALLBACK,
                'callback' => array($this, '_getPrequalifResult')),
            'operation' => array('label' => $this->view->translate('E.Op'),
                'type' => self::COLS_CALLBACK,
                'callback' => array($this, '_getOperationnelResult')),
            'direction' => array('label' => $this->view->translate('E.Dir'),
                'type' => self::COLS_CALLBACK,
                'callback' => array($this, '_getDirectionResult')),
            'salaire' => array('label' => $this->view->translate('Salaire'),
                'type' => self::COLS_CALLBACK,
                'callback' => array($this, '_getSalaireCandidat')),
            'photo' => $this->view->translate('Photo'),
        );

        $this->_showCheckbox = false;
        $this->_defaultOrder = 'id DESC';
        $this->_itemPerPage = 20;

        $this->_order = array('id', 'last_name', 'first_name', 'poste', 'Experience', 'societe');


        $candidat = Centurion_Db::getSingleton('resourcing/candidat')
                ->findBy('id', intval($this->_request->getParam('candidat-id')))
                ->current();
        $this->view->candidat = $candidat;
        if (!in_array($this->_request->getActionName(), array('detail', 'index'))) {
            $controller = $this->_request->getParam('q') ? $this->_request->getParam('q') : 'prequalif';
            $candidatId = $this->_request->getParam('candidat-id');
            $id = isset($candidatId) ? $candidatId : $this->_request->getParam('id');
            $entretiens = Centurion_Db::getSingleton('resourcing/' . $controller)
                    ->findBy('id_candidat', intval($id));
            $this->view->entretiens = $entretiens;
            $this->view->destination = $controller;
        }
        
        $this->_filters = array(
            'last_name' => array(
                'behavior' => self::FILTER_BEHAVIOR_CALLBACK,
                'callback' => array($this, 'filterCandidatName'),
                'label' => $this->view->translate('Nom'),
            ),
//            'Candidat' => array(
//                'type' => self::FILTER_TYPE_BETWEEN_DATE,
//                'behavior' => self::FILTER_BEHAVIOR_BETWEEN,
//                //'data' => $dateEntretiensArray,
//                'label' => $this->view->translate('Dernier entretien'),
//            ),
            'technologie' => array(
                'type' => self::FILTER_TYPE_CHECKBOX,
                'data' => $technologiesArray,
                'behavior' => self::FILTER_BEHAVIOR_IN,
                //'callback' => array($this, 'filterCandidatName'),
                'label' => $this->view->translate('Technologie'),
            ),
//            'id_experience' => array(
//                'type' => self::FILTER_TYPE_SELECT,
//                'behavior' => self::FILTER_BEHAVIOR_IN,
//                'data' => $experiencesArray,
//                'label' => $this->view->translate('Experience'),
//            ),
        );

        
        parent::init();
        $this->_rowActions['Consulter'] = array(
            'url' => array(array('candidat-id' => '___id___', 'controller' => 'candidat', 'action' => 'detail')),
            'cls' => 'glyph-icon icon-eye',
            'clsurl' => 'btn medium bg-gray-alt'
        );
        unset($this->_rowActions['Edit']);
        $this->_rowActions['Delete']['cls'] = 'glyph-icon icon-clock-os';
        $this->_rowActions['Delete']['clsurl'] = 'btn medium bg-gray-alt';
    }
    
    protected function _processValues($values)
    {
        if($this->_request->getActionName() != 'put') {
            unset($values['_XSRF']);
        }
        if ($this->getRequest()->isPost()){
            if ($this->_getForm()->isValid($values)) {
                $this->_preSave();
                $object = $this->_getForm()->save();
                $this->_postSave();

                $this->_cleanCache();
                $url = null;

                if ($this->_hasParam('_next', false))
                    $url = urldecode($this->_getParam('_next', null));
                else if (isset($values['_save']) || isset($values['_saveBig']))
                    $params = array('module'     => $this->_request->getModuleName(),
                                    'controller' => $this->_request->getControllerName(),
                                    'action'     => 'index',
                                    'id'         => null,
                    );
                else if (isset($values['_addanother']))
                    $params = array('module'     => $this->_request->getModuleName(),
                                    'controller' => $this->_request->getControllerName(),
                                    'action'     => 'new',
                                    'id'         => null,
                    );
                else if (isset($values['_continueto']))
                    $url = $this->_request->getParam('_continueurl', null);
                else
                    $params = array('module'     => $this->_request->getModuleName(),
                                    'controller' => $this->_request->getControllerName(),
                                    'action'     => 'get',
                                    'candidat-id'=> $object->id,
                                    'id'         => $object->id,
                                    'q'          => $this->_request->getParam ('q'),
                                    'saving'     => 'done');

                if (null === $url) {
                    $params['saving'] = 'done';

                    $url = $this->_helper->url->url(array_merge($this->_extraParam, $params), null, true);
                }

                if (!$this->getRequest()->isXmlHttpRequest()) {
                    if (method_exists($this->_getForm(), 'getInstance')) {
                        $instance = $this->_getForm()->getInstance();
                        $url = str_replace(array('___pk___', '___model___'), array($instance->id, get_class($instance->getTable())), $url);
                    }

                    return $this->_response->setRedirect($url);
                }
            } else {
                if (null != ($element = $this->_getForm()->getElement('_XSRF')) && $element->hasErrors()) {
    //                print '<pre>';print_r($element->getName());
    //                print '<pre>';print_r($element->getValue());
    //                print '<pre>';print_r($element->getErrors());die;
                    $this->view->errors[] = $this->view->translate('Form hasn\'t been save. Maybe you have waiting to much time. Try again.');
                } else {
                    $this->view->errors[] = $this->view->translate('An error occur when validating the form. See below.');
                }
                if ($this->_request->getActionName() == 'put' && $this->_request->getControllerName() == 'candidat') {
                    $files = new Zend_Session_Namespace('files');
                    //On edit form, we lost cv and photo element
                    //we get them from session
                    $candidatModel = new Resourcing_Model_DbTable_Candidat();
                    $candidatId = $this->_request->getParam('id');
                    $where = $candidatModel->getAdapter()->quoteInto('id = ?', $candidatId);
                    $candidatObject = $candidatModel->fetchRow($where);
                    $update = false;
                    $candidatCv = $candidatObject->cv;
                    $candidatPhoto = $candidatObject->photo;

                    if (empty($candidatObject->cv) && !empty($files->cv)) {
                        $candidatCv = $files->cv;
                        $update = true;
                    }
                    if (empty($candidatObject->photo) && !empty($files->photo)) {
                        $candidatPhoto = $files->photo;
                        $update = true;
                    }
                    if ($update) {
                        $candidatModel->getAdapter()->update('candidat', array('cv' => $candidatCv, 'photo' => $candidatPhoto), 'id = ' . $candidatObject->id);
                    }
                }
            }
        }

        $this->view->form = $this->_getForm();
        
        $script = substr($this->view->selectScript(array(sprintf('%s/form.phtml', $this->_request->getControllerName()),
                                         'grid/form.phtml')), 0, -6);

        $this->render($script, true, true);
    }

    protected function _postSave() {
        $files = new Zend_Session_Namespace('files');
        //On edit form, we lost cv and photo element
        //we get them from session
        $candidatModel = new Resourcing_Model_DbTable_Candidat();
        $idObj = $candidatModel->fetchRow(null, 'id DESC');
        $candidatObject = $candidatModel->fetchRow('id = ' . $idObj->id);
        $candidatId = $this->_request->getParam('id');
        if (isset($candidatId) && !empty($candidatId)) {
            $where = $candidatModel->getAdapter()->quoteInto('id = ?', $candidatId);
            $candidatObject = $candidatModel->fetchRow($where);
        }
        $update = false;
        $candidatCv = $candidatObject->cv;
        $candidatPhoto = $candidatObject->photo;

        if (empty($candidatObject->cv) && !empty($files->cv)) {
            $candidatCv = $files->cv;
            $update = true;
        }
        if (empty($candidatObject->photo) && !empty($files->photo)) {
            $candidatPhoto = $files->photo;
            $update = true;
        }
        if ($update) {
            $candidatModel->getAdapter()->update('candidat', array('cv' => $candidatCv, 'photo' => $candidatPhoto), 'id = ' . $candidatObject->id);
        }
//        $files->unsetAll();
//        Zend_Session::namespaceUnset('files');
        $serverUrl = new Zend_View_Helper_ServerUrl( );
        $url = $serverUrl->serverUrl();
        $object = $this->_getForm();
        $ecoleCandidat = $object->getElement('ecole')->getValue();
        if (!empty($ecoleCandidat)) {
            $ecolesObject = new Resourcing_Model_DbTable_Ecole();
            $where = $ecolesObject->getAdapter()->quoteInto('ecole = ?', $object->getElement('ecole')->getValue());
            $ecoleExist = $ecolesObject->fetchRow($where);
            if (!$ecoleExist) {
                $ecolesObject->insert(array('ecole' => $ecoleCandidat));
            }
        }

        $file = $object->getElement('cv')->getValue();
        //Convert CV files to pdf
        /*if (is_file(realpath(PUBLIC_PATH . Resourcing_Form_Model_Candidat::PATH_CV . $file))) {
            $ext = pathinfo(realpath(PUBLIC_PATH . Resourcing_Form_Model_Candidat::PATH_CV . $file), PATHINFO_EXTENSION);
            $file_name = basename(realpath(PUBLIC_PATH . Resourcing_Form_Model_Candidat::PATH_CV . $file), '.' . $ext);
            if (!is_file($file_name . '.pdf')) {
                if (in_array($ext, array('doc', 'docx'))) {
                    $doc = new Zend_Service_LiveDocx_MailMerge();
                    $doc->setUsername('othmen')->setPassword('Keyrus2014');
                    $doc->setLocalTemplate($url . Resourcing_Form_Model_Candidat::PATH_CV . $file);
                    $doc->assign('dummyFieldName', 'dummyFieldValue');
                    $doc->createDocument();
                    $document = $doc->retrieveDocument('pdf');
                    file_put_contents(PUBLIC_PATH . Resourcing_Form_Model_Candidat::PATH_CV . $file_name . '.pdf', $document);
                }
            }
        }*/
        parent::_postSave();
    }

    public function detailAction() {

        $result = Centurion_Db::getSingleton('resourcing/candidat')
                ->findBy('id', intval($this->_request->getParam('candidat-id')))
                ->current();
        $this->view->dataCandidat = $result;
        $prequalifObject = new Resourcing_Model_DbTable_Prequalif();
        $wherePreq = $prequalifObject->getDefaultAdapter()
                            ->quoteInto('id_candidat = ? ', $this->_request->getParam('candidat-id'));
        $prequalifs = $prequalifObject->fetchAll($wherePreq, 'id DESC');
        $this->view->prequalifs = $prequalifs->toArray();
        
        $operationObject = new Resourcing_Model_DbTable_Operation();
        $whereOper = $operationObject->getDefaultAdapter()
                            ->quoteInto('id_candidat = ? ', $this->_request->getParam('candidat-id'));
        $operationnels = $operationObject->fetchAll($whereOper, 'id DESC');
        $this->view->operationnels = $operationnels->toArray();
        
        
        $directionObject = new Resourcing_Model_DbTable_Direction();
        $whereDir = $directionObject->getDefaultAdapter()
                            ->quoteInto('id_candidat = ? ', $this->_request->getParam('candidat-id'));
        $directionnels = $directionObject->fetchAll($whereDir, 'id DESC');
        $this->view->directionnels = $directionnels->toArray();
        
        $rateObject = new Resourcing_Model_DbTable_Rate();
        $where = $rateObject->getDefaultAdapter()
                            ->quoteInto('candidat_id = ? ', $this->_request->getParam('candidat-id'));
        $whereLike = $rateObject->getDefaultAdapter()
                            ->quoteInto('candidat_id = ' . $this->_request->getParam('candidat-id') . ' AND value = 1');
        $whereRated = $rateObject->getDefaultAdapter()
                            ->quoteInto('candidat_id = ' . $this->_request->getParam('candidat-id') .
                                    ' AND id_operationnel = ' . Centurion_Auth::getInstance()->getIdentity()->id);
        
        $rate = $rateObject->fetchAll($where);
        $rateLike = $rateObject->fetchAll($whereLike);
        $isRated = $rateObject->fetchRow($whereRated);
        $this->view->rate = $rate;
        $this->view->rateLike = $rateLike;
        $this->view->isRated = $isRated;
        
        $operationnel = new Auth_Model_DbTable_User();
        $this->view->listOperationnel = $operationnel->all();
    }
    
    public function votesAction() {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        
        $id_candidat = $this->_request->getParam('candidat_id');
        $id_operationnel = $this->_request->getParam('operationnel');
        $value = ($this->_request->getParam('action') == 'vote_up') ? 1 : 0;
        $ratesObject = new Resourcing_Model_DbTable_Rate();
        $where = $ratesObject->getDefaultAdapter()
                            ->quoteInto('candidat_id = ' . $id_candidat . ' AND id_operationnel = ' . $id_operationnel);
        $rate = $ratesObject->fetchRow($where);
        if(!isset($rate->id) || empty($rate->id)) {
            $data = array('id_operationnel' => $id_operationnel,
                          'candidat_id' => $id_candidat,
                          'value' => $value);
            $insert = $ratesObject->insert($data);
            
            $whereRate = $ratesObject->getDefaultAdapter()
                            ->quoteInto('candidat_id = ? ', $id_candidat);
            $whereRateLike = $ratesObject->getDefaultAdapter()
                            ->quoteInto('candidat_id = ' . $id_candidat . ' AND value = 1');
            $rate = $ratesObject->fetchAll($whereRate);
            $rateLike = $ratesObject->fetchAll($whereRateLike);
            $message = 'Votes: ';
            count($rateLike) > 0 ? $message .= ' +' . count($rateLike) : ''; 
            count($rate) > 0 ? $message .= '(from ' .count($rate) . ' votes)' : '0';
        }
        
        if($insert){
            echo json_encode ($message);
        }
        else 
            echo false;
        
    }
    
    protected function _renderDetailForm($form) {
        $this->view->form = $form;
        $this->view->formViewScript[] = 'grid/_form.phtml';

        $this->_formViewScript = 'candidat/detail.phtml';
        $this->_preRenderForm();
        $script = substr($this->view->selectScript(array($this->_formViewScript, 'grid/form.phtml')), 0, -6);
        //$script = substr($this->view->selectScript(array(sprintf('%s/form.phtml', $this->_request->getControllerName()), 'grid/form.phtml')), 0, -6);
        $this->render($script, true, true);
    }

    public function getSelectFiltred() {
        $sql = parent::getSelectFiltred();
        $sql->order(' id ' . Zend_Db_Select::SQL_DESC);

        return $this->_select;
    }

    public function newAction() {
        $ecolesObject = new Resourcing_Model_DbTable_Ecole();
        $ecoles = $ecolesObject->all();
        $this->view->ecoles = $ecoles;

        parent::newAction();
    }

    public function getAction() {
        $result = Centurion_Db::getSingleton('resourcing/candidat')
                ->findBy('id', intval($this->_request->getParam('id')))
                ->current();
        if (isset($result->photo) && !empty($result->photo) && is_file(PUBLIC_PATH . Resourcing_Form_Model_Candidat::PATH_PHOTO . $result->photo)) {
            $this->view->candidatPhoto = Resourcing_Form_Model_Candidat::PATH_PHOTO . $result->photo;
        }
        if (Zend_Session::namespaceIsset('files')) {
            Zend_Session::namespaceUnset('files');
        }
        Zend_Session::start();
        $files = new Zend_Session_Namespace('files');
        $files->cv = $result->cv;
        $files->photo = $result->photo;
        $ecolesObject = new Resourcing_Model_DbTable_Ecole();
        $ecoles = $ecolesObject->all();
        $this->view->ecoles = $ecoles;

        parent::getAction();
    }

    public function getFullName($object) {
        return (string) $object;
    }

    public function _getPrequalifResult($object) {
        $prequalif = Centurion_Db::getSingleton('resourcing/prequalif')
                ->findBy('id_candidat', intval($object->id));

        $count = count($prequalif);
        if ($count > 0) {
            $data = $prequalif->toArray();
            $max = max(array_keys($data));
            if ($prequalif[$max]->avis == 'Favorable') {
                return '<i class="glyph-icon font-green icon-check-square"></i>';
            } elseif ($prequalif[$max]->avis == 'Défavorable') {
                return '<i class="glyph-icon font-red icon-clock-os-circle"></i>';
            } elseif ($prequalif[$max]->avis == 'Neutre') {
                return '<i class="glyph-icon font-orange icon-minus-square"></i>';
            }
        }
        return '<i class="glyph-icon font-gray icon-square-o"></i>';
    }

    public function _getOperationnelResult($object) {
        $operation = Centurion_Db::getSingleton('resourcing/operation')
                ->findBy('id_candidat', intval($object->id));

        $count = count($operation);
        if ($count > 0) {
            $data = $operation->toArray();
            $max = max(array_keys($data));
            if ($operation[$max]->verdict == 'Oui') {
                return '<i class="glyph-icon font-green icon-check-square"></i>';
            } elseif ($operation[$max]->verdict == 'Non') {
                return '<i class="glyph-icon font-red icon-clock-os-circle"></i>';
            } elseif ($operation[$max]->verdict == 'Neutre') {
                return '<i class="glyph-icon font-orange icon-minus-square"></i>';
            }
        }
        return '<i class="glyph-icon font-gray icon-square-o"></i>';
    }

    public function _getDirectionResult($object) {
        $direction = Centurion_Db::getSingleton('resourcing/direction')
                ->findBy('id_candidat', intval($object->id));

        $count = count($direction);
        if ($count > 0) {
            $data = $direction->toArray();
            $max = max(array_keys($data));
            if ($direction[$max]->verdict == 'Oui') {
                return '<i class="glyph-icon font-green icon-check-square"></i>';
            } elseif ($direction[$max]->verdict == 'Non') {
                return '<i class="glyph-icon font-red icon-clock-os-circle"></i>';
            } elseif ($direction[$max]->verdict == 'Neutre') {
                return '<i class="glyph-icon font-orange icon-minus-square"></i>';
            }
        }
        return '<i class="glyph-icon font-gray icon-square-o"></i>';
    }

    public function _getSalaireCandidat($object) {
        $salaire = Centurion_Db::getSingleton('resourcing/prequalif')
                ->findBy('id_candidat', intval($object->id));
        $count = count($salaire);
        if ($count > 0) {
            $data = $salaire->toArray();
            $max = max(array_keys($data));
            return $salaire[$max]->pretention_salariale_min;
        }
        return 'test';
    }
    
    public function _getCountLikes($object) {
        
        $whereLike = Centurion_Db::getSingleton('resourcing/rate')
                    ->getDefaultAdapter()
                    ->quoteInto('candidat_id = ' . intval($object->id) . ' AND value = 1');
        $likes = Centurion_Db::getSingleton('resourcing/rate')
                ->fetchAll($whereLike)
                ->count();
        $returnLike = '';
        if($likes > 0){
            $returnLike = '+' . $likes . '<i class="glyph-icon font-green icon-thumbs-o-up"></i>';
        }
        
        return compact('returnLike');
    }
    
    public function filterCandidatName($value, &$sqlFilter) {
        $sqlFilter[] = new Zend_Db_Expr(
                Centurion_Db_Table_Abstract::getDefaultAdapter()
                        ->quoteInto('last_name like ? OR first_name like ? COLLATE utf8_general_ci', '%' . $value . '%', '%' . $value . '%'));
    }

    public function filterDateEntretienCandidat($value, &$sqlFilter) {
        $dateEntretiensArray = array();

        $date_gt = !empty($value['date_entretien']['gt']) ? $value['date_entretien']['gt'] : '1990-01-01';
        $date_lt = !empty($value['date_entretien']['lt']) ? $value['date_entretien']['lt'] : '2050-12-31';
        if (new DateTime($date_gt) > new DateTime($date_lt)) {
            $tmp = $date_gt;
            $date_gt = $date_lt;
            $date_lt = $tmp;
        }

        $dateEntretiensPrequalif = Centurion_Db::getSingleton('resourcing/prequalif')
                ->fetchAll('date_entretien BETWEEN "' . $date_gt . '" AND "' . $date_lt . '"');
        foreach ($dateEntretiensPrequalif as $dateEntretienPrequalif) {
            $dateEntretiensArray[$dateEntretienPrequalif->id_candidat] = $dateEntretienPrequalif->date_entretien;
        }
        $idsCandidats = implode(', ', array_reverse(array_keys($dateEntretiensArray)));
        $idsCandidats = !empty($idsCandidats) ? $idsCandidats : -1;
        if (!empty($value['date_entretien']['gt']) && !empty($value['date_entretien']['lt'])) {
            $sqlFilter[] = new Zend_Db_Expr(
                    Centurion_Db_Table_Abstract::getDefaultAdapter()
                            ->quoteInto('`candidat`.id IN (' . $idsCandidats . ')', ''));
        }
    }

    public function filterSalaireCandidat($value, &$sqlFilter) {
        //print '<pre>';print_r($value);die;
        $salaireArray = array();

        $salaire_gt = !empty($value['salaire']['gt']) ? $value['salaire']['gt'] : '0';
        $salaire_lt = !empty($value['salaire']['lt']) ? $value['salaire']['lt'] : '10000000';
        if ($salaire_gt > $salaire_lt) {
            $tmp = $salaire_gt;
            $salaire_gt = $salaire_lt;
            $salaire_lt = $tmp;
        }

        $salairesCandidat = Centurion_Db::getSingleton('resourcing/prequalif')
                ->fetchAll('pretention_salariale BETWEEN "' . $salaire_gt . '" AND "' . $salaire_lt . '"');
        foreach ($salairesCandidat as $salaireCandidat) {
            if (!empty($salaireCandidat->id_candidat)) {
                $salaireArray[] = $salaireCandidat->id_candidat;
            }
        }
        $idsCandidats = implode(', ', array_unique($salaireArray));
        $idsCandidats = !empty($idsCandidats) ? $idsCandidats : -1;
        if (!empty($value['salaire']['gt']) && !empty($value['salaire']['lt'])) {
            $sqlFilter[] = new Zend_Db_Expr(
                    Centurion_Db_Table_Abstract::getDefaultAdapter()
                            ->quoteInto('`candidat`.id IN (' . $idsCandidats . ')', ''));
        }
    }

    public function filterCandidatMobilite($value, &$sqlFilter) {
        if (isset($value) && !empty($value)) {
            $mobiliteArray = Centurion_Db::getSingleton('resourcing/prequalif')
                    ->fetchAll('mobilite = "' . $value . '"');
            foreach ($mobiliteArray as $mobilite) {
                if (!empty($mobilite->id_candidat)) {
                    $candidatMobilite[] = $mobilite->id_candidat;
                }
            }
            $idsCandidatsMobilite = (is_array($candidatMobilite) && !empty($candidatMobilite)) ? implode(', ', array_unique($candidatMobilite)) : '';
            $sqlFilter[] = new Zend_Db_Expr(
                    Centurion_Db_Table_Abstract::getDefaultAdapter()
                            ->quoteInto('`candidat`.id IN (' . $idsCandidatsMobilite . ')', ''));
        }
    }

    public function filterCandidatDisponibilite($value, &$sqlFilter) {
        if (isset($value) && !empty($value)) {
            $where = Centurion_Db::getSingleton('resourcing/prequalif')
                    ->getDefaultAdapter()
                    ->quoteInto('disponibilite like ? COLLATE utf8_general_ci', '%' . $value . '%');
            $disponibiliteArray = Centurion_Db::getSingleton('resourcing/prequalif')
                    ->fetchAll($where);
            foreach ($disponibiliteArray as $disponibilite) {
                if (!empty($disponibilite->id_candidat)) {
                    $candidatDisponibilite[] = $disponibilite->id_candidat;
                }
            }
            $idsCandidatsDisponibilite = (is_array($candidatDisponibilite) && !empty($candidatDisponibilite)) ? implode(', ', array_unique($candidatDisponibilite)) : '';
            $sqlFilter[] = new Zend_Db_Expr(
                    Centurion_Db_Table_Abstract::getDefaultAdapter()
                            ->quoteInto('`candidat`.id IN (' . $idsCandidatsDisponibilite . ')', ''));
        }
    }
    
    /**
     * Action to create index
     */
    /*public function luceneIndexAction() {
        set_time_limit(0);
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        include APPLICATION_PATH . '/../library/App/Pdfparser/vendor/autoload.php';
        $indexPath = APPLICATION_PATH . '/../data/indexes';
        //$indexPathBd = APPLICATION_PATH . '/../data/indexes/bd'; //to search only from database index

        $index = Zend_Search_Lucene::create($indexPath);
        //$indexBd = Zend_Search_Lucene::create($indexPathBd);
        
        $index->setMaxBufferedDocs(200);
        //$indexBd->setMaxBufferedDocs(100);
        
        $candidatsModel = new Resourcing_Model_DbTable_Candidat();

        $doc = new Zend_Search_Lucene_Document();
        foreach ($candidatsModel->all() as $candidats) {
            $doc->addField(Zend_Search_Lucene_Field::Keyword('id_candidat', $candidats->id));
            $doc->addField(Zend_Search_Lucene_Field::Text('last_name', $candidats->last_name));
            $doc->addField(Zend_Search_Lucene_Field::Text('first_name', $candidats->first_name));
            $doc->addField(Zend_Search_Lucene_Field::Text('type_candidat', $candidats->type_candidat));
            $doc->addField(Zend_Search_Lucene_Field::Keyword('id_experience', $candidats->id_experience));
            $doc->addField(Zend_Search_Lucene_Field::Keyword('id_etude', $candidats->id_etude));
            $doc->addField(Zend_Search_Lucene_Field::Text('poste', htmlentities($candidats->poste)));
            $doc->addField(Zend_Search_Lucene_Field::Text('experience', $candidats->Experience->experience));
            $doc->addField(Zend_Search_Lucene_Field::unStored('etude', $candidats->Etudes->etude));
            $doc->addField(Zend_Search_Lucene_Field::Text('societe', htmlentities($candidats->societe)));
            $doc->addField(Zend_Search_Lucene_Field::unStored('comment', $candidats->commentaire));
            $doc->addField(Zend_Search_Lucene_Field::unStored('cv', $candidats->cv));
            $doc->addField(Zend_Search_Lucene_Field::Text('photo', $candidats->photo));

            $technologies = $candidats->technologie->toArray();
            $liste_technos = '';
            if (!empty($technologies)) {
                foreach ($technologies as $technologie) {
                    $liste_technos .= $technologie['technologie'] . ' ';
                }
            }
            $doc->addField(Zend_Search_Lucene_Field::Text('technologie', $liste_technos));

            $prequalif = Centurion_Db::getSingleton('resourcing/prequalif')
                    ->fetchAll('id_candidat = ' . intval($candidats->id), 'id DESC')
                    ->current();
            $operation = Centurion_Db::getSingleton('resourcing/operation')
                    ->fetchAll('id_candidat = ' . intval($candidats->id), 'id DESC')
                    ->current();
            $direction = Centurion_Db::getSingleton('resourcing/direction')
                    ->fetchAll('id_candidat = ' . intval($candidats->id), 'id DESC')
                    ->current();

            $date_entretien = array('prequalif' => $prequalif->date_entretien,
                'operation' => $operation->date_entretien,
                'direction' => $direction->date_entretien
            );
            sort($date_entretien);

            $date_entretien = str_replace('-', '', $date_entretien[0]);
            $doc->addField(Zend_Search_Lucene_Field::Keyword('salaire', (int) trim($prequalif->pretention_salariale)));
            $doc->addField(Zend_Search_Lucene_Field::Keyword('date_entretien', $date_entretien));
            $doc->addField(Zend_Search_Lucene_Field::Text('prequalif_result', $prequalif->avis));
            $doc->addField(Zend_Search_Lucene_Field::Keyword('prequalif_recommandation', $prequalif->recommandation_id));
            $doc->addField(Zend_Search_Lucene_Field::Text('operation_result', $operation->verdict));
            $doc->addField(Zend_Search_Lucene_Field::Keyword('operation_recommandation', $operation->recommandation_id));
            $doc->addField(Zend_Search_Lucene_Field::Text('direction_result', $direction->verdict));
            $doc->addField(Zend_Search_Lucene_Field::Keyword('direction_recommandation', $direction->recommandation_id));

            //$indexBd->addDocument($doc);
            //$indexBd->optimize();
            //Indexation des cv au format pdf
            $original_cv = $candidats->cv;
            $file_name = realpath(PUBLIC_PATH . Resourcing_Form_Model_Candidat::PATH_CV . $original_cv);
            if (is_file($file_name)) {
                chmod($file_name, 0777);
                $ext = pathinfo($file_name, PATHINFO_EXTENSION);
                if ('pdf' == $ext) {
                    $parser = new \Smalot\PdfParser\Parser();
                    $pdf = $parser->parseFile($file_name);

                    // Retrieve all pages from the pdf file.
                    $pages = $pdf->getPages();

                    $cpt = 1;
                    // Loop over each page to extract text.
                    foreach ($pages as $page) {
			$doc->addField(Zend_Search_Lucene_Field::unStored('pdf_text_' . $cpt, $page->getText(), 'UTF-8'));
			$cpt++;
                    }

                    // Retrieve all details from the pdf file.
                    $details = $pdf->getDetails();

                    // Loop over each property to extract values (string or array).
                    foreach ($details as $property => $value) {
                        if (is_array($value)) {
                            $value = implode(', ', $value);
                        }
                        $doc->addField(Zend_Search_Lucene_Field::unIndexed($property, $value));
                        //echo $property . ' => ' . $value . "\n";
                    }
                    //$indexPdf = App_Search_Lucene_Index_Pdfs::index($file_name, $index);
                } elseif (in_array($ext, array('docx'))) {
                    $indexDoc = Zend_Search_Lucene_Document_Docx::loadDocxFile($file_name);
                    $index->addDocument($indexDoc);
                } elseif (in_array($ext, array('ppt', 'ptx'))) {
                    $indexPpt = Zend_Search_Lucene_Document_Pptx::loadPptxFile($file_name);
                    $index->addDocument($indexPpt);
                }
            }
            $index->addDocument($doc);
        }
        //$index->commit();
        $index->optimize();
        //print_r($index->ge);
        //$indexBd->optimize();
    }*/

    /*public function searchAction() {

        $this->_layout = 'search_grid';
        $indexPath = APPLICATION_PATH . '/../data/indexes';
        //$indexPathBd = APPLICATION_PATH . '/../data/indexes/bd';
        //$index = Zend_Search_Lucene::create($indexPath);
        $index = Zend_Search_Lucene::open($indexPath);
        //$indexBd = Zend_Search_Lucene::open($indexPathBd);
		
        $technologiesObject = new Resourcing_Model_DbTable_Technologie();
        $technologies = $technologiesObject->all();
        $this->view->technologies = $technologies;

        $etudesObject = new Resourcing_Model_DbTable_Etudes();
        $etudes = $etudesObject->all();
        $this->view->etudes = $etudes;

        $experiencesObject = new Resourcing_Model_DbTable_Experience();
        $experiences = $experiencesObject->all();
        $this->view->experiences = $experiences;
        
        $recommandationsObject = new Resourcing_Model_DbTable_Recommandations();
        $recommandations = $recommandationsObject->all();
        $this->view->recommandations = $recommandations;
        
        $query = new Zend_Search_Lucene_Search_Query_Boolean();
        $query->addSubquery(new Zend_Search_Lucene_Search_Query_Term(new Zend_Search_Lucene_Index_Term(null)));
        //$hits = $index->find($query);
        //$index->optimize();
        //$hits = $this->getAllDocument($indexBd);
        $hits = $this->getAllDocument($index);


        if ($this->_request->isGet()) {
            //print '<pre>';print_r($this->_request->getParams());print '</pre>';die;
            $datefrom = $todate = $minsal = $maxsal = '';
            $query = new Zend_Search_Lucene_Search_Query_Boolean();

            //$subquery1 = new Zend_Search_Lucene_Search_Query_MultiTerm();
            if ($this->_request->getParam('mots_cles')) {
                $term = new Zend_Search_Lucene_Index_Term(strtolower($this->_request->getParam('mots_cles')));
                $subquery1 = new Zend_Search_Lucene_Search_Query_Term($term);
                
                $query->addSubquery($subquery1, true);
            }

            if ($this->_request->getParam('poste')) {
                $subqueryposte = new Zend_Search_Lucene_Search_Query_Phrase();
                $postes = explode(' ', strtolower($this->_request->getParam('poste')));
                foreach ($postes as $poste) {
                    $subqueryposte->addTerm(new Zend_Search_Lucene_Index_Term($poste));
                }

                $subqueryposte->setSlop(100);
                $query->addSubquery($subqueryposte, true);
            }
            //Recherche par date d'entretien
            if ($this->_request->getParam('from') || $this->_request->getParam('to')) {
                if ($this->_request->getParam('from')) {
                    $datefrom = str_replace('-', '', $this->_request->getParam('from'));
                    $from = new Zend_Search_Lucene_Index_Term($datefrom, 'date_entretien');
                    if (!$this->_request->getParam('to') || $this->_request->getParam('to') == '') {
                        $todate = new Zend_Search_Lucene_Index_Term('20501231', 'date_entretien');
                        $to = new Zend_Search_Lucene_Index_Term($todate, 'date_entretien');
                    }
                }
                if ($this->_request->getParam('to')) {
                    $todate = str_replace('-', '', $this->_request->getParam('to'));
                    $to = new Zend_Search_Lucene_Index_Term($todate, 'date_entretien');
                    if (!$this->_request->getParam('from') || $this->_request->getParam('from') == '') {
                        $datefrom = new Zend_Search_Lucene_Index_Term('19700101', 'date_entretien');
                        $from = new Zend_Search_Lucene_Index_Term($datefrom, 'date_entretien');
                    }
                }
                $queryDate = new Zend_Search_Lucene_Search_Query_Range($from, $to, true);
                $query->addSubquery($queryDate, true);
            }

            if ($this->_request->getParam('minsal') || $this->_request->getParam('maxsal')) {
                if ($this->_request->getParam('minsal')) {
                    $minsal = new Zend_Search_Lucene_Index_Term($this->_request->getParam('minsal'), 'salaire');
                    if (!$this->_request->getParam('maxsal') || $this->_request->getParam('maxsal') == '') {
                        $maxsal = new Zend_Search_Lucene_Index_Term('500000000', 'salaire');
                    }
                }
                if ($this->_request->getParam('maxsal')) {
                    $maxsal = new Zend_Search_Lucene_Index_Term($this->_request->getParam('maxsal'), 'salaire');
                    if (!$this->_request->getParam('minsal') || $this->_request->getParam('minsal') == '') {
                        $minsal = new Zend_Search_Lucene_Index_Term('0', 'salaire');
                    }
                }
                $querySalaire = new Zend_Search_Lucene_Search_Query_Range($minsal, $maxsal, true);
                $querySalaire->rewrite($index);
                $query->addSubquery($querySalaire, true);
            }

            if ($this->_request->getParam('experience')) {
                $experience = new Zend_Search_Lucene_Index_Term($this->_request->getParam('experience'), 'id_experience');
                $queryExper = new Zend_Search_Lucene_Search_Query_Term($experience);
                $query->addSubquery($queryExper, true);
            }

            if ($this->_request->getParam('etudes')) {
                $etude = new Zend_Search_Lucene_Index_Term($this->_request->getParam('etudes'), 'id_etude');
                $queryEtude = new Zend_Search_Lucene_Search_Query_Term($etude);
                $query->addSubquery($queryEtude, true);
            }

            if ($this->_request->getParam('type_candidat')) {
                $request = $this->_request->getParam('type_candidat');
                $type = array();
                foreach ($request as $value) {
                    $type[] = new Zend_Search_Lucene_Index_Term(strtolower($value), 'type_candidat');
                }
                $queryType = new Zend_Search_Lucene_Search_Query_MultiTerm($type, array(null, null));
                $query->addSubquery($queryType, true);
            }

            if ($this->_request->getParam('technologies')) {
                $request_technos = $this->_request->getParam('technologies');
                $technos = array();
                $queryTechno = new Zend_Search_Lucene_Search_Query_Phrase();
                foreach ($request_technos as $techno) {
                    $queryTechno->addTerm(new Zend_Search_Lucene_Index_Term(strtolower($techno), 'direction_recommandation'));
                    $queryTechno->addTerm(new Zend_Search_Lucene_Index_Term(strtolower($techno), 'direction_recommandation'));
                    $queryTechno->addTerm(new Zend_Search_Lucene_Index_Term(strtolower($techno), 'direction_recommandation'));
                }
                $queryTechno->setSlop(100);
                $query->addSubquery($queryTechno, true);
            }
            
            if ($this->_request->getParam('recommandations')) {
                $request_recoms = $this->_request->getParam('recommandations');
                $recoms = array();
                $queryRecoms = new Zend_Search_Lucene_Search_Query_Phrase();
                foreach ($request_recoms as $recom) {
                    $queryRecoms->addTerm(new Zend_Search_Lucene_Index_Term(strtolower($recom), 'direction_recommandation'));
                    $queryRecoms->addTerm(new Zend_Search_Lucene_Index_Term(strtolower($recom), 'operation_recommandation'));
                    $queryRecoms->addTerm(new Zend_Search_Lucene_Index_Term(strtolower($recom), 'prequalif_recommandation'));
                }
                $queryRecoms->setSlop(100);
                $query->addSubquery($queryRecoms, true);
            }

            if (!empty($query) && (($datefrom != '' || $todate != '' || $maxsal != '' || $minsal != '' ||
                    $this->_request->getParam('mots_cles') || $this->_request->getParam('experience') 
                    || $this->_request->getParam('etudes') || $this->_request->getParam('type_candidat') 
                    || $this->_request->getParam('technologies') || $this->_request->getParam('recommandations') ))) {
                //$index->optimize();
                $hits = $index->find($query);
            } else {
                $query->addSubquery(new Zend_Search_Lucene_Search_Query_Term(new Zend_Search_Lucene_Index_Term(null)));
                //$index->optimize();
                //$hits = $this->getAllDocument($indexBd);
                $hits = $this->getAllDocument($index);
            }
        }
        $this->view->count = count($hits);
        //$this->view->candidats = $hits;
        
        $filtersArray = array();
        $filter = '';
        foreach ($this->_request->getParams() as $key => $val){
            if (!in_array($key, array('module', 'controller', 'action', 'page', 'filter')) && !empty($val)){
                $filtersArray[$key] = $val;
            }
        }
        $size = count($filtersArray, COUNT_RECURSIVE);
        $cpt = 1;
        if(!empty($filtersArray)){
            foreach($filtersArray as $cle => $valeur){
                if (!empty($valeur)){
                    if (is_array($valeur)){
                        foreach($valeur as $v){
                            ($cpt > 1 && $cpt < $size) ? $filter .= '&' : '';
                            $filter .= $cle . '[]=' . $v;
                        }
                    } else {
                        ($cpt > 1 && $cpt < $size) ? $filter .= '&' : '';
                        $filter .= $cle . '=' . $valeur;
                    }
                }
                $cpt++;
            }
        }
        $this->view->filters = $filter;
        $this->view->urlFilter = '/' . $this->_request->getModuleName() . '/' . $this->_request->getControllerName() . '/' .
                $this->_request->getActionName(); 
        $paginator = Zend_Paginator::factory($hits);
        $pageNumber=$this->_getParam('page',0);
        $paginator->setItemCountPerPage(10);
        $paginator->setCurrentPageNumber($pageNumber);
        $this->view->candidats = $paginator;
        
        parent::indexAction();
    }*/
    
    public function searchAction() {

        $this->_layout = 'search_grid';
        
        $candidatObject = new Resourcing_Model_DbTable_Candidat();
        
        $technologiesObject = new Resourcing_Model_DbTable_Technologie();
        $technologies = $technologiesObject->all();
        $this->view->technologies = $technologies;

        $etudesObject = new Resourcing_Model_DbTable_Etudes();
        $etudes = $etudesObject->all();
        $this->view->etudes = $etudes;

        $experiencesObject = new Resourcing_Model_DbTable_Experience();
        $experiences = $experiencesObject->all();
        $this->view->experiences = $experiences;
        
        $search = $candidatObject->getSearchResult($candidatObject, $technologiesObject, $etudesObject, 
                $experiencesObject, $this->_request);
        //print '<pre>';print_r($search['count']);die;
        $this->view->count = $search['count'];
        $this->view->candidats = $search['candidats'];
        $this->view->rates = $search['rates'];
        $this->view->filters = $search['filter'];
        $this->view->urlFilter = $search['urlFilter']; 
        $this->view->isGet = $search['isGet'];
        $paginator = Zend_Paginator::factory($search['candidats']);
        $pageNumber=$this->_getParam('page',0);
        $paginator->setItemCountPerPage(10);
        $paginator->setCurrentPageNumber($pageNumber);
        $this->view->candidats = $paginator;
        
        parent::indexAction();
    }
    
    public function downloadCvAction (){
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        
        $candidatObject = new Resourcing_Model_DbTable_Candidat();
        $id = unserialize(base64_decode($this->_request->getParam('id')));
        $where = $candidatObject->getAdapter()->quoteInto('id = ?', $id);
        $candidat = $candidatObject->fetchRow($where);
        $real_file = realpath(PUBLIC_PATH . Resourcing_Form_Model_Candidat::PATH_CV . $candidat->cv);
        if(is_file($real_file)){
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename='.basename($real_file));
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($real_file));
            ob_clean();
            flush();
            readfile($real_file);
            exit;
        }
    }
    
    public function exportCandidatsAction(){
        require_once 'Resourcing/PHPExcel.php';
        
        $candidatObject = new Resourcing_Model_DbTable_Candidat();
        $technologiesObject = new Resourcing_Model_DbTable_Technologie();
        $etudesObject = new Resourcing_Model_DbTable_Etudes();
        $experiencesObject = new Resourcing_Model_DbTable_Experience();
        
        $search = $candidatObject->getSearchResult($candidatObject, $technologiesObject, $etudesObject, 
                $experiencesObject, $this->_request);
        //print '<pre>';print_r($this->_request->getParams());die;
        $sheet = $candidatObject->generateWorkSheet($search['candidats']);
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $date =  date('YmdHis');
        ob_start();
        $this->_response->setHeader('Content-Type', 'application/vnd.ms-excel');
        $this->_response->setHeader('Expires', 0);
        $this->_response->setHeader('Cache-Control', 'must-revalidate, post-check=0, pre-check=0');
        $this->_response->setHeader(
            'Content-Disposition',
            'attachment;filename=' . sprintf(
                'export_candidats-%s.xlsx',
                $date
            )
        );
        $writer = PHPExcel_IOFactory::createWriter($sheet, 'Excel2007');
        $writer->save('php://output');
    }
    
    public function indexAction() {
        $this->view->urlFilter = '/' . $this->_request->getModuleName() . '/' . $this->_request->getControllerName() . '/' .
                $this->_request->getActionName();
        parent::indexAction();
    }

    public function importDataAction() {
        require_once 'Resourcing/PHPExcel.php';
        require_once 'Resourcing/PHPExcel/IOFactory.php';
        
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        
        $file = realpath(PUBLIC_PATH . self::PATH_IMPORT . 'Vivier_candidats_Equinoxes_2011.xlsx');
        $ecolesObject = new Resourcing_Model_DbTable_Ecole();
        $etudesObject = new Resourcing_Model_DbTable_Etudes();
        $candidatObject = new Resourcing_Model_DbTable_Candidat();
        $sourceObject = new Resourcing_Model_DbTable_Sourcecv();
        $domaineObject = new Resourcing_Model_DbTable_Domaine();
        $debutObject = new Resourcing_Model_DbTable_DebutCarriere();
        $experienceModel = new Resourcing_Model_DbTable_Experience();
        
        //$fileObject = new PHPExcel($file);
        //  Read your Excel workbook
        try {
            $inputFileType = PHPExcel_IOFactory::identify($file);
            $objReader = PHPExcel_IOFactory::createReader($inputFileType);
            $objPHPExcel = $objReader->load($file);
        } catch (Exception $e) {
            die('Error loading file "' . pathinfo($file, PATHINFO_BASENAME) . '": ' . $e->getMessage());
        }

        //  Get worksheet dimensions
        $sheet = $objPHPExcel->getSheet(0);
        $highestRow = $sheet->getHighestRow();
        $highestColumn = $sheet->getHighestColumn();
        
        //Import des candidats
        for($row = 2; $row <= $highestRow; $row++){
            $lastname = trim($sheet->getCell('A' . $row)->getValue());
            $firstname = trim($sheet->getCell('B' . $row)->getValue());
            $where = $candidatObject->getAdapter()->quoteInto('last_name like ? AND first_name like ?', 
                    '%' . $lastname . '%', '%' . $firstname . '%');
            $candidatExist = $candidatObject->fetchRow($where);
            if(!$candidatExist){
                $source = trim($sheet->getCell('C' . $row)->getValue());
                $whereSource = $sourceObject->getAdapter()->quoteInto('sourcecv like ?', $source);
                $sourceExist = $sourceObject->fetchRow($whereSource);
                if(!$sourceExist){
                    $insertSource = $sourceObject->insert(array('sourcecv' => $source));
                    $sourceObjectId = $insertSource;
                } else {
                    $sourceObjectId = $sourceExist->id ;
                }
                
                $domaine = trim($sheet->getCell('D' . $row)->getValue());
                if(!empty($domaine)){
                    $whereDomaine = $domaineObject->getAdapter()->quoteInto('domaine like ?', $domaine);
                    $domaineExist = $domaineObject->fetchRow($whereDomaine);
                    if(!$domaineExist){
                        $insertDomaine = $domaineObject->insert(array('domaine' => $domaine));
                        $domaineObjectId = $insertDomaine;
                    } else {
                        $domaineObjectId = $domaineExist->id;
                    }
                }
                $ecole = trim($sheet->getCell('G' . $row)->getValue());
                if(!empty($ecole)){
                    $whereEcole = $ecolesObject->getAdapter()->quoteInto('ecole like ?', $ecole);
                    $ecoleExist = $ecolesObject->fetchRow($whereEcole);
                    if(!$ecoleExist && !empty($ecole)){
                        $ecolesObject->insert(array('ecole' => $ecole));
                    }
                }
                
                $debut = trim($sheet->getCell('I' . $row)->getValue());
                if(!empty($debut)){
                    $whereDebut = $debutObject->getAdapter()->quoteInto('annee = ?', (int)$debut);
                    $debutCarriereId = $debutObject->fetchRow($whereDebut)->id;
                }
                
                $etudes = trim($sheet->getCell('K' . $row)->getValue());
                if(!empty($etudes)){
                    $whereEtudes = $etudesObject->getAdapter()->quoteInto('etude like ?', '%' . $etudes . '%');
                    $etudesExist = $etudesObject->fetchRow($whereEtudes);
                    if(!$etudesExist){
                        $insertEtudes = $etudesObject->insert(array('etude' => $etudes));
                        $etudesObjectId = $insertEtudes;
                    } else {
                        $etudesObjectId = $etudesExist->id;
                    }
                }
                
                $cerification = trim($sheet->getCell('N' . $row)->getValue());
                
                $dateDebut = trim($sheet->getCell('I' . $row)->getValue());
                if (!empty($dateDebut)) {
                    if($dateDebut > 1995 && $dateDebut < 2014){
                        $diff = (int) date('Y') - $dateDebut;
                        if($diff < 1){
                            $experience = 4;
                        } elseif($diff < 2) {
                            $experience = 5;
                        } elseif($diff < 5) {
                            $experience = 6;
                        } else {
                            $experience = 7;
                        }
                    }
                }
                //var_dump(trim($sheet->getCell('O' . $row)->getValue()));
                $dataCandidats = array(
                    'type_candidat'             => 'Profil',
                    'civilite'                  => 'Mr',
                    'last_name'                 => $lastname,
                    'first_name'                => $firstname,
                    'email'                     => trim($sheet->getCell('O' . $row)->getValue()),
                    'tel'                       => (int)trim($sheet->getCell('Q' . $row)->getValue()),
                    'tel2'                      => '',
                    'nationalite'               => 'Tunisienne',
                    'residence'                 => trim($sheet->getCell('H' . $row)->getValue()),
                    'id_domaine'                => $domaineObjectId,
                    'poste'                     => trim($sheet->getCell('F' . $row)->getValue()),
                    'ecole'                     => $sheet->getCell('G' . $row)->getValue(),
                    'id_experience'             => $experience,
                    'statut'                    => trim($sheet->getCell('J' . $row)->getValue()),
                    'id_etude'                  => $etudesObjectId,
                    'societe'                   => trim($sheet->getCell('L' . $row)->getValue()),
                    'societe_precedente'        => trim($sheet->getCell('M' . $row)->getValue()),
                    'competences_secondaires'   => $sheet->getCell('E' . $row)->getValue(),
                    'id_debut_carriere'         => $debutCarriereId,
                    'certifie'                  => !empty($cerification) ? 'Oui' : 'Non',
                    'certification'             => $cerification,
                    'id_sourcecv'               => $sourceObjectId,
                );
                
                $candidatObject->insert($dataCandidats);
            }
        }
        
    }

    public function getAllDocument($index, $deleted = false) {
        $document = null;
        // number of document in the index
        $numDocs = $index->count();
        for ($id = 0; $id < $numDocs; $id++) {
            if ($deleted == false) {
                if (!$index->isDeleted($id)) {
                    $documentObject = $index->getDocument($id);
                    if(in_array('id_candidat', $documentObject->getFieldNames())){
                        $document[$id] = $index->getDocument($id);
                    }
                }
            } else {
                    $documentObject = $index->getDocument($id);
                if(in_array('id_candidat', $documentObject->getFieldNames())){
                    $document[$id] = $index->getDocument($id);
                }
            }
        }
        return $document;
    }

    public function deletePhotoAction() {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $candidatId = $this->_request->getPost('candidat');
        $candidatObject = new Resourcing_Model_DbTable_Candidat();
        $candidat = $candidatObject->fetchRow('id = ' . $candidatId);
        if($candidat){
            $candidatObject->update(array('photo' => 'deleted'), 'id = ' . $candidatId);
        }
        echo json_encode(true);
    }

    public function getOperationnelAction() {
        if ($this->_request->isXmlHttpRequest()) {
            $this->_helper->layout->disableLayout();
            $this->_helper->viewRenderer->setNoRender(true);
            $typeId = $this->_request->getPost('type');
            $userObject = new Auth_Model_DbTable_User();
            $operationnels = $userObject->all();
            $result = '<option></option>';
            foreach ($operationnels as $operationnel) {
                $roles = $operationnel->groups->toArray();
                foreach ($roles as $role) {
                    if ($role['id'] == $typeId) {
                        $result .= '<option value="' . $operationnel->id . '">' . $operationnel->username . '</option>';
                    }
                }
            }
            echo json_encode($result);
        }
    }

    public function getExperienceAction() {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $id_debut = $this->_request->getPost('debut');
        $result = '<option></option>';
        if ($id_debut) {
            $debutModel = new Resourcing_Model_DbTable_DebutCarriere();
            $debut = $debutModel->fetchRow('id = ' . $id_debut)->annee;
            //$debut = $debutModel->all();
            $experienceModel = new Resourcing_Model_DbTable_Experience();
            $experienceObject = $experienceModel->all();
            $diff = (int) date('Y') - $debut;
            $arrayResult = array();
            foreach ($experienceObject as $experience) {
                if ($diff == $experience->min) {
                    $arrayResult[$experience->id] = $experience->experience;
                }
                if (!empty($experience->max) && $diff == $experience->max) {
                    $arrayResult[$experience->id] = $experience->experience;
                }
                if ($diff <= $experience->max && $diff >= $experience->min) {
                    $arrayResult[$experience->id] = $experience->experience;
                }
                if (empty($experience->max) && $diff >= $experience->min) {
                    $arrayResult[$experience->id] = $experience->experience;
                }
            }
            array_unique($arrayResult);
            foreach ($arrayResult as $key => $value) {
                $result .= '<option value="' . $key . '">' . $value . '</option>';
            }
        }
        echo json_encode($result);
    }

    public function sendNotificationAction() {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        $userObject = new Auth_Model_DbTable_User();
        $candidatObject = new Resourcing_Model_DbTable_Candidat();
        $whereCandidat = $candidatObject->getAdapter()->quoteInto('id = ?', (int)$this->_request->getParam('candidat-id'));
        $candidat = $candidatObject->fetchRow($whereCandidat);
        
        if($this->_request->getParam('type') == 'prequalif'){
            $operationnelId = self::PREQUALIF_RESPONSABLE_ID;
        } else {
            $operationnelId = $this->_request->getPost('operationnel');
        }
        $type_entretien = $this->_request->getPost('type_entretien');
        $date_entretien = $this->_request->getPost('date_entretien');
        $time_entretien = $this->_request->getPost('time_entretien');
        $where = $userObject->getAdapter()->quoteInto('id = ?', $operationnelId);
        $operationnels = $userObject->all($where)->toArray();
        $eventsObject = new Resourcing_Model_DbTable_Events();
        $eventsObject->insert(array('id_user'   => $operationnelId,
                                   'event'     => 'Entretien avec le candidat ' . $candidat->first_name . ' ' . $candidat->last_name,
                                   'start'     => $date_entretien . ' ' . $time_entretien,
                                   'end'       => $date_entretien . ' ' . $time_entretien));

            //print '<pre>';print_r($time_entretien);print '</pre>';die;
//            print '<pre>';print_r($time_entretien);print '</pre>';
//            print '<pre>';print_r($operationnels);print '</pre>';
        //die;
//            $config = array('ssl' => 'tls',
//                            'auth' => 'login',
//                            'username' => 'thr.othmen@gmail.com',
//                            'password' => '');

        $config = array('auth' => 'login',
            'ssl' => 'tls',
            'port' => '587',
            'username' => 'othmen.tahri',
            'password' => 'Keyrus#2014'
        );

        $transport = new Zend_Mail_Transport_Smtp('frparsca01.keyruscorp.key', $config);
        $mail = new Zend_Mail();
        $mail->setFrom('thr.othmen@yahoo.fr', 'Tahri');
        $mail->addTo('othmen.tahri@keyrus.com', 'Othmen');
        $mail->setSubject('Hello World');
        $mail->setBodyText('This is the text of the mail.');
        try {
            $mail->send($transport);
        } catch (Exception $ex) {
            $ex->getMessage();
        }
        //print '<pre>';print_r($mail->send($transport));print '</pre>';
        $result = 'Un mail a été envoyé à ' . $operationnels->email;
        echo json_encode($result);
        //}
    }

}
