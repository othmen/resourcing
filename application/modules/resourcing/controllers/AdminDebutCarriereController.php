<?php

class Resourcing_AdminDebutCarriereController extends Centurion_Controller_CRUD
{

    public function preDispatch()
    {
        $this->_helper->authCheck();
        $this->_helper->aclCheck();
        $this->_helper->layout->setLayout('admin');
        parent::preDispatch();
    }
    
    public function init()
    {
        $this->_formClassName = 'Resourcing_Form_Model_DebutCarriere';
        $this->_displays = array(
            'annee'        => $this->view->translate('Début Carrière'),
        );

        $this->view->placeholder('headling_1_content')->set($this->view->translate('Début Carrière'));
        $this->view->placeholder('headling_1_add_buttonadd')->set($this->view->translate('Début Carrière'));
        $this->_filters = array(
                'annee' => array(
                    'behavior' => self::FILTER_BEHAVIOR_CALLBACK,
                    'callback' => array($this, 'filterDebutCarriereName'),
                    'label' => $this->view->translate('Début Carrière'),
                ),
            );
        parent::init();
        
    }
    
   public function filterDebutCarriereName($value, &$sqlFilter)
    {
        $sqlFilter[] = new Zend_Db_Expr(
            Centurion_Db_Table_Abstract::getDefaultAdapter()
            ->quoteInto('annee like ? COLLATE utf8_general_ci', '%' . $value . '%'));
    }
  
  
}

