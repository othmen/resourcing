<?php

class Resourcing_AdminAvantageController extends Centurion_Controller_CRUD
{

    public function preDispatch()
    {
        $this->_helper->authCheck();
        $this->_helper->aclCheck();
        $this->_helper->layout->setLayout('admin');
        parent::preDispatch();
    }
    
    public function init()
    {
        $this->_formClassName = 'Resourcing_Form_Model_Avantage';
        $this->_displays = array(
            'avantage'        => $this->view->translate('Avantages'),
        );

        $this->view->placeholder('headling_1_content')->set($this->view->translate('Avantage'));
        $this->view->placeholder('headling_1_add_buttonadd')->set($this->view->translate('Avantage'));
        $this->_filters = array(
                'avantage' => array(
                    'behavior' => self::FILTER_BEHAVIOR_CALLBACK,
                    'callback' => array($this, 'filterAvantageName'),
                    'label' => $this->view->translate('Avantage'),
                ),
            );
        parent::init();
        
    }
    
   public function filterAvantageName($value, &$sqlFilter)
    {
        $sqlFilter[] = new Zend_Db_Expr(
            Centurion_Db_Table_Abstract::getDefaultAdapter()
            ->quoteInto('avantage like ? COLLATE utf8_general_ci', '%' . $value . '%'));
    }
  
  
}

