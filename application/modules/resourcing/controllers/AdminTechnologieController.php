<?php

class Resourcing_AdminTechnologieController extends Centurion_Controller_CRUD
{

    public function preDispatch()
    {
        $this->_helper->authCheck();
        $this->_helper->aclCheck();
        $this->_helper->layout->setLayout('admin');
        parent::preDispatch();
    }
    
    public function init()
    {
        $this->_formClassName = 'Resourcing_Form_Model_Technologie';
        $this->_displays = array(
            'technologie'        => $this->view->translate('Technologies'),
        );

        $this->view->placeholder('headling_1_content')->set($this->view->translate('Technologies'));
        $this->view->placeholder('headling_1_add_buttonadd')->set($this->view->translate('Technologies'));
        $this->_filters = array(
                'technologie' => array(
                    'behavior' => self::FILTER_BEHAVIOR_CALLBACK,
                    'callback' => array($this, 'filterTechnologieName'),
                    'label' => $this->view->translate('Technologie'),
                ),
            );
        parent::init();
        
    }
    
   public function filterTechnologieName($value, &$sqlFilter)
    {
        $sqlFilter[] = new Zend_Db_Expr(
            Centurion_Db_Table_Abstract::getDefaultAdapter()
            ->quoteInto('technologie like ? COLLATE utf8_general_ci', '%' . $value . '%'));
    }
  
  
}

