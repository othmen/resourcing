<?php

class Resourcing_EventsController extends Centurion_Controller_CRUD {

    public function preDispatch() {
        $this->_helper->authCheck();
        $this->_helper->aclCheck();
//        $this->_helper->layout->setLayout('default');
//        parent::preDispatch();
    }

    public function init() {
        $this->_formClassName = 'Resourcing_Form_Model_Events';
        $this->_displays = array(
            'id_user' => $this->view->translate('Identifiant'),
            'event' => $this->view->translate('Nom de l\'événement'),
            'start' => $this->view->translate('Date de début'),
            'end' => $this->view->translate('Poste recherché'),
            'infos_keyrus' => $this->view->translate('Date de fin'),
        );

        parent::init();
    }
    
    public function indexAction($param) {
        $eventsObject = new Resourcing_Model_DbTable_Events();
        $holidaysObject = new Resourcing_Model_DbTable_Holidays();
        $feries = $holidaysObject->fetchAll()->toArray();
        $holidays = array();
        
        $user = Centurion_Auth::getInstance()->getIdentity()->id;
        $where = $eventsObject->getAdapter()->quoteInto('id_user = ?', $user);
        $events = $eventsObject->fetchAll($where);
        $event = array();
        foreach($events as $key => $value){
            $event[] = array('id'       => $value['id'],
                             'title'    => $value['event'],
                             'start'    => $value['start'],
                             'end'      => $value['end'],
                             'allDay'   => false);
        }
        foreach($feries as $value){
            $holidays[] = array('id'       => 'holiday',
                                'title'    => $value['title'],
                                'start'    => $value['date'],
                                'end'      => $value['date'],
                                'backgroundColor' => '#db2736',
                                'allDay'   => true);
        }
        
        $mergeEvents = array_merge($event, $holidays);
        //print '<pre>';print_r($feries);die;
        $this->view->jsonEvents = json_encode($mergeEvents);
//        $this->view->jsonEvents = json_encode($event);
        $this->view->jsonHolidays = json_encode($holidays);
    }

    public function createEventAction($param) {
        
        $eventObject = new Resourcing_Model_DbTable_Events();
        if ($this->_request->isXmlHttpRequest()) {
            $this->_helper->layout->disableLayout();
            $this->_helper->viewRenderer->setNoRender(true);
            $debut = $this->_request->getPost('debut');
            $fin = $this->_request->getPost('fin');
            $event = $this->_request->getPost('event');
            $user = Centurion_Auth::getInstance()->getIdentity()->id;
            if(!empty($event)){
                if($this->_request->getPost('id')) {
                   $id = $this->_request->getPost('id');
                   $eventUpdate = $eventObject->update(array(   'id_user'   => $user,
                                                                'event'     => $event,
                                                                'start'     => $debut,
                                                                'end'       => $fin
                                                            ), 'id = ' . $id);

                }else {
                    $eventCreation = $eventObject->insert(array('id_user'   => $user,
                                                            'event'     => $event,
                                                            'start'     => $debut,
                                                            'end'       => $fin));
                }
            }
            if($eventCreation || $eventUpdate)
                echo true;
            
        }
    }
    
    public function deleteEventAction($param) {
        
        $eventObject = new Resourcing_Model_DbTable_Events();
        if ($this->_request->isXmlHttpRequest()) {
            $this->_helper->layout->disableLayout();
            $this->_helper->viewRenderer->setNoRender(true);
            $id = $this->_request->getPost('id');
            $where = $eventObject->getAdapter()->quoteInto('id = ?', $id); 
            $delete = $eventObject->delete($where);
            if($delete)
                echo true;
            
        }
    }

}
