<?php

class Resourcing_AdminDomaineController extends Centurion_Controller_CRUD
{

    public function preDispatch()
    {
        $this->_helper->authCheck();
        $this->_helper->aclCheck();
        $this->_helper->layout->setLayout('admin');
        parent::preDispatch();
    }
    
    public function init()
    {
        $this->_formClassName = 'Resourcing_Form_Model_Domaine';
        $this->_displays = array(
            'domaine'        => $this->view->translate('Domaines'),
        );

        $this->view->placeholder('headling_1_content')->set($this->view->translate('Domaines'));
        $this->view->placeholder('headling_1_add_buttonadd')->set($this->view->translate('Domaine'));
        $this->_filters = array(
                'domaine' => array(
                    'behavior' => self::FILTER_BEHAVIOR_CALLBACK,
                    'callback' => array($this, 'filterDomaineName'),
                    'label' => $this->view->translate('Domaine'),
                ),
            );
        parent::init();
        
    }
    
   public function filterEcoleName($value, &$sqlFilter)
    {
        $sqlFilter[] = new Zend_Db_Expr(
            Centurion_Db_Table_Abstract::getDefaultAdapter()
            ->quoteInto('domaine like ? COLLATE utf8_general_ci', '%' . $value . '%'));
    }
  
  
}

