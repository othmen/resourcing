<?php

class Resourcing_AdminEtudesController extends Centurion_Controller_CRUD
{

    public function preDispatch()
    {
        $this->_helper->authCheck();
        $this->_helper->aclCheck();
        $this->_helper->layout->setLayout('admin');
        parent::preDispatch();
    }
    
    public function init()
    {
        $this->_formClassName = 'Resourcing_Form_Model_Etudes';
        $this->_displays = array(
            'etude'        => $this->view->translate('Etudes'),
        );

        $this->view->placeholder('headling_1_content')->set($this->view->translate('Etudes'));
        $this->view->placeholder('headling_1_add_buttonadd')->set($this->view->translate('Etudes'));
        $this->_filters = array(
                'etude' => array(
                    'behavior' => self::FILTER_BEHAVIOR_CALLBACK,
                    'callback' => array($this, 'filterEtudeName'),
                    'label' => $this->view->translate('Etude'),
                ),
            );
        parent::init();
        
    }
    
   public function filterEtudeName($value, &$sqlFilter)
    {
        $sqlFilter[] = new Zend_Db_Expr(
            Centurion_Db_Table_Abstract::getDefaultAdapter()
            ->quoteInto('etude like ? COLLATE utf8_general_ci', '%' . $value . '%'));
    }
  
  
}

