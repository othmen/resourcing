<?php

class Resourcing_Model_DbTable_DebutCarriere extends Centurion_Db_Table_Abstract
{
    protected $_name = 'debut_carriere';
    
    protected $_primary = 'id';
    
    protected $_rowClass = 'Resourcing_Model_DbTable_Row_DebutCarriere';
    
    protected $_meta = array('verboseName'   => 'Debut Carrière',
                             'verbosePlural' => 'Debuts Carrière');
    
    protected $_dependentTables = array('Resourcing_Model_DbTable_Candidat');
    
}


