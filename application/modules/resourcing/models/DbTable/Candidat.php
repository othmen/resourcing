<?php

class Resourcing_Model_DbTable_Candidat extends Centurion_Db_Table_Abstract {

    protected $_name = 'candidat';
    protected $_primary = 'id';
    protected $_rowClass = 'Resourcing_Model_DbTable_Row_Candidat';
    protected $_meta = array(   'verboseName' => 'Candidat',
        'verbosePlural' => 'Candidats'
    );
    
    protected $_dependentTables = array('Resourcing_Model_DbTable_CandidatCompetence',
        'Resourcing_Model_DbTable_CandidatTechnologie',
        'Resourcing_Model_DbTable_CandidatMetier',
        'Resourcing_Model_DbTable_Prequalif',
        'Resourcing_Model_DbTable_Rate'
    );
    
    protected $_referenceMap = array(
        
        'DebutCarriere' => array(
            'columns' => 'id_debut_carriere',
            'refColumns' => 'id',
            'refTableClass' => 'Resourcing_Model_DbTable_DebutCarriere',
            'onDelete' => self::CASCADE,
            'onUpdate' => self::CASCADE
        ),
        'Domaine' => array(
            'columns' => 'id_domaine',
            'refColumns' => 'id',
            'refTableClass' => 'Resourcing_Model_DbTable_Domaine',
            'onDelete' => self::CASCADE,
            'onUpdate' => self::CASCADE
        ),
        'Experience' => array(
            'columns' => 'id_experience',
            'refColumns' => 'id',
            'refTableClass' => 'Resourcing_Model_DbTable_Experience',
            'onDelete' => self::CASCADE,
            'onUpdate' => self::CASCADE
        ),
        'Etudes' => array(
            'columns' => 'id_etude',
            'refColumns' => 'id',
            'refTableClass' => 'Resourcing_Model_DbTable_Etudes',
            'onDelete' => self::CASCADE,
            'onUpdate' => self::CASCADE
        ),
        'Sourcecv' => array(
            'columns' => 'id_sourcecv',
            'refColumns' => 'id',
            'refTableClass' => 'Resourcing_Model_DbTable_Sourcecv',
            'onDelete' => self::CASCADE,
            'onUpdate' => self::CASCADE
        ),
    );
    protected $_manyDependentTables = array(
        'metier' => array(
            'refTableClass' => 'Resourcing_Model_DbTable_Metier',
            'intersectionTable' => 'Resourcing_Model_DbTable_CandidatMetier',
            'columns' => array(
                'local' => 'candidat_id',
                'foreign' => 'metier_id'
            )
        ),
        'technologie' => array(
            'refTableClass' => 'Resourcing_Model_DbTable_Technologie',
            'intersectionTable' => 'Resourcing_Model_DbTable_CandidatTechnologie',
            'columns' => array(
                'local' => 'candidat_id',
                'foreign' => 'technologie_id'
            )
        ),
        'competence' => array(
            'refTableClass' => 'Resourcing_Model_DbTable_Competence',
            'intersectionTable' => 'Resourcing_Model_DbTable_CandidatCompetence',
            'columns' => array(
                'local' => 'candidat_id',
                'foreign' => 'competence_id'
            )
        )
    );
    
    public function getSearchResult($candidatObject, $technologiesObject, $etudesObject, $experiencesObject, $request){
        $candidatObject = new Resourcing_Model_DbTable_Candidat();
        
        $candidatTechnologies = new Resourcing_Model_DbTable_CandidatTechnologie();
        $directionObject = new Resourcing_Model_DbTable_Direction(); 
        $operationObject = new Resourcing_Model_DbTable_Operation(); 
        $prequalifObject = new Resourcing_Model_DbTable_Prequalif();
        $rates = array();
        $isGet = false;
        
        $candidats = $candidatObject->fetchAll(null, 'id DESC');
        $whereString = '';
        //$_isGet = '';
        $sqlFilter = array();
        if ($request->isGet()) {
            $datefrom = $todate = $minsal = $maxsal = '';
            //$_isGet = '#search-result';
            if ($request->getParam('mots_cles')) {
                $requestMotsCles = $request->getParam('mots_cles');
                $whereString = 'last_name like ? OR first_name like ? OR tel like ? OR tel2 LIKE ? '
                        . 'OR nationalite LIKE ? OR residence LIKE ? OR poste LIKE ? OR ecole LIKE ? '
                        . 'OR statut = ? OR societe LIKE ? OR societe_precedente LIKE ? '
                        . 'OR competences_secondaires LIKE ? OR commentaire LIKE ? COLLATE utf8_general_ci';
                $sqlFilter[] = new Zend_Db_Expr(Centurion_Db_Table_Abstract::getDefaultAdapter()
                                ->quoteInto($whereString, '%' . $requestMotsCles . '%'));
            }

            if ($request->getParam('poste')) {
                $requestPoste = $request->getParam('poste');
                $sqlFilter[] = new Zend_Db_Expr(Centurion_Db_Table_Abstract::getDefaultAdapter()
                                ->quoteInto('poste LIKE ? COLLATE utf8_general_ci', '%' . $requestPoste . '%'));
            }
            //Recherche par date d'entretien
            if ($request->getParam('from') || $request->getParam('to')) {
                if ($request->getParam('from')) {
                    $wherePrequalifFrom = $prequalifObject->getDefaultAdapter()
                            ->quoteInto('date_entretien >= ?', $request->getParam('from'));
                    $whereOperationFrom = $operationObject->getDefaultAdapter()
                            ->quoteInto('date_entretien >= ?', $request->getParam('from'));
                    $whereDirectionFrom = $directionObject->getDefaultAdapter()
                            ->quoteInto('date_entretien >= ?', $request->getParam('from'));
                    $prequalifFrom = $prequalifObject->fetchAll($wherePrequalifFrom, 'date_entretien DESC, id DESC');
                    $operationFrom = $operationObject->fetchAll($whereOperationFrom, 'date_entretien DESC, id DESC');
                    $directionFrom = $directionObject->fetchAll($whereDirectionFrom, 'date_entretien DESC, id DESC');
                    $fromIds = array();
                    foreach($prequalifFrom->toArray() as $key => $value ){
                        if(!empty($value['id_candidat']) && !in_array($value['id_candidat'], $fromIds)){
                            $fromIds[] = $value['id_candidat'];
                        }
                    }
                    foreach($operationFrom->toArray() as $key => $value ){
                        if(!empty($value['id_candidat']) && !in_array($value['id_candidat'], $fromIds)){
                            $fromIds[] = $value['id_candidat'];
                        }
                    }
                    foreach($directionFrom->toArray() as $key => $value ){
                        if(!empty($value['id_candidat']) && !in_array($value['id_candidat'], $fromIds)){
                            $fromIds[] = $value['id_candidat'];
                        }
                    }
                    $sqlFilter[] = new Zend_Db_Expr(Centurion_Db_Table_Abstract::getDefaultAdapter()
                                ->quoteInto('id IN ("' . implode('","', $fromIds) . '")'));
                    
                }
                if ($request->getParam('to')) {
                    $wherePrequalifTo = $prequalifObject->getDefaultAdapter()
                            ->quoteInto('date_entretien <= ?', $request->getParam('to'));
                    $whereOperationTo = $operationObject->getDefaultAdapter()
                            ->quoteInto('date_entretien <= ?', $request->getParam('to'));
                    $whereDirectionTo = $directionObject->getDefaultAdapter()
                            ->quoteInto('date_entretien <= ?', $request->getParam('to'));
                    $prequalifTo = $prequalifObject->fetchAll($wherePrequalifTo, 'id DESC');
                    $operationTo = $operationObject->fetchAll($whereOperationTo, 'id DESC');
                    $directionTo = $directionObject->fetchAll($whereDirectionTo, 'id DESC');
                    $toIds = array();
                    foreach($prequalifTo->toArray() as $key => $value ){
                        if(!empty($value['id_candidat']) && !in_array($value['id_candidat'], $toIds)){
                            $toIds[] = $value['id_candidat'];
                        }
                    }
                    foreach($operationTo->toArray() as $key => $value ){
                        if(!empty($value['id_candidat']) && !in_array($value['id_candidat'], $toIds)){
                            $toIds[] = $value['id_candidat'];
                        }
                    }
                    foreach($directionTo->toArray() as $key => $value ){
                        if(!empty($value['id_candidat']) && !in_array($value['id_candidat'], $toIds)){
                            $toIds[] = $value['id_candidat'];
                        }
                    }
                    $sqlFilter[] = new Zend_Db_Expr(Centurion_Db_Table_Abstract::getDefaultAdapter()
                                ->quoteInto('id IN ("' . implode('","', $toIds) . '")'));
                }
                
            }

            if ($request->getParam('minsal') || $request->getParam('maxsal')) {
                if ($request->getParam('minsal') && !$request->getParam('maxsal')) {
                    $whereMinSal = $prequalifObject->getDefaultAdapter()
                            ->quoteInto('pretention_salariale_min >= ? ' ,(int)$request->getParam('minsal'));
                    $PrequalifMinSal = $prequalifObject->fetchAll($whereMinSal, 'id DESC');
                    $PrequalifMinSalIds = array();
                    foreach($PrequalifMinSal->toArray() as $key => $value ){
                        if(!empty($value['id_candidat']) && !in_array($value['id_candidat'], $PrequalifMinSalIds)){
                            $PrequalifMinSalIds[] = $value['id_candidat'];
                        }
                    }
                    $sqlFilter[] = new Zend_Db_Expr(Centurion_Db_Table_Abstract::getDefaultAdapter()
                                ->quoteInto('id IN ("' . implode('","', $PrequalifMinSalIds) . '")'));
                }
                if ($request->getParam('maxsal') && !$request->getParam('minsal')) {
                    $whereMaxSal = $prequalifObject->getDefaultAdapter()
                            ->quoteInto('pretention_salariale_max <= ?', (int)$request->getParam('maxsal'));
                    $PrequalifMaxSal = $prequalifObject->fetchAll($whereMaxSal, 'id DESC');
                    $PrequalifMaxSalIds = array();
                    foreach($PrequalifMaxSal->toArray() as $key => $value ){
                        if(!empty($value['id_candidat']) && !in_array($value['id_candidat'], $PrequalifMaxSalIds)){
                            $PrequalifMaxSalIds[] = $value['id_candidat'];
                        }
                    }
                    $sqlFilter[] = new Zend_Db_Expr(Centurion_Db_Table_Abstract::getDefaultAdapter()
                                ->quoteInto('id IN ("' . implode('","', $PrequalifMaxSalIds) . '")'));
                }
                if ($request->getParam('maxsal') && $request->getParam('minsal')) {
                    $minsal = (int)$request->getParam('minsal') > 0 ? (int)$request->getParam('minsal') : 0;
                    $maxsal = (int)$request->getParam('maxsal') > 0 ? (int)$request->getParam('maxsal') : 0;
                    $whereMinSal = $prequalifObject->getDefaultAdapter()
                            ->quoteInto('pretention_salariale_min >= ? ', $minsal);
                    $whereMaxSal = $prequalifObject->getDefaultAdapter()
                            ->quoteInto('pretention_salariale_max <= ? ', $maxsal);
                    $PrequalifMinSal = $prequalifObject->fetchAll($whereMinSal, 'id DESC');
                    $PrequalifMaxSal = $prequalifObject->fetchAll($whereMaxSal, 'id DESC');
                    $PrequalifMinSalIds = array();
                    $PrequalifMaxSalIds = array();
                    //$PrequalifSalIds = array();
                    foreach($PrequalifMinSal->toArray() as $key => $value ){
                        if(!empty($value['id_candidat']) && !in_array($value['id_candidat'], $PrequalifMinSalIds)){
                            $PrequalifMinSalIds[] = $value['id_candidat'];
                        }
                    }
                    foreach($PrequalifMaxSal->toArray() as $key => $value ){
                        if(!empty($value['id_candidat']) && !in_array($value['id_candidat'], $PrequalifMaxSalIds)){
                            $PrequalifMaxSalIds[] = $value['id_candidat'];
                        }
                    }
                    $intersectSalIds = array_unique(array_merge($PrequalifMaxSalIds, $PrequalifMinSalIds));
                    $sqlFilter[] = new Zend_Db_Expr(Centurion_Db_Table_Abstract::getDefaultAdapter()
                                ->quoteInto('id IN ("' . implode('","', $intersectSalIds) . '")'));
                }
                
            }

            if ($request->getParam('experience')) {
                $sqlFilter[] = new Zend_Db_Expr(Centurion_Db_Table_Abstract::getDefaultAdapter()
                                ->quoteInto('id_experience = ? ', $request->getParam('experience')));
            }

            if ($request->getParam('etudes')) {
                $sqlFilter[] = new Zend_Db_Expr(Centurion_Db_Table_Abstract::getDefaultAdapter()
                                ->quoteInto('id_etude = ? ', $request->getParam('etudes')));
            }

            if ($request->getParam('type_candidat')) {
                $requestType = implode('","', $request->getParam('type_candidat'));
                $sqlFilter[] = new Zend_Db_Expr(Centurion_Db_Table_Abstract::getDefaultAdapter()
                                ->quoteInto('type_candidat IN ("' . $requestType . '") '));
            }

            if ($request->getParam('technologies')) {
                $requestTechnologies = implode('","', $request->getParam('technologies'));
                $whereTechnologie = $technologiesObject
                                    ->getDefaultAdapter()
                                    ->quoteInto('technologie IN ("' . $requestTechnologies . '")');
                $technologies = $technologiesObject->fetchAll($whereTechnologie);
                $technologiesIds = array();
                foreach($technologies->toArray() as $key => $value){
                    $technologiesIds[] = $value['id'];
                }
                $whereTechnoIds = $candidatTechnologies
                                ->getDefaultAdapter()
                                ->quoteInto('technologie_id IN ("' . implode('","', $technologiesIds) . '")');
                
                $candidatsTechnos = $candidatTechnologies->fetchAll($whereTechnoIds);
                $technoIds = array();
                foreach($candidatsTechnos->toArray() as $cle => $candidat){
                    $technoIds[] = $candidat['candidat_id'];
                }
                $sqlFilter[] = new Zend_Db_Expr(Centurion_Db_Table_Abstract::getDefaultAdapter()
                                ->quoteInto('id IN ("' . implode('","', $technoIds) . '")'));
            }
            
            if (($datefrom != '' || $todate != '' || $maxsal != '' || $minsal != '' ||
                    $request->getParam('mots_cles') || $request->getParam('poste')
                    || $request->getParam('experience') || $request->getParam('etudes') || $request->getParam('minsal') 
                    || $request->getParam('maxsal') || $request->getParam('from') || $request->getParam('to') 
                    || $request->getParam('type_candidat') || $request->getParam('technologies'))) {
                $candidats = $candidatObject->all($sqlFilter, 'id DESC');
                $isGet = true;
            } else {
                $candidats = $candidatObject->fetchAll(null, 'id DESC');
            }
        }
        $count = count($candidats);
        foreach($candidats as $candidat){
            $hasRates = $this->getCountLikes($candidat->id);
            if(!empty($hasRates) && $hasRates != '&nbsp;&nbsp;'){
                $rates[$candidat->id] = $hasRates;
            }
        }
        $filtersArray = array();
        $filter = '';
        foreach ($request->getParams() as $key => $val){
            if (!in_array($key, array('module', 'controller', 'action', 'page', 'filter')) && !empty($val)){
                $filtersArray[$key] = $val;
            }
        }
        $size = count($filtersArray, COUNT_RECURSIVE);
        $cpt = 1;
        //print '<pre>';print_r($filtersArray);
        if(!empty($filtersArray)){
            foreach($filtersArray as $cle => $valeur){
                if (!empty($valeur)){
                    if (is_array($valeur)){
                        foreach($valeur as $v){
                            ($cpt > 0 && $cpt < $size) ? $filter .= '&' : '';
                            $filter .= $cle . '[]=' . $v;
                        }
                    } else {
                        ($cpt > 1 && $cpt < $size) ? $filter .= '&' : '';
                        $filter .= $cle . '=' . $valeur;
                    }
                }
                $cpt++;
            }
        }
        $urlFilter = '/' . $request->getModuleName() . '/' . $request->getControllerName() . '/' .
                $request->getActionName(); 
        return compact('candidats', 'rates','count', 'filter', 'urlFilter', 'isGet');
    }
    
    public function getCountLikes($id){
        $whereLike = Centurion_Db::getSingleton('resourcing/rate')
                    ->getDefaultAdapter()
                    ->quoteInto('candidat_id = ' . intval($id) . ' AND value = 1');
        $likes = Centurion_Db::getSingleton('resourcing/rate')
                ->fetchAll($whereLike)
                ->count();
        $returnLike = '';
        if($likes > 0){
            $returnLike = '<span class="font-bold">+' . $likes . '</span> <span class="large"><i class="glyph-icon large font-green icon-thumbs-o-up"></i></span>';
        }
        return $returnLike;
    }
    
    /**
     *
     * @param array $candidats
     * 
     * @return PHPExcel 
     */
    public function generateWorkSheet($candidats) {
        
        $directionObject = new Resourcing_Model_DbTable_Direction(); 
        $operationObject = new Resourcing_Model_DbTable_Operation(); 
        $prequalifObject = new Resourcing_Model_DbTable_Prequalif();
        
        $bold = array('font' => array('bold' => true));
        $centerAll = array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
            )
        );

        $horizontalCenter = array('alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
        $verticalalCenter = array('alignment' => array('vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER));
        $headerColor = array(
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => '35a551'),
            )
        );

        //PHPExcel_Cell::setValueBinder(new PHPExcel_Cell_AdvancedValueBinder());

        $sheet = new PHPExcel();
        //$sheet->getProperties()->setCreator($user->fullname);
        $page = $sheet->getActiveSheet();
        $page->getSheetView()->setZoomScale(100);

        $headers = array(
            'Nom', 'Société actuelle', 'Profil', 'Date entretien', 'Expérience', 'Résultat Préqualif', 'Résultat Opérationnel', 'Résultat de direction', 'Disponibilité', 
            'Salaire actuel', 'Salaire souhaité min', 'Salaire souhaité max'
        );

        //Writing headers
        $page->getStyle('A1:L1')->applyFromArray($bold + $headerColor)->getFont()->setSize(8);
        foreach ($headers as $offset => $header) {
            $page->setCellValue(self::getColumnNameAtOffset($offset) . '1', $header);
        }
        $page->getColumnDimension('A')->setWidth(25);
        $page->getColumnDimension('B')->setWidth(25);
        $page->getColumnDimension('C')->setWidth(40);
        $page->getColumnDimension('D')->setWidth(15);
        $page->getColumnDimension('E')->setWidth(15);
        $page->getColumnDimension('F')->setWidth(10);
        $page->getColumnDimension('G')->setWidth(10);
        $page->getColumnDimension('H')->setWidth(10);
        $page->getColumnDimension('I')->setWidth(15);
        $page->getColumnDimension('J')->setWidth(15);
        $page->getColumnDimension('K')->setWidth(15);
        $page->getColumnDimension('L')->setWidth(15);
        $row = 2;

        //Filling cells
        foreach ($candidats as $candidat) {
            $whereDirection = $directionObject->getDefaultAdapter()
                                ->quoteInto('id_candidat = ?', $candidat->id);
            $direction = $directionObject->fetchRow($whereDirection, 'id DESC');

            $whereOperation = $operationObject->getDefaultAdapter()
                                ->quoteInto('id_candidat = ?', $candidat->id);
            $operation = $operationObject->fetchRow($whereOperation, 'id DESC');

            $wherePrequalif = $prequalifObject->getDefaultAdapter()
                                ->quoteInto('id_candidat = ?', $candidat->id);
            $prequalif = $prequalifObject->fetchRow($wherePrequalif, 'id DESC');
            if(!Centurion_Auth::getCurrent()->hasPerm('view_candidate_salary')){
                $prequalif->salaire_actuel = '';
                $prequalif->pretention_salariale_min = '';
                $prequalif->pretention_salariale_max = '';
            }
//            $date_entretien = array('prequalif' => $prequalif->date_entretien,
//                'operation' => $operation->date_entretien,
//                'direction' => $direction->date_entretien
//            );
//            $entretiens = $date_entretien;
//            sort($date_entretien);
//            //$date_entretien = sort(array($direction->date_entretien, $direction->date_entretien));
//            //$date = new Zend_Date($rawDate, self::MYSQL_DATE_FORMAT);
//            $last_entretien = array_search($date_entretien[max(array_keys($date_entretien))], $entretiens);
//            $resultat = '';
//            if($last_entretien == 'prequalif')
//                $resultat = $$last_entretien->avis;
//            elseif(in_array($last_entretien, array('direction', 'operation')))
//                $resultat = $$last_entretien->verdict;
            $page->getStyle('A' . $row .':L' . $row)->getFont()->setSize(8);
            $page->setCellValue('A' . $row, $candidat->first_name . ' ' . $candidat->last_name);
            $page->setCellValue('B' . $row, $candidat->societe);
            $page->setCellValue('C' . $row, $candidat->poste);
            $page->setCellValue('D' . $row, $date_entretien[max(array_keys($date_entretien))]);
            $page->setCellValue('E' . $row, $candidat->Experience->experience);
            $page->setCellValue('F' . $row, $prequalif->avis);
            $page->setCellValue('G' . $row, $operation->verdict);
            $page->setCellValue('H' . $row, $direction->verdict);
            $page->setCellValue('I' . $row, $prequalif->disponibilite);
            $page->setCellValue('J' . $row, $prequalif->salaire_actuel);
            $page->setCellValue('K' . $row, $prequalif->pretention_salariale_min);
            $page->setCellValue('L' . $row, $prequalif->pretention_salariale_max);
            $row++;
        }
        //$page->setCellValue('A' . $row, print_r($data['logs'], 1));

        return $sheet;
    }
    
    public static function getColumnNameAtOffset($offset)
    {
        $indexes = array('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U',
                          'V', 'W', 'X', 'Y', 'Z', 'AA', 'AB', 'AC', 'AD', 'AE');
        return $indexes[$offset];
    }

}
