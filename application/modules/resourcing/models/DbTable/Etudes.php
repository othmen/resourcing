<?php

class Resourcing_Model_DbTable_Etudes extends Centurion_Db_Table_Abstract
{
    protected $_name = 'etudes';
    
    protected $_primary = 'id';
    
    protected $_rowClass = 'Resourcing_Model_DbTable_Row_Etudes';
    
    protected $_meta = array('verboseName'   => 'Etude',
                             'verbosePlural' => 'Etudes');
    
    protected $_dependentTables = array('Resourcing_Model_DbTable_Candidat');
    
}


