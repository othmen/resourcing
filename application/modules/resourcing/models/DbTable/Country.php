<?php

class Resourcing_Model_DbTable_Country extends Centurion_Db_Table_Abstract
{
    protected $_name = 'country';
    
    protected $_primary = 'id';
    
    protected $_rowClass = 'Resourcing_Model_DbTable_Row_Country';
    
    protected $_meta = array('verboseName'   => 'Pays',
                             'verbosePlural' => 'Pays');
    
    protected $_dependentTables = array('Resourcing_Model_DbTable_Prequalif');
    
}


