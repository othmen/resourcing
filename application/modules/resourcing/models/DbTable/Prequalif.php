<?php

class Resourcing_Model_DbTable_Prequalif extends Centurion_Db_Table_Abstract {
    
    protected $_name = 'prequalif';
    protected $_primary = 'id';
    protected $_rowClass = 'Resourcing_Model_DbTable_Row_Prequalif';
    protected $_meta = array(   'verboseName' => 'Entretien Prequalif',
                                'verbosePlural' => 'Entretiens Prequalif'
                            );
    protected $_dependentTables = array('Resourcing_Model_DbTable_PrequalifAvantage', 
                                        'Resourcing_Model_DbTable_PrequalifCountry');
    
    protected $_referenceMap = array(
                'Operationnel' => array(
                    'columns' => 'id_operationnel',
                    'refColumns' => 'id',
                    'refTableClass' => 'Auth_Model_DbTable_User',
                    'onDelete' => self::CASCADE,
                    'onUpdate' => self::CASCADE
                ),
                'Candidat' => array(
                    'columns' => 'id_candidat',
                    'refColumns' => 'id',
                    'refTableClass' => 'Resourcing_Model_DbTable_Candidat',
                    'onDelete' => self::CASCADE,
                    'onUpdate' => self::CASCADE
                ),
                'TypeContrat' => array(
                    'columns' => 'id_type_contrat',
                    'refColumns' => 'id',
                    'refTableClass' => 'Resourcing_Model_DbTable_TypeContrat',
                    'onDelete' => self::CASCADE,
                    'onUpdate' => self::CASCADE
                ),
                'TestTechnique' => array(
                    'columns' => 'id_test_technique',
                    'refColumns' => 'id',
                    'refTableClass' => 'Resourcing_Model_DbTable_TestTechnique',
                    'onDelete' => self::CASCADE,
                    'onUpdate' => self::CASCADE
                ),
                'Country' => array(
                    'columns' => 'id_mission_country',
                    'refColumns' => 'id',
                    'refTableClass' => 'Resourcing_Model_DbTable_Country',
                    'onDelete' => self::CASCADE,
                    'onUpdate' => self::CASCADE
                )
            );
    protected $_manyDependentTables = array(
        'avantages'        =>  array(
            'refTableClass'     =>  'Resourcing_Model_DbTable_Avantage',
            'intersectionTable' =>  'Resourcing_Model_DbTable_PrequalifAvantage',
            'columns'   =>  array(
                'local'     =>  'prequalif_id',
                'foreign'   =>  'avantage_id'
            )
        ),
        'countries'        =>  array(
            'refTableClass'     =>  'Resourcing_Model_DbTable_Country',
            'intersectionTable' =>  'Resourcing_Model_DbTable_PrequalifCountry',
            'columns'   =>  array(
                'local'     =>  'prequalif_id',
                'foreign'   =>  'country_id'
            )
        )
    );
}
