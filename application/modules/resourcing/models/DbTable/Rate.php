<?php

class Resourcing_Model_DbTable_Rate extends Centurion_Db_Table_Abstract
{
    protected $_name = 'rate';
    
    protected $_primary = 'id';
    
    protected $_rowClass = 'Resourcing_Model_DbTable_Row_Rate';
    
    protected $_meta = array('verboseName'   => 'Vote',
                             'verbosePlural' => 'Votes');
    
    protected $_referenceMap = array(
        
        'operationnel' => array(
            'columns' => 'id_operationnel',
            'refColumns' => 'id',
            'refTableClass' => 'Resourcing_Model_DbTable_Operationnel',
            'onDelete' => self::CASCADE,
            'onUpdate' => self::CASCADE
        ),
        'candidat' => array(
            'columns' => 'candidat_id',
            'refColumns' => 'id',
            'refTableClass' => 'Resourcing_Model_DbTable_Candidat',
            'onDelete' => self::CASCADE,
            'onUpdate' => self::CASCADE
        )
    );
    
}


