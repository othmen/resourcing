<?php

class Resourcing_Model_DbTable_PrequalifCountry extends Centurion_Db_Table_Abstract {
    
    protected $_name = 'prequalif_country';
    protected $_primary = 'id';
    protected $_rowClass = 'Resourcing_Model_DbTable_Row_PrequalifCountry';
    protected $_meta = array(   'verboseName' => 'Pays mission',
                                'verbosePlural' => 'Pays missions'
                            );
    
    protected $_referenceMap = array(
        
        'prequalif' => array(
            'columns' => 'prequalif_id',
            'refColumns' => 'id',
            'refTableClass' => 'Resourcing_Model_DbTable_Prequalif',
            'onDelete' => self::CASCADE,
            'onUpdate' => self::CASCADE
        ),
        'country' => array(
            'columns' => 'country_id',
            'refColumns' => 'id',
            'refTableClass' => 'Resourcing_Model_DbTable_Country',
            'onDelete' => self::CASCADE,
            'onUpdate' => self::CASCADE
        ),
    );
}
