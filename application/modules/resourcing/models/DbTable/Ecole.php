<?php

class Resourcing_Model_DbTable_Ecole extends Centurion_Db_Table_Abstract
{
    protected $_name = 'ecole';
    
    protected $_primary = 'id';
    
    protected $_rowClass = 'Resourcing_Model_DbTable_Row_Ecole';
    
    protected $_meta = array('verboseName'   => 'Ecole',
                             'verbosePlural' => 'Ecoles');
    
    protected $_dependentTables = array('Resourcing_Model_DbTable_Candidat');
    
}


