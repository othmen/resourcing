<?php

class Resourcing_Model_DbTable_Experience extends Centurion_Db_Table_Abstract
{
    protected $_name = 'experience';
    
    protected $_primary = 'id';
    
    protected $_rowClass = 'Resourcing_Model_DbTable_Row_Experience';
    
    protected $_meta = array('verboseName'   => 'Experience',
                             'verbosePlural' => 'Experiences');
    
    protected $_dependentTables = array('Resourcing_Model_DbTable_Candidat');
    
}


