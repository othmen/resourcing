<?php

class Resourcing_Model_DbTable_Operationnel extends Centurion_Db_Table_Abstract
{
    protected $_name = 'operationnel';
    
    protected $_primary = 'id';
    
    protected $_rowClass = 'Resourcing_Model_DbTable_Row_Operationnel';
    
    protected $_meta = array('verboseName'   => 'Operatioennel',
                             'verbosePlural' => 'Operatioennels');
    
    protected $_dependentTables = array('Resourcing_Model_DbTable_Operation', 'Resourcing_Model_DbTable_Direction');
    
}


