<?php

class Resourcing_Model_DbTable_PrequalifAvantage extends Centurion_Db_Table_Abstract {
    
    protected $_name = 'prequalif_avantage';
    protected $_primary = 'id';
    protected $_rowClass = 'Resourcing_Model_DbTable_Row_PrequalifAvantage';
    protected $_meta = array(   'verboseName' => 'Avantage',
                                'verbosePlural' => 'Avantages'
                            );
    
    protected $_referenceMap = array(
        
        'prequalif' => array(
            'columns' => 'prequalif_id',
            'refColumns' => 'id',
            'refTableClass' => 'Resourcing_Model_DbTable_Prequalif',
            'onDelete' => self::CASCADE,
            'onUpdate' => self::CASCADE
        ),
        'avantage' => array(
            'columns' => 'avantage_id',
            'refColumns' => 'id',
            'refTableClass' => 'Resourcing_Model_DbTable_Avantage',
            'onDelete' => self::CASCADE,
            'onUpdate' => self::CASCADE
        ),
    );
}
