<?php

class Resourcing_Model_DbTable_Technologie extends Centurion_Db_Table_Abstract
{
    protected $_name = 'technologie';
    
    protected $_primary = 'id';
    
    protected $_rowClass = 'Resourcing_Model_DbTable_Row_Technologie';
    
    protected $_meta = array('verboseName'   => 'Technologie',
                             'verbosePlural' => 'Technologies');
    
}


