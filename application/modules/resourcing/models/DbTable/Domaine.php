<?php

class Resourcing_Model_DbTable_Domaine extends Centurion_Db_Table_Abstract
{
    protected $_name = 'domaine';
    
    protected $_primary = 'id';
    
    protected $_rowClass = 'Resourcing_Model_DbTable_Row_Domaine';
    
    protected $_meta = array('verboseName'   => 'Domaine',
                             'verbosePlural' => 'Domaines');
    
    protected $_dependentTables = array('Resourcing_Model_DbTable_Candidat');
    
}


