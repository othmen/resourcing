<?php

class Resourcing_Model_DbTable_Operation extends Centurion_Db_Table_Abstract {

    protected $_name = 'operation';
    protected $_primary = 'id';
    protected $_rowClass = 'Resourcing_Model_DbTable_Row_Operation';
    protected $_meta = array('verboseName' => 'Entretien Opération',
        'verbosePlural' => 'Entretiens Opération'
    );
    protected $_referenceMap = array(
        'Operationnel' => array(
            'columns' => 'id_operationnel',
            'refColumns' => 'id',
            'refTableClass' => 'Auth_Model_DbTable_User',
            'onDelete' => self::CASCADE,
            'onUpdate' => self::CASCADE
        )
    );

}
