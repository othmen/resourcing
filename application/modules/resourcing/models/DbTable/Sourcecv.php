<?php

class Resourcing_Model_DbTable_Sourcecv extends Centurion_Db_Table_Abstract
{
    protected $_name = 'sourcecv';
    
    protected $_primary = 'id';
    
    protected $_rowClass = 'Resourcing_Model_DbTable_Row_Sourcecv';
    
    protected $_meta = array('verboseName'   => 'Source CV',
                             'verbosePlural' => 'Sources CV');
    
    protected $_dependentTables = array('Resourcing_Model_DbTable_Candidat');
    
}


