<?php

class Resourcing_Model_DbTable_CandidatTechnologie extends Centurion_Db_Table_Abstract {
    
    protected $_name = 'candidat_technologie';
    protected $_primary = 'id';
    protected $_rowClass = 'Resourcing_Model_DbTable_Row_CandidatTechnologie';
    protected $_meta = array(   'verboseName' => 'Technologie Candidat',
                                'verbosePlural' => 'Technologies Candidat'
                            );
    
    protected $_referenceMap = array(
        
        'technologie' => array(
            'columns' => 'technologie_id',
            'refColumns' => 'id',
            'refTableClass' => 'Resourcing_Model_DbTable_Technologie',
            'onDelete' => self::CASCADE,
            'onUpdate' => self::CASCADE
        ),
        'candidat' => array(
            'columns' => 'candidat_id',
            'refColumns' => 'id',
            'refTableClass' => 'Resourcing_Model_DbTable_Candidat',
            'onDelete' => self::CASCADE,
            'onUpdate' => self::CASCADE
        ),
    );
}
