<?php

class Resourcing_Model_DbTable_TypeContrat extends Centurion_Db_Table_Abstract
{
    protected $_name = 'type_contrat';
    
    protected $_primary = 'id';
    
    protected $_rowClass = 'Resourcing_Model_DbTable_Row_TypeContrat';
    
    protected $_meta = array('verboseName'   => 'Type Contrat',
                             'verbosePlural' => 'Types Contrats');
    
    protected $_dependentTables = array('Resourcing_Model_DbTable_Prequalif');
    
}


