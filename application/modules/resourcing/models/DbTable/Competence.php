<?php

class Resourcing_Model_DbTable_Competence extends Centurion_Db_Table_Abstract
{
    protected $_name = 'competence';
    
    protected $_primary = 'id';
    
    protected $_rowClass = 'Resourcing_Model_DbTable_Row_Competence';
    
    protected $_meta = array('verboseName'   => 'Compétence',
                             'verbosePlural' => 'Compétences');
    
    protected $_dependentTables = array('Resourcing_Model_DbTable_CandidatCompetence');
    
}


