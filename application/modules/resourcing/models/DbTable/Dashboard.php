<?php

class Resourcing_Model_DbTable_Dashboard extends Centurion_Db_Table_Abstract {
    
    protected $_name = 'events';
    protected $_primary = 'id';
    protected $_rowClass = 'Resourcing_Model_DbTable_Row_Events';
    protected $_meta = array(   'verboseName' => 'Événement',
                                'verbosePlural' => 'Événements'
                            );
}
