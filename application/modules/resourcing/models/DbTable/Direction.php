<?php

class Resourcing_Model_DbTable_Direction extends Centurion_Db_Table_Abstract {

    protected $_name = 'direction';
    protected $_primary = 'id';
    protected $_rowClass = 'Resourcing_Model_DbTable_Row_Direction';
    protected $_meta = array('verboseName' => 'Entretien Direction',
        'verbosePlural' => 'Entretiens Direction'
    );
    protected $_referenceMap = array(
        'Operationnel' => array(
            'columns' => 'id_operationnel',
            'refColumns' => 'id',
            'refTableClass' => 'Auth_Model_DbTable_User',
            'onDelete' => self::CASCADE,
            'onUpdate' => self::CASCADE
        )
    );

}
