<?php

class Resourcing_Model_DbTable_Events extends Centurion_Db_Table_Abstract {
    
    protected $_name = 'events';
    protected $_primary = 'id';
    protected $_rowClass = 'Resourcing_Model_DbTable_Row_Events';
    protected $_meta = array(   'verboseName' => 'Événement',
                                'verbosePlural' => 'Événements'
                            );
    
    protected $_referenceMap = array(
        'utilisateur' => array(
            'columns' => 'id_user',
            'refColumns' => 'id',
            'refTableClass' => 'Auth_Model_DbTable_User',
            'onDelete' => self::CASCADE,
            'onUpdate' => self::CASCADE
        )
    );
}
