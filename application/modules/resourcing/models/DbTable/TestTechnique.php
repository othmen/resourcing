<?php

class Resourcing_Model_DbTable_TestTechnique extends Centurion_Db_Table_Abstract
{
    protected $_name = 'test_technique';
    
    protected $_primary = 'id';
    
    protected $_rowClass = 'Resourcing_Model_DbTable_Row_TestTechnique';
    
    protected $_meta = array('verboseName'   => 'Test Technique',
                             'verbosePlural' => 'Tests Techniques');
    
    protected $_dependentTables = array('Resourcing_Model_DbTable_Prequalif');
    
}


