<?php

class Resourcing_Model_DbTable_CandidatCompetence extends Centurion_Db_Table_Abstract {
    
    protected $_name = 'candidat_competence';
    protected $_primary = 'id';
    protected $_rowClass = 'Resourcing_Model_DbTable_Row_CandidatCompetence';
    protected $_meta = array(   'verboseName' => 'Compétence Candidat',
                                'verbosePlural' => 'Compétences Candidat'
                            );
    
    protected $_referenceMap = array(
        
        'competence' => array(
            'columns' => 'competence_id',
            'refColumns' => 'id',
            'refTableClass' => 'Resourcing_Model_DbTable_Competence',
            'onDelete' => self::CASCADE,
            'onUpdate' => self::CASCADE
        ),
        'candidat' => array(
            'columns' => 'candidat_id',
            'refColumns' => 'id',
            'refTableClass' => 'Resourcing_Model_DbTable_Candidat',
            'onDelete' => self::CASCADE,
            'onUpdate' => self::CASCADE
        ),
    );
}
