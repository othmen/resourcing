<?php

class Resourcing_Model_DbTable_Metier extends Centurion_Db_Table_Abstract
{
    protected $_name = 'metier';
    
    protected $_primary = 'id';
    
    protected $_rowClass = 'Resourcing_Model_DbTable_Row_Metier';
    
    protected $_meta = array('verboseName'   => 'Metier',
                             'verbosePlural' => 'Metiers');

    protected $_dependentTables = array('Resourcing_Model_DbTable_CandidatMetier');
    
}


