<?php

class Resourcing_Model_DbTable_Holidays extends Centurion_Db_Table_Abstract
{
    protected $_name = 'holidays';
    
    protected $_primary = 'id';
    
    protected $_rowClass = 'Resourcing_Model_DbTable_Row_Holidays';
    
    protected $_meta = array('verboseName'   => 'Jour férié',
                             'verbosePlural' => 'Jours fériés');
    
}


