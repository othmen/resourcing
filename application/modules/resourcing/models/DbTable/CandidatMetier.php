<?php

class Resourcing_Model_DbTable_CandidatMetier extends Centurion_Db_Table_Abstract {
    
    protected $_name = 'candidat_metier';
    protected $_primary = 'id';
    protected $_rowClass = 'Resourcing_Model_DbTable_Row_CandidatMetier';
    protected $_meta = array(   'verboseName' => 'Métier Candidat',
                                'verbosePlural' => 'Métiers Candidat'
                            );
    
    protected $_referenceMap = array(
        
        'metier' => array(
            'columns' => 'metier_id',
            'refColumns' => 'id',
            'refTableClass' => 'Resourcing_Model_DbTable_Metier',
            'onDelete' => self::CASCADE,
            'onUpdate' => self::CASCADE
        ),
        'candidat' => array(
            'columns' => 'candidat_id',
            'refColumns' => 'id',
            'refTableClass' => 'Resourcing_Model_DbTable_Candidat',
            'onDelete' => self::CASCADE,
            'onUpdate' => self::CASCADE
        ),
    );
}
