<?php

class Resourcing_Model_DbTable_Avantage extends Centurion_Db_Table_Abstract
{
    protected $_name = 'avantage';
    
    protected $_primary = 'id';
    
    protected $_rowClass = 'Resourcing_Model_DbTable_Row_Avantage';
    
    protected $_meta = array('verboseName'   => 'Avantage',
                             'verbosePlural' => 'Avantages');
    
    protected $_dependentTables = array('Resourcing_Model_DbTable_PrequalifAvantage');
    
}


