<?php

class Resourcing_View_Helper_GridRowIconActions extends Admin_View_Helper_GridRowActions
{

    public function GridRowIconActions($label, $options)
    {   
        
        $url = '';
        $cls = '';
        $clsurl = '';
        $clickCallback = '';
        	
        $linkAttribs = array('title' => $label);
        
        if (isset($options['url'])) {
            if (is_array($options['url'])) {
                $url = call_user_func_array(array($this->view, 'url'), $options['url']);
            } else {
                $url = $options['url'];
            }
            
            $this->_row = $this->view->row;
            $url = preg_replace_callback('`/___(.*)___(/|\?|$)`', array(&$this, '_replaceRowCallback'), $url);
            $this->_row = null;

            $url = $url . '?ticket=' . $this->view->ticket()->getKey($url);
        }

        if (isset($options['clickCallback'])) {
            $linkAttribs = array_merge($linkAttribs, array('onclick' => $options['clickCallback']));
        }        
        
        $linkAttribs = $this->_htmlAttribs($linkAttribs);
        
        if (isset($options['cls']))
            $cls = $options['cls'];
        if (isset($options['clsurl']))
            $clsurl = $options['clsurl'];
            
        return sprintf('<a href="%s" class="help %s" %s><i class="ui-icon %s"></i></a>', $url, $clsurl, $linkAttribs, $cls); 
    }
}