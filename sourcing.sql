CREATE DATABASE  IF NOT EXISTS `sourcing` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `sourcing`;
-- MySQL dump 10.13  Distrib 5.6.13, for Win32 (x86)
--
-- Host: localhost    Database: sourcing
-- ------------------------------------------------------
-- Server version	5.1.61

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `auth_belong`
--

DROP TABLE IF EXISTS `auth_belong`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_belong` (
  `user_id` int(11) unsigned NOT NULL,
  `group_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`user_id`,`group_id`),
  KEY `fk_belong__group_id___group__user_id` (`group_id`),
  CONSTRAINT `fk_belong__group_id___group__user_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_reference_32` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_belong`
--

LOCK TABLES `auth_belong` WRITE;
/*!40000 ALTER TABLE `auth_belong` DISABLE KEYS */;
INSERT INTO `auth_belong` VALUES (1,1),(7,1),(8,1),(3,2),(9,2),(10,2),(11,2),(3,4),(4,4),(5,4),(6,4),(9,4),(10,4),(11,4),(2,5);
/*!40000 ALTER TABLE `auth_belong` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_group`
--

DROP TABLE IF EXISTS `auth_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_group` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` text,
  `group_parent_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_group__group_parent_id___group__id` (`group_parent_id`),
  CONSTRAINT `fk_group__group_parent_id___group__id` FOREIGN KEY (`group_parent_id`) REFERENCES `auth_group` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group`
--

LOCK TABLES `auth_group` WRITE;
/*!40000 ALTER TABLE `auth_group` DISABLE KEYS */;
INSERT INTO `auth_group` VALUES (1,'Administrator','Administrator',4),(2,'Webmaster','Webmaster',3),(3,'User','User',NULL),(4,'Moderator','Moderator',NULL),(5,'Anonymous','Anonymous',NULL);
/*!40000 ALTER TABLE `auth_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_group_permission`
--

DROP TABLE IF EXISTS `auth_group_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_group_permission` (
  `group_id` int(11) unsigned NOT NULL,
  `permission_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`group_id`,`permission_id`),
  KEY `fk_group_permission__permission_id___permission__id` (`permission_id`),
  CONSTRAINT `fk_group_permission__group_id___group__id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_group_permission__permission_id___permission__id` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group_permission`
--

LOCK TABLES `auth_group_permission` WRITE;
/*!40000 ALTER TABLE `auth_group_permission` DISABLE KEYS */;
INSERT INTO `auth_group_permission` VALUES (1,0),(2,0),(1,1),(2,1),(1,3),(1,4),(2,4),(4,4),(1,5),(2,5),(1,6),(2,6),(1,7),(1,8),(1,9),(4,11),(2,19),(4,19),(2,20),(4,20),(2,21),(4,21),(2,22),(4,22),(2,23),(2,24),(2,25),(4,25),(2,26),(4,26),(2,27),(4,27),(2,28),(4,28),(2,29),(2,30),(1,31),(2,31),(3,31),(4,31),(5,31),(1,32),(2,32),(3,32),(4,32),(1,33),(2,33),(3,33),(4,33),(1,34),(2,34),(4,34),(1,35),(2,35),(4,35),(1,36),(2,36),(4,36),(1,37),(2,37),(4,37),(1,38),(2,38),(3,38),(4,38),(5,38),(1,39),(2,39),(4,39),(1,40),(2,40),(4,40),(1,41),(2,41),(4,41),(1,42),(2,42),(4,42),(1,43),(2,43),(4,43),(1,44),(2,44),(4,44);
/*!40000 ALTER TABLE `auth_group_permission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_permission`
--

DROP TABLE IF EXISTS `auth_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_permission` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_permission`
--

LOCK TABLES `auth_permission` WRITE;
/*!40000 ALTER TABLE `auth_permission` DISABLE KEYS */;
INSERT INTO `auth_permission` VALUES (0,'admin_index_dashboard','View admin dashboard'),(1,'auth_admin-user_get','View an user'),(3,'auth_admin-user_post','Create an user'),(4,'auth_admin-user_put','Update user information'),(5,'auth_admin-user_index','View user index'),(6,'auth_admin-user_list','View user list'),(7,'auth_admin-group-permission_index','View permission per group'),(8,'auth_admin-group-permission_switch','Switch permission'),(9,'all','All'),(11,'resourcing_candidat_list','View resourcing index list'),(12,'resourcing_candidat_get','View an resourcing index'),(13,'resourcing_candidat_post','Create an resourcing index'),(14,'resourcing_candidat_new','Access to creation of an resourcing index'),(15,'resourcing_candidat_delete','Delete an resourcing index'),(16,'resourcing_candidat_put','Update an resourcing index'),(17,'resourcing_candidat_batch','Batch an resourcing index'),(18,'resourcing_candidat_switch','Switch an resourcing index'),(19,'resourcing_candidat_index','View liste candidat'),(20,'resourcing_candidat_search','search list candidat'),(21,'resourcing_candidat_detail','detail candidat'),(22,'resourcing_operation_new','add new entretien operationnel'),(23,'resourcing_direction_new','add new entretien directionnel'),(24,'resourcing_direction_get','edit entretiens direction'),(25,'resourcing_operation_get','edit entretiens operation'),(26,'resourcing_candidat_get','edit account user'),(27,'resourcing_operation_put','put operation data'),(28,'resourcing_operation_post','post data operation'),(29,'resourcing_direction_post','post data direction'),(30,'resourcing_direction_put','put direction data'),(31,'resourcing_candidat_luceneIndex','Indexation du contenu'),(32,'resourcing_events_create-event','Create or update event'),(33,'resourcing_events_index','show calendar'),(34,'resourcing_prequalif_new','Create prequalif entretien'),(35,'resourcing_prequalif_post','post prequalif entretien'),(36,'resourcing_prequalif_put','put prequalif entretien'),(37,'resourcing_prequalif_get','get info prequalif entretien'),(38,'resourcing_events_delete-event','delete event'),(39,'resourcing_candidat_export-candidats','export candidats liste'),(40,'resourcing_candidat_votes','vote for candidat'),(41,'resourcing_users_index','detail own information'),(42,'resourcing_users_get','edit own account'),(43,'resourcing_users_put','save own account information '),(44,'resourcing_candidat_download-cv','download CV for candidat');
/*!40000 ALTER TABLE `auth_permission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user`
--

DROP TABLE IF EXISTS `auth_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `first_name` varchar(30) DEFAULT NULL,
  `last_name` varchar(30) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(128) NOT NULL,
  `salt` varchar(128) DEFAULT NULL,
  `algorithm` varchar(128) NOT NULL,
  `can_be_deleted` int(1) unsigned NOT NULL DEFAULT '1',
  `is_active` int(1) unsigned NOT NULL DEFAULT '0',
  `is_super_admin` int(1) unsigned NOT NULL DEFAULT '0',
  `is_staff` int(1) unsigned NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_login` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_parent_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `can_be_deleted` (`can_be_deleted`),
  KEY `is_active` (`is_active`),
  KEY `is_super_admin` (`is_super_admin`),
  KEY `is_staff` (`is_staff`),
  KEY `email` (`email`),
  KEY `username` (`username`),
  KEY `fk_user__user_parent_id___user__id` (`user_parent_id`),
  CONSTRAINT `fk_user__user_parent_id___user__id` FOREIGN KEY (`user_parent_id`) REFERENCES `auth_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user`
--

LOCK TABLES `auth_user` WRITE;
/*!40000 ALTER TABLE `auth_user` DISABLE KEYS */;
INSERT INTO `auth_user` VALUES (1,'admin',NULL,NULL,'ce@octaveoctave.com','692a868f105e05b46f6f798ef3ec8554b7d658f3','a73056c2bbdca2d4148049493e296e70','sha1',0,1,1,0,'2009-11-23 11:36:31','2014-05-30 00:06:59','2014-05-30 12:06:59',NULL),(2,'anonymous',NULL,NULL,NULL,'',NULL,'',0,1,0,0,'0000-00-00 00:00:00','2010-11-29 03:17:19','2010-11-29 15:17:19',NULL),(3,'Hatem.Skik',NULL,NULL,'hatem.skik@keyrus.com','4e40bd01ae870aeb031c4a753e07ef3f322a5c97','ea838d1b9a753533c11ec0a09227627f','sha1',1,1,0,0,'2014-03-17 20:06:51','2014-05-16 00:35:07','2014-05-30 14:47:52',NULL),(4,'Zied.Belhadj',NULL,NULL,'zied.belhadj@keyrus.com','be38a11a209b3a4405d718977eb8a44a6cf9698a','f1e0bdd716a124859a4719763ca03aa2','sha1',0,1,0,0,'2014-03-17 20:07:34','2014-05-16 10:56:45','2014-05-16 15:30:08',NULL),(5,'Salah.Elabidi',NULL,NULL,'salah.elabidi@keyrus.com','84661fa1498d845e975faedc7321c093737ee67d','da09290016c7f343a3f3a66094c5f24f','sha1',0,1,0,0,'2014-03-17 20:08:15','2014-05-30 11:16:59','2014-05-30 11:23:47',NULL),(6,'Mourad.Benhamida',NULL,NULL,'mourad.benhamida@keyrus.com','0ed5e03ce39c59ce33cb5234c6f20e7063754144','14a5aae1bf537a2c17f41d7011d59a49','sha1',1,1,0,0,'2014-03-17 20:08:59','2014-05-16 03:43:12','2014-05-30 11:23:56',NULL),(7,'Asma.Ayari',NULL,NULL,'asma.ayari@keyrus.com','f4317ccb2154db85b6bb840265a0def7b7c98b64','8bd543c27ec8080fe85c6606b324eacf','sha1',0,1,1,1,'2014-04-18 17:42:57','2014-05-30 02:41:16','2014-05-30 14:41:16',7),(8,'othmen.tahri',NULL,NULL,'othmen.tahri@keyrus.com','43c78a01cfc5c28aeca3454c7e9c63bd7ba73782','629283aa5594c86f3959938714423749','sha1',0,1,1,1,'2014-05-07 11:29:17','2014-05-30 11:23:15','2014-05-30 11:23:15',NULL),(9,'Mehdi.Skik',NULL,NULL,'mehdi.skik@keyrus.com','57475e3a7735eb43eac22609cfadb017c534b4c5','8a9ae21bd27e715cff7956fc25a4438b','sha1',0,1,0,1,'2014-05-08 15:29:06','2014-05-26 00:17:10','2014-05-26 12:17:10',NULL),(10,'Talel.Kaabachi',NULL,NULL,'talel.kaabachi@keyrus.com','590e5ec75e98f9a26cff0f20ae014010d6d472ae','9c22c5aaa9ba62c53b0b9a558ce7ee19','sha1',0,1,0,0,'2014-05-13 17:16:45','0000-00-00 00:00:00','2014-05-30 14:47:25',NULL),(11,'Ahmed.Sahli',NULL,NULL,'ahmed.sahli@keyrus.com','aa5e658a4fc88fcaf3f1b6a751090ceb1163281c','7f4671f5b20b4f47dfd243038d986220','sha1',0,1,0,0,'2014-05-21 11:23:50','2014-05-26 04:46:19','2014-05-30 14:47:08',NULL);
/*!40000 ALTER TABLE `auth_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user_permission`
--

DROP TABLE IF EXISTS `auth_user_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user_permission` (
  `user_id` int(11) unsigned NOT NULL,
  `permission_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`user_id`,`permission_id`),
  KEY `fk_persmission__action_id___action__id` (`permission_id`),
  CONSTRAINT `fk_permission__user_id___user__id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_persmission__action_id___action__id` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user_permission`
--

LOCK TABLES `auth_user_permission` WRITE;
/*!40000 ALTER TABLE `auth_user_permission` DISABLE KEYS */;
INSERT INTO `auth_user_permission` VALUES (1,1),(8,1),(9,1),(8,3),(8,4),(8,5),(8,6),(9,6),(8,7),(8,8),(8,9),(8,11),(8,12),(8,13),(8,14),(8,15),(8,16),(8,17),(8,18),(8,19),(9,19),(8,20),(9,20),(8,21),(9,21),(4,22),(8,22),(9,22),(8,23),(9,23),(8,24),(9,24),(8,25),(9,25),(8,26),(9,26),(8,27),(9,27),(8,28),(9,28),(8,29),(9,29),(8,30),(9,30),(1,31),(2,31),(4,31),(7,31),(8,31),(1,32),(2,32),(4,32),(7,32),(8,32),(9,32),(1,33),(2,33),(4,33),(7,33),(8,33),(9,33),(9,35),(9,36),(9,38),(1,39),(4,39),(7,39),(8,39),(9,39);
/*!40000 ALTER TABLE `auth_user_permission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `avantage`
--

DROP TABLE IF EXISTS `avantage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `avantage` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `avantage` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `avantage`
--

LOCK TABLES `avantage` WRITE;
/*!40000 ALTER TABLE `avantage` DISABLE KEYS */;
INSERT INTO `avantage` VALUES (1,'Tickets resto'),(2,'Assurance Groupe'),(3,'Tél'),(4,'Forfait Tél');
/*!40000 ALTER TABLE `avantage` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `candidat`
--

DROP TABLE IF EXISTS `candidat`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `candidat` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type_candidat` enum('Candidat','Profil') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Profil',
  `civilite` enum('Mr','Mlle','Mme') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Mr',
  `last_name` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `first_name` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tel` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tel2` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nationalite` text COLLATE utf8_unicode_ci NOT NULL,
  `residence` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `id_domaine` int(11) DEFAULT NULL,
  `poste` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `ecole` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_experience` int(11) DEFAULT NULL,
  `statut` enum('Etudiant','En poste','A la recherche du travail') COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_etude` int(11) DEFAULT NULL,
  `societe` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `societe_precedente` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `competences_secondaires` text COLLATE utf8_unicode_ci,
  `id_debut_carriere` int(11) DEFAULT NULL,
  `certifie` enum('Oui','Non') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Non',
  `certification` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_sourcecv` int(11) DEFAULT NULL,
  `cv` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `photo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `commentaire` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `id_domaine_idx` (`id_domaine`),
  KEY `id_ecole_idx` (`ecole`),
  KEY `id_experience_idx` (`id_experience`),
  KEY `id_etude_idx` (`id_etude`),
  KEY `id_debut_carriere` (`id_debut_carriere`),
  KEY `id_sourcecv` (`id_sourcecv`),
  CONSTRAINT `FK_id_domaine` FOREIGN KEY (`id_domaine`) REFERENCES `domaine` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `id_debut_cariiere` FOREIGN KEY (`id_debut_carriere`) REFERENCES `debut_carriere` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `id_experience` FOREIGN KEY (`id_experience`) REFERENCES `experience` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=137 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `candidat`
--

LOCK TABLES `candidat` WRITE;
/*!40000 ALTER TABLE `candidat` DISABLE KEYS */;
INSERT INTO `candidat` VALUES (88,'Candidat','Mr','MAHFOUDI','mahfoudimohamed@yahoo.fr','Mohamed','0033763167440','0021693006140','Tunisienne, Française','36/38 avenue  Champs Pierreux,  92000 Nanterre',3,'Consultant SAP BI','ISI',6,'En poste',8,'Faurecia','AZUR SYSTEME','•	Maîtrise des techniques fondamentales du Business Intelligence (ETL, Data Warehousing, OLAP, Reporting, ABAP,  etc.);\r\n•	Conception et élaboration des tableaux de bord;\r\n•	Mise en œuvre des flux d’alimentation de l’entrepôt de données ;\r\n•	Connaissances des bases fonctionnelles et techniques des domaines de la Finance, Manufactsuring et Purchasing and Sales dans SAP;\r\n•	  Recueil des besoins des utilisateurs et rédaction des spécifications fonctionnelles et techniques;\r\n•	  Documentation et accompagnement des utilisateurs;\r\n•	Formation des nouveaux embauchés sur les outils techniques existants.\r\n',22,'Oui','Certificat CCNA1 « NETWORKING BASICS » de CISCO Systems Networking Academy',NULL,'Consultant SAP BI _Mahfoudi Mohamed.docx','mohamed_mahfoudhi.png','	Système d’Exploitation: Windows, UNIX/LINUX.\r\n	Logiciels d’aide à la décision : SAP BW 7.0, Business Object, Microsoft BI (SSIS, SSAS, SSRS, MDX), Suite Bex. (Query Desiner, Report designer and Web appplication designer), SQL Server 2005-2008, Crystal report, Report builder, Talend, Pentaho (Kettle, Mondrian, Jasper Reports).\r\n	Conception: Merise I/II, UML, Modélisation des SI décisionnels (étoile/flocon).\r\n	Outils de conception: Power Designer, Rational Rose, Mega 2009.\r\n	Programmation: ABAP (langage de développement SAP), C/C++, Java/JE22 (AWT, Swing, RMI), VB.\r\n	Bases de données: ORACLE (8i, 9i, 10g), MySQL, PostgreSQL, SQL Server, Access.\r\n	Gestion de projet : Gantt Project, Ms Project.\r\n	Web: XML, JSP, JSF, PHP, HTML, JavaScript, CSS, ASP.NET.\r\n'),(89,'Candidat','Mr','Ltifi','imedltifi@gmail.com','Imed','27780474','54115454','Tunisienne','El Omrane Sup, Tunisie',3,'Consultant BI','FST',6,'En poste',5,'BGFi Engineering','Société Magasin Générale',' Aide au choix, migration, modélisation, optimisation, déploiement et sécurisation des plates-formes BI  (IBM Cognos BI,\r\nMicrosoft BI).\r\nInformatique décisionnelle                         IBM Infosphere DataStage, Data Manager, Framework Manager, report studio, Transformer, SSIS, SSRS.\r\nLangages	  			 JAVA, VB, C, C++, VB.net.\r\nWeb                                                                Html, CSS, DHtml, Xml, java script, PHP.\r\nOutils de Conception et Développement   Power Amc11, Rational Rose, MagicDraw.\r\nMéthodologies 			         	UML, Merise, modélisation des SI décisionnels (Modélisation en étoile/en flocon)\r\nSystèmes de base de données   	Mysql, Oracle (SQL, PL/SQL, forms, reports), SQL server (Transact-SQL), Access,  MS Reporting Services.\r\nSystèmes d’exploitation 	         	 	Ms Dos, Windows / Linux			   	\r\nBureautiques			        	Ms. Office (Word, Excel, Power Point, Outlook, Access).\r\nGPAO			        	               Prélude Production4\r\nConnaissance Réseaux                             Ethernet, Protocoles: TCP/IP.\r\n                                             Réseaux Locaux : topologies, câblage.\r\n                                                    Sécurité des Réseaux Informatiques :   Attaques, virus ;\r\n',22,'Oui','IBM Cognos 10 BI Metadata Model Developer, IBM Certified Designer - Cognos 10 BI Reports, IBM Certified Developer - Cognos 10 BI OLAP Models',NULL,'CV IMED LTIFI - 2014.doc','ltifi_imed.png','Attitude personnelle :\r\n       Dynamique, passionné par l’informatique.\r\n       Grande capacité d’apprentissage et d’intégration.\r\n       Ambitieux, rigoureux, respect du travail en équipe, esprit de créativité.\r\nDivers :\r\nInternet.  \r\nCinéma.\r\n Football\r\n'),(90,'Candidat','Mr','Ben haha','ben.haha.med.mehdi@gmail.com','Med Mehdi','55454533',NULL,'Tunisienne','Zone 84, Rue Al Ezdihar, Impasse Al Weha 8030 Grombalia, Nabeul',5,'Ingénieur études et développement JAVA/J2EE','INSAT',6,'En poste',7,'SunGard Global Trading','Cylande Africa','Développement J2EE : JSP, JSF, Facelets, Struts, Hibernate, JPA,\r\nEJB 3, spring, Richfaces\r\n.Net: Windows forms, ASP.net, ADO.net.Linq\r\nProgrammation Java, C#, C/C++, PHP, pascal, PL SQL, Jelly\r\nConception SI UML 2.0, MERISE, RUP, 2TUP, SCRUM\r\nBases de données MySQL, SQL server 2005, Derby',22,'Oui','MCP : Certification Microsoft 70-536 : .NET Framework 2.0 — Application Development Foundation',NULL,'cv Med Mehdi Ben haha.pdf','mehdi_benhaha.png',NULL),(91,'Candidat','Mr','GHORBEL','bilel.ghorbel@outlook.com','Bilel','27501460',NULL,'Tunisienne','La petite Ariana',4,'Ingénieur études et de développement JAVA/J2EE confirmé','ENSI',6,'En poste',7,'IP-TECH',NULL,NULL,24,'Oui','Oracle Certified Java Programmer 6 (OCJP) avec un score de 98%',NULL,'CV_bilel-ghorbel.pdf','bilel_ghorbel.png',NULL),(92,'Candidat','Mr','FAKHFAKH','Marouen.fakhfakh@engineer.com','Marouen','0033669770337','23212543','Tunisienne, Française','6 rue de mekka 2010 , Mannouba',NULL,'Ingénieur en Informatique','ESPRIT',6,'En poste',8,'ATOSWORDLINE','BGI Tunisie','Ingénieur informatique financière – ATOSWORDLINE , France\r\nStage de fin d’étude, Implémentation de nouveaux services dans la partie Front-End de l’application de gestion de portefeuille. GWT, SPRING, HIBERNATE, SYBASE, JBOOS 6.\r\n\r\n',22,'Non',NULL,NULL,'Marouen FAKHFAKH.pdf','marouen_fakhfekh.png','Programmation\r\n: C++, C#, Java, Java EE 6/5, Javascript, VBA, Matlab, XML, PLSQL,\r\nBase de données\r\n: Oracle, Sql server, MySql, Sybase\r\nMathématiques et Finances\r\n: EDP, Stochastiques, Modèles Discrets et Continus, Finance du marché,\r\nCalcul actuariel, Analyse numérique.'),(93,'Candidat','Mme','LATIRI','samiamorjane@yahoo.fr','Samia','93049592',NULL,'Franco-Tunisienne','23 bis rue Taïeb Mhiri Carthage Byrsa',6,'Responsable Organisation SI','BTS Assurances',7,'En poste',6,'GAT Assurances, Compagnie d\'assurances et de réassurance privée','WECA GROUP','Responsable Organisation SI\r\nResponsable Web\r\nDirectrice de projets et Chef de projets\r\nResponsable de la production et Chef de projets\r\nChargée d’Etudes Organisation (cadre)\r\nSuperviseur pôle Prestations\r\nGestionnaire confirmée',7,'Non',NULL,NULL,'Samia LATIRI.pdf','samia_latiri.png',NULL),(94,'Candidat','Mr','LABBENE','labbeneabdelaziz@gmail.com','Abdelaziz','0021650814315',NULL,'Tunisienne','Tunisie',3,'Consultant Microsoft SharePoint et Business Intelligence','ESSTT',7,'En poste',8,'DOTIT','LATICE','Programmation : C#, ASP.Net, SQL, Transact-SQL, PL/SQL, HTML4 et 5, JavaScript, JQuery, CSS, XML, WCF, LINQ, ASP .Net MVC2, Entity, Asp.Net MVC4 (Web API).\r\nSGBD : Microsoft SQL Server 2008/2008R2/2012, Oracle, MySQL.\r\nPlatforme Cloud: Windiows Azure.\r\nPlatforme Server: Windows Server 2008, 2008 r2, 2012, 2012 r2\r\nSolution de collaboration: Microsoft SharePoint 2010, SharePoint 2013, SharePoint Designer, Office 365/SharePoint Online.\r\nSuite BI de Microsoft:\r\n* ETL: SQL Server Integration Services (SSIS).\r\n* Cubes: SQL Server Analysis Services (SSAS), MDX, DAX.\r\n* Reporting: SQL Server Reporting Services (SSRS).\r\n* Self-Service BI: Excel 2010, 2013, Power BI.\r\n* BI avec SharePoint: Visio Services, Excel services, PerformancePoint Services, Reporting Services, KPIs.\r\nBig Data (Initiation): Hadoop, MapReduce, xVelocity, HDInsight, Hive, HBase, PDW\r\nEnvironnement technique: Visual Studio, SQL Server Data Tools, Business Intelligence Development Studio, Team Foundation Server, Microsoft Report Builder, PerformancePoint Dashboard Designer.\r\nAutres connaissances: Java, Android, Php4, Flex.',21,'Oui','Microsoft Certified Professionnal',NULL,'Abdeaziz Labbene.pdf',NULL,'Certifications:\r\nMicrosoft Specialist: Developing ASP.NET MVC 4 Web Application 70-486.\r\nMicrosoft Specialist: Programming in HTML5 with JavaScript and CSS3 70-480.\r\nMicrosoft Technology Specialist, Microsoft SharePoint 2010, Application Development 70-573.\r\nMCSA Certified Solutions Associate: SQL Server 2008.\r\nMicrosoft Technology Specialist, Microsoft SQL Server 2008, Business Intelligence Development and Maintenance 70-448.\r\nMicrosoft Technology Specialist, Microsoft SQL Server 2008, implementation and maintenance 70-432.'),(95,'Profil','Mlle','Arfaoui','faten.arfaoui.90@gmail.com','Faten','0021623990739',NULL,'Tunisienne','Sanheja, 05, Rue Mohamed El kasab, CP :2021 ,Manouba,Tunisie',4,'Ingénieur en Informatique','ESSTT',5,'A la recherche du travail',7,'ESSTT',NULL,'Langage : C, C++, Shell, Java, VHDL, Pascal,SQL, PL/SQL, Qt Creator, HTML 5, XML, CSS, PHP, jQuery.\r\nBase de données : MySQL server , Oracle, Microsoft SQL Server. Technologies : J2EE (Hibernate,EJb,JSF), Ajax , symfony2 .\r\nModélisation :UML2, Design patterns, Co-design, Méthode B.\r\nSystème d’exploitation: Programmation noyau sous Unix , Shell LEX ,MAKE FILE',24,'Non',NULL,NULL,'ArfaouiFatenCV.pdf','faten_arfaoui.png',NULL),(96,'Candidat','Mr','HOSNI','hosniaymen@yahoo.fr','Aymen','0021627943343',NULL,'Tunisienne','Tunisie',4,'Développeur JAVA','INSAT',6,'En poste',7,'Talan Tunisie International',NULL,'Compétences sectorielles\r\n* Géo-localisation\r\n* Télécom\r\nCompetences fonctionnelles\r\n*X Telecom operator BSS\r\n*X Systeme de facturations\r\nServeurs d’applications & Progiciels\r\n* JBoss AS\r\n* Apache Tomcat\r\nMéthodologies\r\n* UML\r\n* SCRUM\r\n* XP\r\nLangages & Outils de developpement\r\n*X Java / C / PHP / Objective-C\r\n*X Eclipse / Visual Studio / Xcode\r\n*X Spring\r\n*X Hibernate\r\n*X EJB\r\n*X JSF\r\n*X Apache CXF/Axis\r\n*X JavaScript\r\n*X SQL\r\n\r\nMateriel & Systemes d¡¦exploitation\r\n*X Windows\r\n*X Unix / Linux\r\n*X Mac OS\r\n\r\n« Bases de donnees relationnelles\r\n*X MS Access\r\n*X MySQL\r\n*X SQLite\r\n*X Oracle',24,'Non',NULL,NULL,'Aymen HOSNI.pdf',NULL,NULL),(97,'Candidat','Mr','NOUIRI','oussama.nouiri@gmail.com','Oussama','0629506468',NULL,'Tunisienne','Paris',4,'Chef de projet','ESTI',7,'En poste',7,'SISLEY-Paris','SEPHORA','Gestion et pilotage des projets\r\nMS Project\r\nMéthodologie et formalisme\r\nMERISE\r\nXP - Agile\r\nLangages de modélisation\r\nUML\r\nLangages de programmation, langages de\r\nmarquage\r\nC#, VB.NET, php\r\nHTML, XML, XSL, XSLT\r\nSQL, PL/SQL, Transact SQL\r\nOutils de Développement /ERP\r\nMs Visual Studio, Ms Sharepoint.\r\nORACLE 8.i et 9.i\r\nSQL SERVER 2000-2005 et 2008.\r\nCommerce Server 2007 et 2009.\r\nOracle ATG\r\nMagento\r\nMovex\r\nSAP\r\nSAP Business One',18,'Oui','MCP Microsoft Certified ASPNet 2.0',NULL,'CV NOUIRI Oussama.pdf','oussama_nouri.png','Certifications\r\nOtcobre 2013\r\nFormation Gestion et pilotage de sites\r\nE-commerce chez E-commerce\r\nAcademy\r\nAoût 2009\r\nAgile Training Center : Session atelier\r\npilote organisée au MTC Paris\r\n(Microsoft Technical Center):\r\nFormation, conçue et encadrée par\r\nFrédéric Queudret.\r\nJuillet 2008\r\nMCP Microsoft Certified ASPNet 2.0'),(98,'Profil','Mr','Halaoui','habib.halaoui@gmail.com','Habib','0021620100492',NULL,'Tunisienne','Tunisie',4,'Développeur','INSAT',4,'A la recherche du travail',7,'INSAT',NULL,NULL,26,'Non',NULL,NULL,'CV_habib-halaoui-2014-01.pdf','habib_halaoui.png',NULL),(99,'Profil','Mlle','Hamed','hamed.khaoula@gmail.com','Khaoula','0021624175401',NULL,'Tunisienne','11 Rue Socrate Sidi Amor Mannouba',4,'Développeur','INSAT',4,'A la recherche du travail',7,'INSAT',NULL,'Programmation: C ,C++,SHELL (Unix), C#, .NET, JAVA ,J2EE ,(STRUTS, SPRING,Hibernate,MVC...)\r\n-Programmation mobile : Android,IOS,Windows Phone,PhoneGap,...\r\n-Technologies web: HTML5,CSS,PHP,Javascript,XML,JSP,SVG,jQuery...\r\n- Langages de programmation logique : PROLOG.\r\n- Système et Réseau : Administration de bases de données, Audit sécurité des systèmes d\'information,\r\nSystèmes temps réel.\r\n-Méthodes de conception: Merise, UML, 2TUP, RUP, PU, Agile, Scrum.\r\n-Qualité Logiciel : ISO 9001, CMMI.\r\n-Programmation bas niveau : STM32, CAN.\r\n-Embarqué: Os embarqués, Simulation/Co-simulation,Comp/Cross compilation, ST20,ST40,ARM,gcc,\r\ngdb,sh4-linux-gcc,...\r\n-Business Intelligence: JasperServer,IReport,Talend Open Studio...\r\n-E-service:Service web,SOAP,ESB,BPEL,...',26,'Non',NULL,NULL,'CV_Hamed_Khaoula.pdf',NULL,'Framework et logiciels :\r\n-Environnements de développement: Microsoft Visual Studio, Eclipse, Eclipse WTP, NetBeans\r\n-Serveurs: Apache Tomcat, Easyphp,WampServer...\r\n-SGBD: Access, MySQL,SQL Server,Postgresql.\r\n-Les serveurs : HTTP, FTP, TELNET, Postfix, DNS, DHCP,LDAP…\r\n-Programmation RPC, Sockets,Middlware,MOM,RMI ,CORBA,JMS…'),(100,'Candidat','Mr','KHECHRIF','zied.khechrif@gmail.com','Zied','0021655207079',NULL,'Tunisienne','84 Rue de Yougoslavie 1001 Tunis',4,'Chef de projet','Université Libre de Tunis',7,'En poste',6,'Karismatik','Offshore Interactive','COMPETNCES:\r\n- Sens du détail\r\n- Communication\r\n- Négociation\r\n- Leadership\r\n- Management\r\n- Travail en équipe\r\n- Gestion d’équipe\r\n- Gestion des priorités\r\n\r\nOutil informatique : Windows, Microsoft Visio, MS office, MacOs, OpenOffice, Internet (Internet Explorer, FireFox), Dreamweaver, Photoshop, Axure, Mantis bug tracker, jira, redmine\r\n\r\nLangages étudiés :\r\nC, C++, Java, HTML, ASP, PHP\r\n\r\n',17,'Non',NULL,NULL,'CV_Zied-Khechrif.pdf','zied_khechrif.png','Chef de projet Senior – Agence Karismatik – Tunis, Tunisie\r\nGestion de 15 ressources\r\n● Analyse cahier des charges\r\n● Rédaction cahier des charges\r\n● Etude fonctionnelle\r\n● Estimation temps/délais\r\n● Elaboration de planning\r\n● Pilotage de l’équipe de développement (gestion de planning et des ressources)\r\n● Pilotage des réalisations confiées aux prestataires ou intégrateurs externes (gestion des plannings, livrables techniques, documentations, versions, formations etc.)\r\n● Respect des délais\r\n● Contrôle/validation des développements\r\n● Vérification de conformité au cahier des charges\r\n● Définition des priorités\r\n● Responsable de production du pole retouche photo, gestion de 5 personnes'),(101,'Candidat','Mr','Sahbeni','Sahbani.tarek@gmail.com','Tarek','0021625983796',NULL,'Tunisienne','Tunis Aouina (2045) cite el chawche N°9',4,'Ingénieur de développement Java/J2EE','FST',6,'En poste',7,'Popsicube','Vermeg','Langage des programmations:\r\nJava (J2SE, J2EE), SQL, PL/SQL, C, C++, Latex,\r\nHTML/XML, JavaScript, Servlet, JSP, JSF, JSTL, EJB3.X,\r\nSOAP, WSDL, UDDI, SAX, JAXB, Web Services, AOP\r\n\r\nOutils\r\nRational Rose, Power AMC, Magic Draw UML, WTP, JPA,\r\nSpring, Hibernate, Ajax, JUnit, Axis2, Maven, Ant,\r\nTomcat, Glass Fish, JBoss5, IBM WebSpehre7, BEA Web\r\nlogic, NetBeans, Eclipse3.x, JBuilder, Jasper Reports\r\n\r\nModel MVC, Model DAO, Model Factory, IOC, UML,\r\nMerise2, CRM, Client/server architecture, Web N tiers,\r\nsockets, Scrum, TCP, UDP, FTP',22,'Oui','Sun Certified Java Associate SCJA (CX-310-019), Sun Certified Java Programmer SCJP (CX-310-065)',NULL,'CVSahbaniTarekFR.pdf',NULL,NULL),(102,'Profil','Mlle','DHAHA','dhaha.sarra@gmail.com','Sarra','0021622200562',NULL,'Tunisienne','Cité olympique 1003 Tunis, Tunisie',9,'Management des systèmes d’informations','INSAT',4,'A la recherche du travail',7,'Tunisiana',NULL,'Compétences\r\nLangages de développement\r\nC, C++, C#, Java, SQL, Borland\r\nTechnologies & Framework\r\nJEE (Spring, Hibernate, JSF, Struts, JPA, EJB 3, Primefaces),\r\n.NET (ASP.NET, ADO.NET, Entity Framework), Web Services, E-Services, SharePoint\r\nConception et Architecture\r\nUML, GOF Design Patterns, J2EE Design Patterns, Architecture MVC, Architecture SOA\r\nBase de données\r\nMicrosoft Access, Oracle, PostgreSQL, MySQL, SQL Server 2008\r\nSystèmes d’exploitation\r\nMicrosoft Windows, Linux\r\nProtocoles\r\nProtocole FIX\r\nOutils\r\nEclipse, Netbeans, Visual Studio, WampServer, Rational Rose, CodeBlocks, StarUML, PgAdmin III, Microsoft Office, PowerPivot, Unity 3D, OpenESB,\r\nMéthodologies\r\nQualité Scrum, Méthodes agiles ISO 9001, CMMI\r\nOutils BI\r\nTalend, IReport, Jaspersoft, Pentaho, QlickView',26,'Non',NULL,NULL,'dhaha_sarra cv.pdf',NULL,NULL),(103,'Profil','Mlle','BOUZID','Emna.Bouzid@gmail.com','Emna','0021622971806',NULL,'Tunisienne','Route Menzel Chaker km 1,5 Sfax Tunisie',4,'Ingénieur en informatique','ENIS',4,'A la recherche du travail',7,'Société I-Way','SUNGARD','CONNAISSANCES INFORMATIQUES\r\nLangages de Programmation : Java/ J2EE (Spring, EJB, Struts, JSF, Annotations personnelles, Hibernate), C#, C/C++, assembleur, windev. Programmation Web : Html/CSS, Html5,PHP, Ajax, Javascript, xml, xsl, applet,JSP, Servlet. Base de Données : MYSQL ,Oracle 10g Modélisation: UML(StarUml,POWERAMC),Mérise(AnalyseSI),Design Patterns. Logiciel de calcul Numérique : Matlab, Maple. Impression d’état : Crystal Reports OS Mobile : Android Environnement de développement : Windev, Visual studio 2008,XML Spy,Eclipse,Netbeans. Autres Connaissances : CORBA, Programmation orientée (Composants,Web Service)',25,'Non',NULL,NULL,'Emna BOUZID.pdf','emna_bouzid.png','FORMATIONS ET ACTIVITES\r\n-Membre actif des Clubs .NET et android et un ancien membre de AIESEC. -Formation android effectuée par la société OrangeTunisie. -Formation en anglais avec l’AIESEC. -Participation au Workshop « Yasmine Open Day 7 ». -Lecture de Documents et d’articles, la musique, le sport. -Certificats de Présence à la journée Software Freedom Day (SFD)\r\n2012 et à Google IO Extended 2012.'),(104,'Profil','Mlle','BOULARES','boularesmanel@gmail.com','Manel','0021623485252',NULL,'Tunisienne','Rue Habib Thameur, el Aouina, el Metline 7034 BIZERTE',4,'Développeur','INSAT',4,'A la recherche du travail',7,'INSAT',NULL,'Domaines de compétences\r\nSystèmes d\'exploitation: Windows XP/Vista Unix/Linux, Ubuntu.\r\nLanguages de programmation: C, C++, Java, Pascal, Visual Basic.\r\nFrameworks et plateformes : ASP.Net-ADO.net (Entity Framework)-Spring-Struts-\r\nHibernate-Android\r\nMultimedia: HTML, JavaScript, XML, CSS, FLASH, Dreamweaver, Photoshop.\r\nBase de données : Oracle, MySQL, Microsoft SQL Server, Access\r\nConception et modélisation : UML et merise\r\nBusiness Intelligence : Talend (intégration des données , ESB), JasperServer & IReport\r\n(création des rapports personnalisés), Mondrian (serveur OLAP pour l’analyse et\r\nl’extraction de données)\r\nMultimédia : Illustrator, Unity 3D, photoshop\r\nConnaissances acquises : Architecture des ordinateurs, J2EE (framework struts,\r\nframework Spring), gestion, Merise, création des sites web, réseaux IP (plan\r\nd’adressage, routage, sécurité...), utilisation du Nmap et packet tracer, Business\r\nintelligence , intelligence artificielle ,fouille des données …',26,'Non',NULL,NULL,'ManelBoulares_CV.pdf',NULL,'Membre Actif de club Insat Android Club.\r\nMembre Actif de club Insat Microsoft Club'),(105,'Candidat','Mr','BEN TERDAYET','mehdi.ben.terdayet@gmail.com','Mehdi','0021622321838',NULL,'Tunisienne','Tunisie',4,'Ingénieur études et développement JAVA/J2EE','ENSI',6,'En poste',7,'OALIA','OALIA Paris','Domaines de compétence\r\nPlateformes :\r\nVisual Studio .Net 2008/2010, Eclipse, Netbeans.\r\nLangages de programmation :\r\nC, C++, Qt, C# , JAVA, Pascal, Assembleur8086, Spim(MIPS), Visual Basic, Programmation système\r\nsous linux.\r\nWeb et Multimédia :\r\nHTML, XHTML, XML, CSS, DTD,HtmlUnit, Dreamweaver,javascript, JSP, Velocity, Ajax,\r\nFrameworks:\r\nJ2EE, JSF, Struts, Hibernate, XNA3.1 (C#), Spring,Junit, Selenium .\r\nSystèmes d’exploitation :\r\nWindows (98, NT, 2000, XP, Vista ,Seven), Linux/Unix.\r\nBases de données :\r\nAccess, PL/SQL, MySQL, administration Oracle10g, PostgreSql.\r\nConception et modélisation :\r\nUML2.0.',23,'Oui','Certificat informatique et internet (C2i), Test of English for International Communication (TOEIC)',NULL,'Mehdi Ben Terdayet--fr.pdf','mehdi_ben_terdayet.png',NULL),(106,'Candidat','Mr','Gharbi','mohamed.gharbi@gmail.com','Mohamed','0021650032031',NULL,'Tunisienne','Tunisie',4,'Ingénieur développeur JAVA/J2EE','FST',6,'En poste',7,'STACI','EUROPCAR','Compétences Techniques\r\nSystème : Windows, Linux.\r\nSGBD : Oracle, MySQL, ACCESS, POSTGRES.\r\nLangage : Java, C, PL SQL, HTML, PHP, JavaScript,CSS.\r\nTechnologies :\r\nJ2EE, Hibernate, Struts, Spring, RCfaces, XML, AJAX,\r\nJDBC, JSP, SERVLET, LIFERAY\r\nPortal.\r\nLogiciels : Eclipse, SVN, SQL DEVELOPPER, QC/QTP.\r\n\r\nMéthodes et Outils associés\r\nUML, architecture n-tiers.',23,'Oui','Oracle Certified Professional, JAVA SE 6 Programmer',NULL,'Mohamed GHARBI.pdf',NULL,NULL),(107,'Candidat','Mr','Ibrahimi','said.ebrahimi.insat@gmail.com','Said','0021620112286',NULL,'Tunisienne','AVENUE : JAFER IBN ABI TALEB Cité EL-Khadhra 1003 – Tunis - TUNISIE',4,'Recherche : Stage PFE','INSAT',4,'Etudiant',7,'INSAT',NULL,'Compétences\r\nConcepts: SOA, Architectures Réparties (RMI, Corba), Qualité\r\nl ogicielle (ISO, CMMI), MVC, Intelligence Artificielle\r\nBases de données: MySQL, Access, SQLite, PostgreSQL, SQL\r\nServer, Oracle\r\nLangages: c/ c++, c#, Java, Python\r\nModélisation: Merise, UML, MDA, Design\r\nPattern\r\nMéthodologie: RUP, Agile (SCRUM)\r\nTechnologies Web : XHTML/HTML, CSS, JS, PHP, XML,\r\nPHP5, AJAX, XPATH\r\nPlateforme J2EE: J2EE, Hibernate, JPA, EJB3, JSF,\r\nPrimeFaces, Spring\r\nPlateforme .NET : Win Forms, ASP.net, Web Forms, Entity\r\nFramework, LinQ\r\nTechnologies Mobile : Android, IOS, WP7 et WP8, Web\r\nMobile (HTML5, CSS3, jQueryMobile …)\r\nOutils: NetBeans, Eclipse, Microsoft Visual Studio, Rational\r\nRose, StarUML, JasperReport, iReport, Talend open studio.\r\nCulture de l’entreprise: Comptabilité, Droit, Economie,\r\nMarketing, Sociologie, GRH, Gestion Financière , gestion de\r\nprojet , Management Stratégique',26,'Non',NULL,NULL,'Saîd_IBRAHIMI.pdf',NULL,NULL),(108,'Candidat','Mr','SOMAI','sofiene.somai@gmail.com','Sofiane','0021699430021',NULL,'Tunisienne','Mohamed Ali street MORNAGUIA 1110 Tunisia',3,'IT Developer Insurance Consultant','ENSI',7,'En poste',7,'Vermeg-BSB','Tadis Tunis SA','Skills\r\n General Skills\r\n1. Technical Specification and\r\nWriting of Associated Documents,\r\n2. Designing and Programming,\r\n3. Testing, Maintenance, Validation,\r\n4. Production and automatisation of tasks,\r\n5. Writing manuals,\r\n6. Optimization of algorithms and\r\n7. programs.\r\n Technical knowledge\r\nC/C++/C#, JAVA, UML, SQL, PL/SQ, TSQL,\r\nXML, Hibernate, Spring, Struts\r\nFramework, JSP, JSF, Javascript...\r\n Functionnal knowledge\r\nMultimedia, Industrie, Insurance, Finance,\r\nTelecom…\r\n Tools\r\nMS Visual Studio 2005, Eclipse, Intellij\r\nIDEA, JBoss, Apache Tomcat, Subversion,\r\nTortoiseSVN, Maven, Sonar, Jira, Mantis,\r\nMS office,DB2, MS SQL Server, Oracle,\r\nMySQL, TOAD …',20,'Non',NULL,NULL,'Sofiane SOMAI.pdf','sofiane_soumai.png',NULL),(109,'Profil','Mr','ZAGRARNI','zag.med.nidhal@gmail.com','Mohamed Nidhal','0021621057847',NULL,'Tunisienne','03 Rue 10116 2053, Kabaria.TUNIS',4,'Ingénieur en Génie Logiciel','INSAT',4,'Etudiant',7,'INSAT',NULL,'Compétences\r\nSystèmes d’exploitation : GNU/Linux, Windows XP, Windows 7.\r\nLangages de programmation : C, C++, JAVA, PASCAL, C#, PL*SQL,EPL.\r\nBases de données : MySQL, PostgreSQL, MS SQL Server, MS Access, Apache Derby, Oracle.\r\nTechnologies Web : HTML4/5, CSS, PHP, Javascript, jQuery, Ajax, XML, J2EE, .NET.\r\nCMS : Joomla, Wordpress.\r\nTechnologie JEE : JSP, Servlet, Struts, Spring, Hibernate, JSF,EJB.\r\nTechnologie Microsoft : ASP .Net, C#.\r\nMéthodologie : SCRUM, Agile.\r\nOutils de modélisation : UML, Merise.\r\nRéseaux : Modèle TCP/IP, Modèle OSI.\r\nIDE: Eclipse, Netbeans, Visual Studio 2010, PowerAMC, ArgoUML, Rational-Rose,\r\nAnalyseSI,Code::Blocks, Dev-C++.\r\nBusiness Intelligence: Talend, Pentaho, Qlikview, iReports, JasperServer, Oracle Devlopper Suite,\r\nOracle Business Intelligence (OBIEE), Oracle Data Integrator.\r\nMultimedia: Unity 3D.\r\nECM et WORKFLOW: SharePoint, Activiti, Alfresco, Esper, Vaadin.\r\nQualité: ISO 9001, CMMI.',26,'Non',NULL,NULL,'ZAGRARNI Mohamed Nidhal.pdf',NULL,NULL),(111,'Candidat','Mr','BOUZID','wassef.bouzid@gmail.com','Wassef','53105200',NULL,'Tunisienne','Résidence La lune du Lac – Lac2',11,'Programmeur SAS','CNAM',6,'En poste',8,'Datametrix','Zitouna Takaful',NULL,23,'Non',NULL,7,NULL,NULL,NULL),(112,'Candidat','Mr','ESSID','fehdessid@gmail.com','FAHD','21252965',NULL,'Tunisienne','03 Rue Abebssa Menzel Abdel Rahmen 7035 Bizerte',3,'Ingénieur Informatique','Ecole Polytechnique de Tunisie',5,'En poste',7,'Opalia Pharma-','Fuba Printed Circuits',NULL,24,'Non',NULL,5,'Fahd ESSID.pdf',NULL,NULL),(113,'Candidat','Mr','KOUCH','ziedkouch@gmail.com','Zied','20771987',NULL,'Tunisienne','Résidence Diar Sokra,l\'Aouina 2045,Tunis,Tunisie.',6,'Développeur','Esprit',6,'En poste',7,'Réciprocité','Medisys Consulting',NULL,23,'Non',NULL,5,'Zied KOUCH.pdf',NULL,NULL),(114,'Candidat','Mr','GARGOURI ','sahbi.gargouri.pro@gmail.com','Sahbi','22881249',NULL,'Tunisienne','Tunis',6,' Ingénieur d’études et développement','INSAT',5,'En poste',7,'Thinktank',NULL,NULL,25,'Non',NULL,5,NULL,NULL,NULL),(119,'Candidat','Mr','MESSELMENI','messelmeni.mounir@gmail.com','Mounir','22487036',NULL,'Tunisienne','apartment 37 building 5 2080, Ariana, Tunisia',12,'Ingénieur Python/Django','INSAT',5,'En poste',7,'3B Entreprise',NULL,'Python/Django (Advanced knowledge): Models, Forms, Custom fields, Customizing the admin, Unit Test, ORM, Celery, Tastypie(REST API), social auth, SES backend, S3 Storage, Fabric, Mongoengine { Javascript (Advanced knowledge): jQuery, AngularJS, KnockoutJS, Backbone\r\n{ CSS: Bootstrap from Twitter { Java/JEE (Advanced knowledge) : Swing/Awt, JSF 2, PrimeFaces, RichFaces,JPA (Hibernate, EclipseLink), EJB 3, JMS, RMI, CDI, JBoss Seam 3 (Security,\r\nPersistence, Mail, Faces, Solder, Transaction), Maven, Cloud Amazon AWS, S3, EC2, SES\r\nApplication, and web, server, Glassfish, JBoss AS, Apache, Apache Tomcat, Gunicorn, Nginxvv\r\n',25,'Non',NULL,3,'Mounir MESSELMENI.pdf',NULL,NULL),(120,'Candidat','Mr','TARHOUNI','tarhouni.wael@gmail.com','WAEL ','55458061',NULL,'Tunisienne','Tunis',12,'Ingénieur recherche et développement','Esprit',6,'En poste',7,'Daleelteq','Municipalité Taif KSA','Ajax,javascript ,html,xhtml,css,jquery,jsf,jsp,richfaces,apache,flash\r\nAssembleur, C/C++, JAVA,JEE,DotNet,SPRING,J2ME,PL/SQL,XML,PHP,PHP5,STRUTS2',24,'Non',NULL,5,NULL,NULL,NULL),(121,'Profil','Mr','RAIS','rais.mohamed87@yahoo.fr','Mohamed ','24106863',NULL,'Tunisienne','Résidence Essaada1, BlocB, appartement 4-216, Cité El Khadra, Tunis',12,'Jeune diplômé','Esprit',4,'A la recherche du travail',7,'Aucune',NULL,'Langages Java, C#, C++, C, SQL, PL/SQL, PHP5, JavaScript.\r\nJavaEE JSF, JPA, Hibernate, Spring data, EJB, JSP, JSTL, Servlets, Spring.\r\n.Net C#.net, VB.net, ASP.net.\r\nModélisation UML.\r\nWeb XHTML, HTML, XML, CSS, JavaScript.\r\nMéthodologies Scrum.\r\nSystèmes Windows, Linux (Ubuntu).\r\nRéseaux RTP, RTCP, LDAP, SNMP, DHCP, VLAN.\r\nOutils Eclipse, MyEclipse, Netbeans, Microsoft Visual Studio, SpringSource, Rational Rose.\r\nSGBD SOtarraUclMe, LM. ysql, SQL Server, PostgreSQL.',26,'Non',NULL,5,'Mohamed RAIS.pdf',NULL,NULL),(122,'Profil','Mr','BELGACEM ','hmaidensi@gmail.com','Abdelhamid ','97034070',NULL,'Tunisienne','08,cité el manezeh,KalaaSghira Sousse Tunisie',12,'Jeune diplômé','ENSI',4,'A la recherche du travail',7,'Aucune',NULL,NULL,26,'Non',NULL,5,'Abdelhamid BELGACEM.pdf',NULL,NULL),(123,'Profil','Mlle','ZITOUNI ','zitouni.abeer@gmail.com','ABIR','23544215',NULL,'Tunisienne','RUE HABIB BOUGATFA SOMAA- 8023 NABEUL',12,'Formatrice','ISAMM',4,'En poste',7,'Centre de formation de korba.',NULL,NULL,26,'Non',NULL,5,'ABIR ZITOUNI.pdf',NULL,NULL),(124,'Candidat','Mr','KERFAHI','kerfahighazi@yahoo.fr','Ghazi ','96437924',NULL,'Tunisienne','42 239 imp 1 cité Essalama Mornaguia Tunis 1110, Tunisie',12,'Ingénieur Etudes et Développement Java/J2EE','ISIT’COM',6,'En poste',7,'Jasmine Conseil',NULL,NULL,24,'Oui','CCNA1, C2I',5,'Ghazi KERFAHI.pdf',NULL,NULL),(125,'Candidat','Mr','BOUDAYA','nizar.boudaya@gmail.com',' Nizar','22699168',NULL,'Tunisienne','N7 Bloc 24b cité Antit DenDen Tunis',12,'Ingénieur Etude et développement','FST',6,'En poste',7,'Insurance Global Operations',NULL,NULL,23,'Non',NULL,5,'Nizar BOUDAYA.pdf',NULL,NULL),(126,'Candidat','Mr','CHALGHOUMI','chaouki.chalghoumi@gmail.com','Chaouki','50012073','98307875','Tunisienne','22 Rue 6853 Cité Ettahrir Supérieur.2042 Tunis',3,'Consultant Fonctionnel BI/EPM/SI Financiers','ISG Tunis',7,'En poste',6,'Orange Tunisie','ORASCOM TELECOM TUNISIA ','Méthodes : Merise, Uml.	\r\nLangages : Java, C++, C, VB, Pascal, PHP, ASP .NET, C#, …\r\nBases de Données   : Oracle8i et9i, MySQL, SQL SERVER...\r\nBI: Hyperion Essbase, Planning, Financial Reporting, Shared Services..\r\nOutils: Microsoft Visual Studio, Dreamweaver, Zend Studio, edit plus…\r\nSystème d’exploitation : Windows 95-98-2000-xp, Mac os, linux...\r\nLogiciels de design : Photoshop, Fireworks, Flash.\r\nLogiciels de conception : AMC Designer, Rational Rose.\r\nRéseaux : Protocoles TCP/IP et modèle ISO d’OSI, réseaux Ethernet.                      \r\nAutres : Commerce électronique, Intelligence Artificielle, Datawarehouse et Datamining, Génie logiciel et conduite de projet, Langages de compilation et Assembleur, Programmation en Shell…\r\n',19,'Non',NULL,5,'Chaouki CHALGHOUMI.pdf',NULL,'Gestion de projets Financial Management (EPM).\r\nContrôle de gestion.( Controlling , montage budgétaire, …).\r\nComptabilité Générale et Comptabilité Analytique.\r\nEtude de rentabilité, Costing,..\r\nReporting Financier, Dashboard , KPIs….\r\nRéférentiel de Gestion.\r\n'),(127,'Candidat','Mr','MEKKI','mekki.mohamedzyed@gmail.com',' Mohamed Zyed','20288541',NULL,'Tunisienne','Cité EL ADL, BOU ARGOUB, NABEUL, 8040',12,'Ingénieur Développeur','INSAT',5,'En poste',7,'Gepmag Services',NULL,'JAVA, J2EE, Spring, Hibernate, Spring security, Spring Web Flow, JAXB, JUnit, JSF, SVN, Jquery, Ajax, CSS3, SOAP, Framework Maximo IBM, Maven, Framework Symfony 2, PHP5, PERL, PL/SQL, C#, VB.net, Android, C, C++\r\nMERISE et processus unifié.',25,'Non',NULL,5,'Mohamed Zyed MEKKI.pdf',NULL,NULL),(128,'Candidat','Mr','BECHRAOUI','bechraoui.amine@gmail.com','Amine','26490009',NULL,'Tunisienne','18 road of Sousse Megrine Riadh Ben Arous 2014 – Tunisia',12,'Ingénieur Développeur','Esprit',4,'En poste',7,'APPID',NULL,'C, C++.\r\nJava, Java Micro Edition.\r\nJava Enterprise Edition: EJB3, Hibernate, Toplink, JPA, JSF.\r\n Spring: IOC, AOP.\r\n Web: HTML, java-script, CSS, PHP, XML.\r\nData Base: SQL, PL, Oracle, MySQL, PostgreSQL.',25,'Non',NULL,5,'Amine BECHRAOUI.pdf',NULL,NULL),(129,'Candidat','Mr','JABRANI','elyes.jabrani@gmail.com','Elyes ','21218950',NULL,'Tunisienne','20, rue du Liberia 2063 Nouvelle-Médina III',3,'Stage d\'été ','ESPRIT',4,'Etudiant',NULL,'En recherche de stage d\'été',NULL,'Windows, Linux\r\nC, C++, Java, J2EE, SQL, PHP\r\nMySql, ORACLE, Microsoft SQL server\r\nR, SAS, SSRS\r\nHTML, JavaScript, AJAX, Joomla\r\nUML, MERISE\r\nAdobe Photoshop, Adobe Illustrator, Adobe Flash\r\nMS Project, SCRUM',26,'Non',NULL,6,'Elyes JABRANI.pdf',NULL,NULL),(130,'Profil','Mr','BEN BRAHAM','hatembenbraham@gmail.com','Hatem ','55300209',NULL,'Tunisienne','Tunis',4,'Chef de projet web - Développeur','FST',7,'En poste',6,'Formacom','SBT','PHP 5.2, PHP 5.3, PHP 5.4, XML, HTML5/CSS3, JavaScript, JQuery,\r\nPL/SQL, MVC, AJAX.Joomla, thelia, wordpress, Prestashop, Drupal, PunBB. Zend, Symfony, CakePHP.',19,'Non',NULL,5,'Hatem BEN BRAHAM.pdf',NULL,NULL),(131,'Candidat','Mr','BEN YOUNES','achrafbenyounes2012@gmail.com','Achraf','27002441',NULL,'Tunisienne','Tunis',12,'Développeur JAVA/SCALA','FST',6,'En poste',7,'Imbus Tunisia ','Annitec Consulting ','Java, Scala, Scalacheck, Haskell, Play, Slick, Sonar, Jenkins, Maven, Mercurial,Artifactory, Junit,Java, Spring, gwt, gxt, Liferay, Jira, Portlet, Sql server, Subversion, Nexus, Junit, EasyMock, Cobertura, find bugs, PMD, check style',22,'Oui','OCJP 6 – Oracle Certified Java Programmer',5,'Achraf BEN YOUNES.pdf',NULL,NULL),(132,'Candidat','Mlle','DHIFALLAH','dhifallah.wafa@gmail.com','Wafa','25886635',NULL,'Tunisienne','8 Rue Med El Baridi Cité la Gazelle 2083 Tunis - TUNISIE',13,'CONSULTANTE PMO / CMMI','IAE de Tours (FRANCE)',6,'En poste',8,'Sungard','Maille Club',NULL,21,'Oui','Srum Master',4,'DHIFALLAH Wafa.doc',NULL,NULL),(133,'Candidat','Mr','KADDECHI','seif.kaddechi@gmail.com','Seif El Islem ','29300034',NULL,'Tunisienne','28. Lac Victoria Les berges du lac Tunis',4,'Consultant Web','ISGI Sfax',7,'En poste',6,'Privata-lex','IDEV CONSULTING',NULL,20,'Non',NULL,5,'Seif El Islam KADDECHI.pdf',NULL,NULL),(134,'Candidat','Mr','YOUSSFI','Youssfi@gmail.com','Ismail','97577398',NULL,'Tunisienne','8 Rue Boukhari Ariana 2080 ',4,'Développeur Web ','ISAMM',7,'En poste',5,'Stars Innovation & D2veloppement  ','Medianet',NULL,15,'Non',NULL,5,'Ismail YOUSSFI.pdf',NULL,NULL),(135,'Candidat','Mr','SAYHI','sayhi.haythem@gmail.com','Haythem ','22280825',NULL,'Tunisienne','Khadra quarter, tunis (Tunisia)',12,'Développeur','FST',6,'En poste',7,'Telnet',NULL,NULL,24,'Non',NULL,5,'Haythem SAYHI.pdf',NULL,NULL),(136,'Candidat','Mr','ELGUESMI','zoubeir.elguesmi@gmail.com',' Zoubeir','52299384',NULL,'Tunisienne','Elmansoura 1 JDAIDA Manouba 2075',14,'Administrateur DATACENTER','ISET Nabeul',7,'En poste',5,'Viamobile','Business & Decision',NULL,20,'Non',NULL,6,'Zoubeir ELGUESMI.pdf',NULL,NULL);
/*!40000 ALTER TABLE `candidat` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `candidat_competence`
--

DROP TABLE IF EXISTS `candidat_competence`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `candidat_competence` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `candidat_id` int(11) NOT NULL,
  `competence_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_candidat` (`candidat_id`,`competence_id`),
  KEY `id_competence` (`competence_id`),
  CONSTRAINT `candidat_competence_candidat` FOREIGN KEY (`candidat_id`) REFERENCES `candidat` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `candidat_competence_competence` FOREIGN KEY (`competence_id`) REFERENCES `competence` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=304 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `candidat_competence`
--

LOCK TABLES `candidat_competence` WRITE;
/*!40000 ALTER TABLE `candidat_competence` DISABLE KEYS */;
INSERT INTO `candidat_competence` VALUES (83,88,2),(84,89,2),(85,90,1),(86,90,5),(87,90,7),(88,91,1),(89,91,5),(90,92,1),(91,93,2),(92,93,5),(93,94,6),(94,94,7),(95,94,12),(96,95,1),(97,95,2),(98,95,5),(99,95,9),(100,96,1),(101,97,1),(102,97,2),(104,97,4),(105,97,5),(106,97,6),(107,97,7),(108,97,9),(109,97,11),(110,97,12),(111,98,1),(112,98,2),(113,98,5),(114,99,1),(115,99,5),(116,100,2),(117,100,5),(118,100,9),(119,100,11),(120,101,1),(121,102,1),(122,102,5),(123,102,6),(124,102,7),(125,103,1),(126,103,6),(127,103,7),(128,104,1),(129,104,5),(130,104,6),(131,104,7),(132,105,1),(133,106,1),(134,107,1),(135,107,5),(136,107,6),(137,107,7),(138,108,1),(139,108,5),(140,108,6),(141,108,7),(142,109,1),(143,109,5),(144,109,6),(145,109,7),(146,109,12),(159,111,13),(160,111,14),(174,112,5),(175,112,7),(176,112,9),(177,112,17),(162,113,1),(163,113,7),(171,113,19),(172,113,20),(173,113,21),(164,114,1),(165,114,15),(166,114,19),(167,114,20),(168,114,21),(169,114,22),(170,114,23),(231,119,25),(232,119,26),(233,120,1),(234,120,15),(235,120,19),(236,120,20),(237,120,21),(238,121,1),(239,121,15),(240,121,19),(241,121,20),(242,122,1),(243,122,6),(244,122,15),(245,122,19),(246,123,1),(247,123,15),(248,123,19),(249,123,20),(250,123,21),(251,124,1),(252,124,15),(253,124,19),(254,124,20),(255,124,21),(256,124,22),(257,124,23),(258,124,24),(259,125,1),(260,125,19),(261,125,20),(262,125,23),(263,126,5),(264,126,27),(265,126,28),(266,126,29),(289,126,31),(267,127,1),(268,127,6),(269,127,15),(270,127,19),(271,127,20),(272,127,22),(273,127,23),(274,128,1),(275,128,15),(276,128,19),(277,128,20),(278,128,23),(279,129,14),(280,130,2),(281,130,5),(282,130,9),(283,131,1),(284,131,15),(285,131,19),(286,131,22),(288,132,30),(290,133,5),(291,133,9),(292,133,11),(293,134,5),(294,134,9),(295,134,11),(296,135,15),(297,135,19),(298,135,20),(299,135,21),(300,135,22),(301,135,23),(303,136,33);
/*!40000 ALTER TABLE `candidat_competence` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `candidat_metier`
--

DROP TABLE IF EXISTS `candidat_metier`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `candidat_metier` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `candidat_id` int(11) DEFAULT NULL,
  `metier_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_user_competence_user_idx` (`candidat_id`),
  KEY `id_user_competence_competence_idx` (`metier_id`),
  CONSTRAINT `id_candidat` FOREIGN KEY (`candidat_id`) REFERENCES `candidat` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `id_user_metier` FOREIGN KEY (`metier_id`) REFERENCES `metier` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=98 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `candidat_metier`
--

LOCK TABLES `candidat_metier` WRITE;
/*!40000 ALTER TABLE `candidat_metier` DISABLE KEYS */;
INSERT INTO `candidat_metier` VALUES (44,88,3),(45,88,4),(46,89,4),(47,90,3),(48,91,3),(49,92,3),(50,93,4),(51,94,3),(52,94,4),(53,95,3),(54,96,3),(55,97,3),(56,97,4),(57,98,3),(58,99,3),(59,100,4),(60,101,3),(61,102,3),(62,103,3),(63,104,3),(64,105,3),(65,106,3),(66,107,3),(67,108,3),(68,108,4),(69,109,3),(71,111,3),(72,112,3),(73,113,3),(74,114,3),(79,119,3),(80,120,3),(81,121,3),(82,122,3),(83,123,3),(84,124,3),(85,125,3),(86,126,4),(87,127,3),(88,128,3),(89,129,3),(90,130,3),(91,131,3),(92,132,3),(93,133,4),(94,134,3),(95,135,3),(97,136,5);
/*!40000 ALTER TABLE `candidat_metier` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `candidat_technologie`
--

DROP TABLE IF EXISTS `candidat_technologie`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `candidat_technologie` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `candidat_id` int(11) DEFAULT NULL,
  `technologie_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_user_technologie_idx` (`candidat_id`),
  KEY `id_user_technologie_technologie_idx` (`technologie_id`),
  CONSTRAINT `id_user_technologie_technologie` FOREIGN KEY (`technologie_id`) REFERENCES `technologie` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `id_user_technologie_user` FOREIGN KEY (`candidat_id`) REFERENCES `candidat` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=147 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `candidat_technologie`
--

LOCK TABLES `candidat_technologie` WRITE;
/*!40000 ALTER TABLE `candidat_technologie` DISABLE KEYS */;
INSERT INTO `candidat_technologie` VALUES (40,88,6),(41,89,6),(42,90,6),(43,91,6),(44,92,6),(45,93,6),(46,94,6),(47,95,6),(48,96,6),(49,97,4),(50,97,5),(51,97,6),(52,98,6),(53,99,6),(54,100,6),(55,101,6),(56,102,6),(57,103,6),(58,104,6),(59,105,4),(60,106,4),(61,107,6),(62,108,4),(63,109,6),(69,111,7),(70,111,8),(74,114,9),(75,113,9),(76,113,14),(77,112,10),(78,112,13),(79,112,14),(115,119,18),(116,119,19),(117,120,9),(118,121,9),(119,122,9),(120,122,14),(121,123,9),(122,124,9),(123,125,9),(124,126,16),(125,126,24),(126,127,9),(127,128,9),(128,129,7),(129,129,15),(130,130,6),(131,131,9),(133,132,25),(134,126,26),(136,133,28),(137,133,29),(138,133,30),(139,133,31),(140,133,32),(141,134,31),(142,134,32),(143,135,9),(145,136,33),(146,136,34);
/*!40000 ALTER TABLE `candidat_technologie` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `centurion_content_type`
--

DROP TABLE IF EXISTS `centurion_content_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `centurion_content_type` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `centurion_content_type`
--

LOCK TABLES `centurion_content_type` WRITE;
/*!40000 ALTER TABLE `centurion_content_type` DISABLE KEYS */;
INSERT INTO `centurion_content_type` VALUES (2,'Cms_Model_DbTable_Flatpage'),(3,'Cms_Model_DbTable_Row_Flatpage'),(8,'Core_Model_DbTable_Navigation');
/*!40000 ALTER TABLE `centurion_content_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `centurion_navigation`
--

DROP TABLE IF EXISTS `centurion_navigation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `centurion_navigation` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `label` varchar(150) DEFAULT NULL,
  `module` varchar(100) DEFAULT NULL,
  `controller` varchar(100) DEFAULT NULL,
  `action` varchar(100) DEFAULT NULL,
  `params` text,
  `permission` varchar(255) DEFAULT NULL,
  `route` varchar(100) DEFAULT NULL,
  `uri` varchar(255) DEFAULT NULL,
  `order` int(11) DEFAULT NULL,
  `is_visible` int(1) NOT NULL DEFAULT '1',
  `is_in_menu` int(1) NOT NULL DEFAULT '1',
  `class` varchar(50) DEFAULT NULL,
  `mptt_lft` int(11) unsigned NOT NULL,
  `mptt_rgt` int(11) unsigned NOT NULL,
  `mptt_level` int(11) unsigned NOT NULL,
  `mptt_tree_id` int(11) unsigned DEFAULT NULL,
  `mptt_parent_id` int(11) unsigned DEFAULT NULL,
  `proxy_model` int(11) unsigned DEFAULT NULL,
  `proxy_pk` int(11) unsigned DEFAULT NULL,
  `can_be_deleted` int(11) unsigned DEFAULT '1',
  `original_id` int(11) unsigned DEFAULT NULL,
  `language_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `proxy_model` (`proxy_model`,`proxy_pk`),
  KEY `order` (`order`),
  KEY `is_visible` (`is_visible`),
  KEY `is_in_menu` (`is_in_menu`),
  KEY `mptt_lft` (`mptt_lft`),
  KEY `mptt_rgt` (`mptt_rgt`),
  KEY `mptt_level` (`mptt_level`),
  KEY `mptt_tree_id` (`mptt_tree_id`),
  KEY `mptt_parent_id` (`mptt_parent_id`),
  KEY `original_id` (`original_id`,`language_id`),
  KEY `can_be_deleted` (`can_be_deleted`),
  KEY `language_id` (`language_id`),
  CONSTRAINT `centurion_navigation_ibfk_1` FOREIGN KEY (`proxy_model`) REFERENCES `centurion_content_type` (`id`),
  CONSTRAINT `centurion_navigation_ibfk_2` FOREIGN KEY (`original_id`) REFERENCES `centurion_navigation` (`id`),
  CONSTRAINT `centurion_navigation_ibfk_3` FOREIGN KEY (`language_id`) REFERENCES `translation_language` (`id`),
  CONSTRAINT `centurion_navigation_ibfk_4` FOREIGN KEY (`language_id`) REFERENCES `translation_language` (`id`),
  CONSTRAINT `fk_navigation__navigation_parent_id___navigation__id` FOREIGN KEY (`mptt_parent_id`) REFERENCES `centurion_navigation` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=145 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `centurion_navigation`
--

LOCK TABLES `centurion_navigation` WRITE;
/*!40000 ALTER TABLE `centurion_navigation` DISABLE KEYS */;
INSERT INTO `centurion_navigation` VALUES (1,'Users','auth','admin-user',NULL,NULL,NULL,'default',NULL,1,1,1,'sqdsdqsdqsd',18,27,1,5,12,NULL,NULL,1,NULL,NULL),(2,'Manage group permissions','auth','admin-group-permission',NULL,NULL,NULL,'default',NULL,3,1,1,NULL,19,20,2,5,1,NULL,NULL,1,NULL,NULL),(3,'Pages','admin','admin-navigation',NULL,NULL,NULL,NULL,NULL,2,1,1,NULL,34,39,1,5,12,NULL,NULL,1,NULL,NULL),(4,'Settings','user','admin-profile',NULL,NULL,NULL,'default',NULL,3,1,1,NULL,40,49,1,5,12,NULL,NULL,1,NULL,NULL),(5,'Template','cms','admin-flatpage-template',NULL,NULL,NULL,NULL,NULL,NULL,1,1,NULL,37,38,2,5,3,NULL,NULL,1,NULL,NULL),(7,'Cache','admin','index','cache',NULL,NULL,NULL,NULL,2,1,1,NULL,41,44,2,5,4,NULL,NULL,1,NULL,NULL),(8,'Clear cache','admin','index','clear-cache',NULL,NULL,NULL,NULL,NULL,1,1,NULL,42,43,3,5,7,NULL,NULL,1,NULL,NULL),(11,'Translation','translation','admin',NULL,NULL,NULL,NULL,NULL,NULL,1,1,NULL,45,46,2,5,4,NULL,NULL,1,NULL,NULL),(12,'Backoffice',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,1,NULL,1,90,0,5,NULL,NULL,NULL,0,NULL,NULL),(13,'Error','admin','index','log',NULL,NULL,NULL,NULL,NULL,1,1,NULL,47,48,2,5,4,NULL,NULL,1,NULL,NULL),(14,'Pages unactivated',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,1,'unactived',1,26,0,20,NULL,NULL,NULL,0,NULL,NULL),(16,'Frontoffice',NULL,NULL,NULL,NULL,'all',NULL,NULL,NULL,1,1,NULL,1,86,0,18,NULL,NULL,NULL,0,NULL,NULL),(20,'Contents',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,1,NULL,2,17,1,5,12,NULL,NULL,1,NULL,2),(105,'Pages','admin','admin-navigation',NULL,NULL,NULL,NULL,NULL,NULL,1,1,NULL,35,36,2,5,3,NULL,NULL,1,NULL,NULL),(118,'Permission','auth','admin-permission',NULL,NULL,NULL,NULL,NULL,NULL,1,1,NULL,21,22,2,5,1,NULL,NULL,1,NULL,NULL),(119,'Script permissions','auth','admin-script-permission',NULL,NULL,NULL,NULL,NULL,NULL,1,1,NULL,23,24,2,5,1,NULL,NULL,1,NULL,NULL),(121,'Group permission','auth','admin-group-permission',NULL,NULL,NULL,NULL,NULL,NULL,1,1,NULL,25,26,2,5,1,NULL,NULL,1,NULL,2),(122,'Configuration',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,1,NULL,50,89,1,5,12,NULL,NULL,1,NULL,2),(123,'Ecoles','resourcing','admin-ecole',NULL,NULL,NULL,NULL,NULL,NULL,1,1,NULL,61,62,2,5,122,NULL,NULL,1,NULL,2),(124,'Etudes','resourcing','admin-etudes',NULL,NULL,NULL,NULL,NULL,NULL,1,1,NULL,63,64,2,5,122,NULL,NULL,1,NULL,2),(125,'Technologies','resourcing','admin-technologie',NULL,NULL,NULL,NULL,NULL,NULL,1,1,NULL,83,84,2,5,122,NULL,NULL,1,NULL,2),(126,'Compétences','resourcing','admin-competence',NULL,NULL,NULL,NULL,NULL,NULL,1,1,NULL,55,56,2,5,122,NULL,NULL,1,NULL,2),(127,'Expériences','resourcing','admin-experience',NULL,NULL,NULL,NULL,NULL,NULL,1,1,NULL,65,66,2,5,122,NULL,NULL,1,NULL,2),(128,'Postes','resourcing','admin-poste',NULL,NULL,NULL,NULL,NULL,NULL,1,1,NULL,75,76,2,5,122,NULL,NULL,1,NULL,2),(129,'Domaines','resourcing','admin-domaine',NULL,NULL,NULL,NULL,NULL,NULL,1,1,NULL,59,60,2,5,122,NULL,NULL,1,NULL,2),(130,'Sociétés','resourcing','admin-societe',NULL,NULL,NULL,NULL,NULL,NULL,1,1,NULL,79,80,2,5,122,NULL,NULL,1,NULL,2),(132,'Métiers','resourcing','admin-metier',NULL,NULL,NULL,NULL,NULL,NULL,1,1,NULL,69,70,2,5,122,NULL,NULL,1,NULL,2),(134,'Sources CV','resourcing','admin-sourcecv',NULL,NULL,NULL,NULL,NULL,NULL,1,1,NULL,81,82,2,5,122,NULL,NULL,1,NULL,2),(135,'Candidats','resourcing','admin-candidat',NULL,NULL,NULL,NULL,NULL,NULL,1,1,NULL,53,54,2,5,122,NULL,NULL,1,NULL,2),(137,'Début de carrière','resourcing','admin-debutcarriere',NULL,NULL,NULL,NULL,NULL,NULL,1,1,NULL,57,58,2,5,122,NULL,NULL,1,NULL,2),(138,'Tests techniques','resourcing','admin-testtechnique',NULL,NULL,NULL,NULL,NULL,NULL,1,1,NULL,85,86,2,5,122,NULL,NULL,1,NULL,2),(139,'Type Contrat','resourcing','admin-typecontrat',NULL,NULL,NULL,NULL,NULL,NULL,1,1,NULL,87,88,2,5,122,NULL,NULL,1,NULL,2),(140,'Avantages','resourcing','admin-avantage',NULL,NULL,NULL,NULL,NULL,NULL,1,1,NULL,51,52,2,5,122,NULL,NULL,1,NULL,2),(141,'Pays','resourcing','admin-country',NULL,NULL,NULL,NULL,NULL,NULL,1,1,NULL,73,74,2,5,122,NULL,NULL,1,NULL,2),(142,'Opérationnels','resourcing','admin-operationnel',NULL,NULL,NULL,NULL,NULL,NULL,1,1,NULL,71,72,2,5,122,NULL,NULL,1,NULL,2),(143,'Recommandations','resourcing','admin-recommandations',NULL,NULL,NULL,NULL,NULL,NULL,1,1,NULL,77,78,2,5,122,NULL,NULL,1,NULL,2),(144,'Jours fériés','resourcing','admin-holidays',NULL,NULL,NULL,NULL,NULL,NULL,1,1,NULL,67,68,2,5,122,NULL,NULL,1,NULL,2);
/*!40000 ALTER TABLE `centurion_navigation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `centurion_site`
--

DROP TABLE IF EXISTS `centurion_site`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `centurion_site` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `domain` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `centurion_site`
--

LOCK TABLES `centurion_site` WRITE;
/*!40000 ALTER TABLE `centurion_site` DISABLE KEYS */;
INSERT INTO `centurion_site` VALUES (1,'example.com','example.com');
/*!40000 ALTER TABLE `centurion_site` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_flatpage`
--

DROP TABLE IF EXISTS `cms_flatpage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_flatpage` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `slug` varchar(100) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `keywords` varchar(255) DEFAULT NULL,
  `body` text,
  `url` varchar(100) DEFAULT NULL,
  `flatpage_template_id` int(11) unsigned NOT NULL,
  `published_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `is_published` int(1) unsigned DEFAULT '0',
  `mptt_lft` int(11) unsigned NOT NULL,
  `mptt_rgt` int(11) unsigned NOT NULL,
  `mptt_level` int(11) unsigned NOT NULL,
  `mptt_tree_id` int(11) unsigned DEFAULT NULL,
  `mptt_parent_id` int(11) unsigned DEFAULT NULL,
  `original_id` int(11) unsigned DEFAULT NULL,
  `language_id` int(11) unsigned DEFAULT NULL,
  `forward_url` varchar(255) DEFAULT NULL,
  `flatpage_type` int(1) NOT NULL DEFAULT '1',
  `route` varchar(50) DEFAULT NULL,
  `class` varchar(255) DEFAULT NULL,
  `cover_id` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `flatpage_template_id` (`flatpage_template_id`),
  KEY `flatpage_cover_id` (`cover_id`),
  KEY `mptt_parent_id` (`mptt_parent_id`),
  KEY `slug` (`slug`),
  KEY `is_published` (`is_published`),
  KEY `mptt_lft` (`mptt_lft`),
  KEY `mptt_rgt` (`mptt_rgt`),
  KEY `mptt_level` (`mptt_level`),
  KEY `mptt_tree_id` (`mptt_tree_id`),
  KEY `original_id` (`original_id`,`language_id`),
  KEY `language_id` (`language_id`),
  CONSTRAINT `cms_flatpage_ibfk_1` FOREIGN KEY (`mptt_parent_id`) REFERENCES `cms_flatpage` (`id`),
  CONSTRAINT `cms_flatpage_ibfk_2` FOREIGN KEY (`original_id`) REFERENCES `cms_flatpage` (`id`),
  CONSTRAINT `cms_flatpage_ibfk_3` FOREIGN KEY (`language_id`) REFERENCES `translation_language` (`id`),
  CONSTRAINT `cms_flatpage__banner_id___media_file__id` FOREIGN KEY (`cover_id`) REFERENCES `media_file` (`id`),
  CONSTRAINT `cms_flatpage__flatpage_template_id___cms_flatpage_template__id` FOREIGN KEY (`flatpage_template_id`) REFERENCES `cms_flatpage_template` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_flatpage`
--

LOCK TABLES `cms_flatpage` WRITE;
/*!40000 ALTER TABLE `cms_flatpage` DISABLE KEYS */;
/*!40000 ALTER TABLE `cms_flatpage` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_flatpage_template`
--

DROP TABLE IF EXISTS `cms_flatpage_template`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_flatpage_template` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `view_script` varchar(50) NOT NULL,
  `class` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_flatpage_template`
--

LOCK TABLES `cms_flatpage_template` WRITE;
/*!40000 ALTER TABLE `cms_flatpage_template` DISABLE KEYS */;
INSERT INTO `cms_flatpage_template` VALUES (1,'Basic','_generic/basic.phtml',NULL),(2,'Basic no title','_generic/basic-notitle.phtml',NULL);
/*!40000 ALTER TABLE `cms_flatpage_template` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `competence`
--

DROP TABLE IF EXISTS `competence`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `competence` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `competence` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `competence`
--

LOCK TABLES `competence` WRITE;
/*!40000 ALTER TABLE `competence` DISABLE KEYS */;
INSERT INTO `competence` VALUES (1,'J2EE'),(2,'Drupal'),(4,'Talend'),(5,'PHP'),(6,'C#'),(7,'ASP.NET'),(9,'Symfony'),(11,'Zend'),(12,'Microsoft SharePoint'),(13,'Stat'),(14,'SAS'),(15,'JAVA'),(16,'Qlik'),(17,'Tableau Software'),(18,'SSIS/SSRS/SSAS'),(19,'Spring'),(20,'Hibernate'),(21,'Struts'),(22,'Maven'),(23,'JSF'),(24,'Jboss'),(25,'Python'),(26,'Django'),(27,'Hyperion Essbase'),(28,'Hyperion planning '),(29,'Hyperion F.R'),(30,'QA'),(31,'SAP BO'),(33,'Systèmes et réseaux');
/*!40000 ALTER TABLE `competence` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `country`
--

DROP TABLE IF EXISTS `country`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `country` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code_pays` varchar(3) COLLATE utf8_unicode_ci NOT NULL,
  `fr` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `en` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=239 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `country`
--

LOCK TABLES `country` WRITE;
/*!40000 ALTER TABLE `country` DISABLE KEYS */;
INSERT INTO `country` VALUES (1,'AF','Afghanistan','Afghanistan'),(2,'ZA','Afrique du Sud','South Africa'),(3,'AL','Albanie','Albania'),(4,'DZ','Algérie','Algeria'),(5,'DE','Allemagne','Germany'),(6,'AD','Andorre','Andorra'),(7,'AO','Angola','Angola'),(8,'AI','Anguilla','Anguilla'),(9,'AQ','Antarctique','Antarctica'),(10,'AG','Antigua-et-Barbuda','Antigua & Barbuda'),(11,'AN','Antilles néerlandaises','Netherlands Antilles'),(12,'SA','Arabie saoudite','Saudi Arabia'),(13,'AR','Argentine','Argentina'),(14,'AM','Arménie','Armenia'),(15,'AW','Aruba','Aruba'),(16,'AU','Australie','Australia'),(17,'AT','Autriche','Austria'),(18,'AZ','Azerbaïdjan','Azerbaijan'),(19,'BJ','Bénin','Benin'),(20,'BS','Bahamas','Bahamas, The'),(21,'BH','Bahreïn','Bahrain'),(22,'BD','Bangladesh','Bangladesh'),(23,'BB','Barbade','Barbados'),(24,'PW','Belau','Palau'),(25,'BE','Belgique','Belgium'),(26,'BZ','Belize','Belize'),(27,'BM','Bermudes','Bermuda'),(28,'BT','Bhoutan','Bhutan'),(29,'BY','Biélorussie','Belarus'),(30,'MM','Birmanie','Myanmar (ex-Burma)'),(31,'BO','Bolivie','Bolivia'),(32,'BA','Bosnie-Herzégovine','Bosnia and Herzegovina'),(33,'BW','Botswana','Botswana'),(34,'BR','Brésil','Brazil'),(35,'BN','Brunei','Brunei Darussalam'),(36,'BG','Bulgarie','Bulgaria'),(37,'BF','Burkina Faso','Burkina Faso'),(38,'BI','Burundi','Burundi'),(39,'CI','Côte d\'Ivoire','Ivory Coast (see Cote d\'Ivoire)'),(40,'KH','Cambodge','Cambodia'),(41,'CM','Cameroun','Cameroon'),(42,'CA','Canada','Canada'),(43,'CV','Cap-Vert','Cape Verde'),(44,'CL','Chili','Chile'),(45,'CN','Chine','China'),(46,'CY','Chypre','Cyprus'),(47,'CO','Colombie','Colombia'),(48,'KM','Comores','Comoros'),(49,'CG','Congo','Congo'),(50,'KP','Corée du Nord','Korea, Demo. People\'s Rep. of'),(51,'KR','Corée du Sud','Korea, (South) Republic of'),(52,'CR','Costa Rica','Costa Rica'),(53,'HR','Croatie','Croatia'),(54,'CU','Cuba','Cuba'),(55,'DK','Danemark','Denmark'),(56,'DJ','Djibouti','Djibouti'),(57,'DM','Dominique','Dominica'),(58,'EG','Égypte','Egypt'),(59,'AE','Émirats arabes unis','United Arab Emirates'),(60,'EC','Équateur','Ecuador'),(61,'ER','Érythrée','Eritrea'),(62,'ES','Espagne','Spain'),(63,'EE','Estonie','Estonia'),(64,'US','États-Unis','United States'),(65,'ET','Éthiopie','Ethiopia'),(66,'FI','Finlande','Finland'),(67,'FR','France','France'),(68,'GE','Géorgie','Georgia'),(69,'GA','Gabon','Gabon'),(70,'GM','Gambie','Gambia, the'),(71,'GH','Ghana','Ghana'),(72,'GI','Gibraltar','Gibraltar'),(73,'GR','Grèce','Greece'),(74,'GD','Grenade','Grenada'),(75,'GL','Groenland','Greenland'),(76,'GP','Guadeloupe','Guinea, Equatorial'),(77,'GU','Guam','Guam'),(78,'GT','Guatemala','Guatemala'),(79,'GN','Guinée','Guinea'),(80,'GQ','Guinée équatoriale','Equatorial Guinea'),(81,'GW','Guinée-Bissao','Guinea-Bissau'),(82,'GY','Guyana','Guyana'),(83,'GF','Guyane française','Guiana, French'),(84,'HT','Haïti','Haiti'),(85,'HN','Honduras','Honduras'),(86,'HK','Hong Kong','Hong Kong, (China)'),(87,'HU','Hongrie','Hungary'),(88,'BV','Ile Bouvet','Bouvet Island'),(89,'CX','Ile Christmas','Christmas Island'),(90,'NF','Ile Norfolk','Norfolk Island'),(91,'KY','Iles Cayman','Cayman Islands'),(92,'CK','Iles Cook','Cook Islands'),(93,'FO','Iles Féroé','Faroe Islands'),(94,'FK','Iles Falkland','Falkland Islands (Malvinas)'),(95,'FJ','Iles Fidji','Fiji'),(96,'GS','Iles Géorgie du Sud et Sandwich du Sud','S. Georgia and S. Sandwich Is.'),(97,'HM','Iles Heard et McDonald','Heard and McDonald Islands'),(98,'MH','Iles Marshall','Marshall Islands'),(99,'PN','Iles Pitcairn','Pitcairn Island'),(100,'SB','Iles Salomon','Solomon Islands'),(101,'SJ','Iles Svalbard et Jan Mayen','Svalbard and Jan Mayen Islands'),(102,'TC','Iles Turks-et-Caicos','Turks and Caicos Islands'),(103,'VI','Iles Vierges américaines','Virgin Islands, U.S.'),(104,'VG','Iles Vierges britanniques','Virgin Islands, British'),(105,'CC','Iles des Cocos (Keeling)','Cocos (Keeling) Islands'),(106,'UM','Iles mineures éloignées des États-Unis','US Minor Outlying Islands'),(107,'IN','Inde','India'),(108,'ID','Indonésie','Indonesia'),(109,'IR','Iran','Iran, Islamic Republic of'),(110,'IQ','Iraq','Iraq'),(111,'IE','Irlande','Ireland'),(112,'IS','Islande','Iceland'),(113,'IL','Israël','Israel'),(114,'IT','Italie','Italy'),(115,'JM','Jamaïque','Jamaica'),(116,'JP','Japon','Japan'),(117,'JO','Jordanie','Jordan'),(118,'KZ','Kazakhstan','Kazakhstan'),(119,'KE','Kenya','Kenya'),(120,'KG','Kirghizistan','Kyrgyzstan'),(121,'KI','Kiribati','Kiribati'),(122,'KW','Koweït','Kuwait'),(123,'LA','Laos','Lao People\'s Democratic Republic'),(124,'LS','Lesotho','Lesotho'),(125,'LV','Lettonie','Latvia'),(126,'LB','Liban','Lebanon'),(127,'LR','Liberia','Liberia'),(128,'LY','Libye','Libyan Arab Jamahiriya'),(129,'LI','Liechtenstein','Liechtenstein'),(130,'LT','Lituanie','Lithuania'),(131,'LU','Luxembourg','Luxembourg'),(132,'MO','Macao','Macao, (China)'),(133,'MG','Madagascar','Madagascar'),(134,'MY','Malaisie','Malaysia'),(135,'MW','Malawi','Malawi'),(136,'MV','Maldives','Maldives'),(137,'ML','Mali','Mali'),(138,'MT','Malte','Malta'),(139,'MP','Mariannes du Nord','Northern Mariana Islands'),(140,'MA','Maroc','Morocco'),(141,'MQ','Martinique','Martinique'),(142,'MU','Maurice','Mauritius'),(143,'MR','Mauritanie','Mauritania'),(144,'YT','Mayotte','Mayotte'),(145,'MX','Mexique','Mexico'),(146,'FM','Micronésie','Micronesia, Federated States of'),(147,'MD','Moldavie','Moldova, Republic of'),(148,'MC','Monaco','Monaco'),(149,'MN','Mongolie','Mongolia'),(150,'MS','Montserrat','Montserrat'),(151,'MZ','Mozambique','Mozambique'),(152,'NP','Népal','Nepal'),(153,'NA','Namibie','Namibia'),(154,'NR','Nauru','Nauru'),(155,'NI','Nicaragua','Nicaragua'),(156,'NE','Niger','Niger'),(157,'NG','Nigeria','Nigeria'),(158,'NU','Nioué','Niue'),(159,'NO','Norvège','Norway'),(160,'NC','Nouvelle-Calédonie','New Caledonia'),(161,'NZ','Nouvelle-Zélande','New Zealand'),(162,'OM','Oman','Oman'),(163,'UG','Ouganda','Uganda'),(164,'UZ','Ouzbékistan','Uzbekistan'),(165,'PE','Pérou','Peru'),(166,'PK','Pakistan','Pakistan'),(167,'PA','Panama','Panama'),(168,'PG','Papouasie-Nouvelle-Guinée','Papua New Guinea'),(169,'PY','Paraguay','Paraguay'),(170,'NL','Pays-Bas','Netherlands'),(171,'PH','Philippines','Philippines'),(172,'PL','Pologne','Poland'),(173,'PF','Polynésie française','French Polynesia'),(174,'PR','Porto Rico','Puerto Rico'),(175,'PT','Portugal','Portugal'),(176,'QA','Qatar','Qatar'),(177,'CF','République centrafricaine','Central African Republic'),(178,'CD','République démocratique du Congo','Congo, Democratic Rep. of the'),(179,'DO','République dominicaine','Dominican Republic'),(180,'CZ','République tchèque','Czech Republic'),(181,'RE','Réunion','Reunion'),(182,'RO','Roumanie','Romania'),(183,'GB','Royaume-Uni','Saint Pierre and Miquelon'),(184,'RU','Russie','Russia (Russian Federation)'),(185,'RW','Rwanda','Rwanda'),(186,'SN','Sénégal','Senegal'),(187,'EH','Sahara occidental','Western Sahara'),(188,'KN','Saint-Christophe-et-Niévès','Saint Kitts and Nevis'),(189,'SM','Saint-Marin','San Marino'),(190,'PM','Saint-Pierre-et-Miquelon','Saint Pierre and Miquelon'),(191,'VA','Saint-Siège ','Vatican City State (Holy See)'),(192,'VC','Saint-Vincent-et-les-Grenadines','Saint Vincent and the Grenadines'),(193,'SH','Sainte-Hélène','Saint Helena'),(194,'LC','Sainte-Lucie','Saint Lucia'),(195,'SV','Salvador','El Salvador'),(196,'WS','Samoa','Samoa'),(197,'AS','Samoa américaines','American Samoa'),(198,'ST','Sao Tomé-et-Principe','Sao Tome and Principe'),(199,'SC','Seychelles','Seychelles'),(200,'SL','Sierra Leone','Sierra Leone'),(201,'SG','Singapour','Singapore'),(202,'SI','Slovénie','Slovenia'),(203,'SK','Slovaquie','Slovakia'),(204,'SO','Somalie','Somalia'),(205,'SD','Soudan','Sudan'),(206,'LK','Sri Lanka','Sri Lanka (ex-Ceilan)'),(207,'SE','Suède','Sweden'),(208,'CH','Suisse','Switzerland'),(209,'SR','Suriname','Suriname'),(210,'SZ','Swaziland','Swaziland'),(211,'SY','Syrie','Syrian Arab Republic'),(212,'TW','Taïwan','Taiwan'),(213,'TJ','Tadjikistan','Tajikistan'),(214,'TZ','Tanzanie','Tanzania, United Republic of'),(215,'TD','Tchad','Chad'),(216,'TF','Terres australes françaises','French Southern Territories - TF'),(217,'IO','Territoire britannique de l\'Océan Indien','British Indian Ocean Territory'),(218,'TH','Thaïlande','Thailand'),(219,'TL','Timor Oriental','Timor-Leste (East Timor)'),(220,'TG','Togo','Togo'),(221,'TK','Tokélaou','Tokelau'),(222,'TO','Tonga','Tonga'),(223,'TT','Trinité-et-Tobago','Trinidad & Tobago'),(224,'TN','Tunisie','Tunisia'),(225,'TM','Turkménistan','Turkmenistan'),(226,'TR','Turquie','Turkey'),(227,'TV','Tuvalu','Tuvalu'),(228,'UA','Ukraine','Ukraine'),(229,'UY','Uruguay','Uruguay'),(230,'VU','Vanuatu','Vanuatu'),(231,'VE','Venezuela','Venezuela'),(232,'VN','ViÃªt Nam','Viet Nam'),(233,'WF','Wallis-et-Futuna','Wallis and Futuna'),(234,'YE','Yémen','Yemen'),(235,'YU','Yougoslavie','Saint Pierre and Miquelon'),(236,'ZM','Zambie','Zambia'),(237,'ZW','Zimbabwe','Zimbabwe'),(238,'MK','ex-République yougoslave de Macédoine','Macedonia, TFYR');
/*!40000 ALTER TABLE `country` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `debut_carriere`
--

DROP TABLE IF EXISTS `debut_carriere`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `debut_carriere` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `annee` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `debut_carriere`
--

LOCK TABLES `debut_carriere` WRITE;
/*!40000 ALTER TABLE `debut_carriere` DISABLE KEYS */;
INSERT INTO `debut_carriere` VALUES (7,'1995'),(8,'1996'),(9,'1997'),(10,'1998'),(11,'1999'),(12,'2000'),(13,'2001'),(14,'2002'),(15,'2003'),(16,'2004'),(17,'2005'),(18,'2006'),(19,'2007'),(20,'2008'),(21,'2009'),(22,'2010'),(23,'2011'),(24,'2012'),(25,'2013'),(26,'2014');
/*!40000 ALTER TABLE `debut_carriere` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `direction`
--

DROP TABLE IF EXISTS `direction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `direction` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_candidat` int(11) DEFAULT NULL,
  `date_entretien` date NOT NULL,
  `id_operationnel` int(11) unsigned NOT NULL,
  `verdict` enum('Oui','Non','Neutre') COLLATE utf8_unicode_ci NOT NULL,
  `motifs_refus` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_decision` date DEFAULT NULL,
  `salaire_propose` int(11) DEFAULT NULL,
  `commentaire` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `identretien_operationnel_user_idx` (`id_candidat`),
  KEY `id_operationnel` (`id_operationnel`),
  CONSTRAINT `FK_direction_operationnel` FOREIGN KEY (`id_operationnel`) REFERENCES `auth_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `identretien_operationnel_user0` FOREIGN KEY (`id_candidat`) REFERENCES `candidat` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `direction`
--

LOCK TABLES `direction` WRITE;
/*!40000 ALTER TABLE `direction` DISABLE KEYS */;
/*!40000 ALTER TABLE `direction` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `domaine`
--

DROP TABLE IF EXISTS `domaine`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `domaine` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `domaine` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `domaine`
--

LOCK TABLES `domaine` WRITE;
/*!40000 ALTER TABLE `domaine` DISABLE KEYS */;
INSERT INTO `domaine` VALUES (3,'BI'),(4,'Web'),(5,'Search'),(6,'eCommerce'),(7,'Sale'),(8,'SI'),(9,'Management'),(10,'BI/Web'),(11,'BioPharma'),(12,'Développement'),(13,'QA'),(14,'IT');
/*!40000 ALTER TABLE `domaine` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ecole`
--

DROP TABLE IF EXISTS `ecole`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ecole` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ecole` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ecole`
--

LOCK TABLES `ecole` WRITE;
/*!40000 ALTER TABLE `ecole` DISABLE KEYS */;
INSERT INTO `ecole` VALUES (6,'ENSI'),(7,'ESPRIT'),(8,'ISI'),(9,'ENIT'),(10,'FST'),(11,'ISAMM'),(12,'6'),(13,'ecole'),(14,'INSAT'),(15,'BTS Assurances'),(16,'ESSTT'),(17,'ESTI'),(18,'Ecole Polytechnique de Tunisie'),(19,'ISIT’COM'),(20,'ISG Tunis'),(21,'IAE de Tours (FRANCE)'),(22,'ISGI Sfax'),(23,'ISET Nabeul');
/*!40000 ALTER TABLE `ecole` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `etudes`
--

DROP TABLE IF EXISTS `etudes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `etudes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `etude` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `etudes`
--

LOCK TABLES `etudes` WRITE;
/*!40000 ALTER TABLE `etudes` DISABLE KEYS */;
INSERT INTO `etudes` VALUES (5,'Bac +3 Lisence'),(6,'Bac +4 Maitrise'),(7,'BAC +5'),(8,'Master'),(9,'DEA');
/*!40000 ALTER TABLE `etudes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `events`
--

DROP TABLE IF EXISTS `events`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `events` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) unsigned NOT NULL,
  `event` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `start` datetime NOT NULL,
  `end` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_user` (`id_user`),
  CONSTRAINT `id_auuth_user` FOREIGN KEY (`id_user`) REFERENCES `auth_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `events`
--

LOCK TABLES `events` WRITE;
/*!40000 ALTER TABLE `events` DISABLE KEYS */;
INSERT INTO `events` VALUES (1,3,'Entretien avec le candidat Sahbi GARGOURI ','2014-05-20 11:00:00','2014-05-20 11:00:00'),(3,7,'Entretien avec le candidat Ghazi  KERFAHI','2014-05-26 16:00:00','2014-05-26 16:00:00'),(5,11,'Entretien avec le candidat Ghazi  KERFAHI','2014-05-26 17:15:00','2014-05-26 18:00:00'),(6,5,'Entretien avec le candidat Ghazi  KERFAHI','2014-05-26 16:15:00','2014-05-26 16:15:00'),(7,11,'Entretien avec le candidat  Nizar BOUDAYA','2014-05-26 16:15:00','2014-05-26 16:15:00'),(8,5,'Entretien avec le candidat Ghazi  KERFAHI','2014-05-26 17:15:00','2014-05-26 17:15:00'),(9,7,'Entretien avec le candidat ABIR ZITOUNI ','2014-06-02 15:00:00','2014-05-27 15:30:00'),(10,5,'Entretien avec le candidat ABIR ZITOUNI ','2014-05-27 11:00:00','2014-05-27 11:00:00'),(11,11,'Entretien avec le candidat ABIR ZITOUNI ','2014-05-27 11:00:00','2014-05-27 11:00:00'),(12,7,'Entretien avec le candidat Mohamed  RAIS','2014-06-03 16:00:00','2014-05-27 16:30:00'),(13,5,'Entretien avec le candidat Mohamed  RAIS','2014-05-27 16:00:00','2014-05-27 16:00:00'),(14,11,'Entretien avec le candidat Mohamed  RAIS','2014-05-27 16:00:00','2014-05-27 16:00:00'),(15,7,'Entretien avec le candidat Chaouki CHALGHOUMI','2014-05-26 11:00:00','2014-05-26 11:00:00'),(16,7,'Entretien avec le candidat  Mohamed Zyed MEKKI','2014-05-30 15:00:00','2014-05-30 15:30:00'),(17,7,'Entretien avec le candidat Elyes  JABRANI','2014-05-29 15:00:00','2014-05-29 15:00:00'),(18,7,'Entretien Seif El Islam KADDECHI','2014-06-02 11:00:00','2014-06-02 11:30:00'),(19,7,'Entretien Skype Achref BEN YOUNES','2014-05-29 17:40:00','2014-05-29 18:00:00'),(21,7,'Entretien Ismail YOUSSFI','2014-05-30 10:00:00','2014-05-30 10:30:00');
/*!40000 ALTER TABLE `events` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `experience`
--

DROP TABLE IF EXISTS `experience`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `experience` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `min` int(11) DEFAULT NULL,
  `max` int(11) DEFAULT NULL,
  `experience` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `experience`
--

LOCK TABLES `experience` WRITE;
/*!40000 ALTER TABLE `experience` DISABLE KEYS */;
INSERT INTO `experience` VALUES (4,0,1,'JD 0-1 an'),(5,1,2,'Junior 1-2 ans'),(6,2,5,'Medium 2-5 ans'),(7,5,NULL,'Senior + 5 ans'),(8,NULL,NULL,'Stagiaire');
/*!40000 ALTER TABLE `experience` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `holidays`
--

DROP TABLE IF EXISTS `holidays`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `holidays` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` date NOT NULL,
  `title` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `desc` varchar(10) COLLATE utf8_unicode_ci DEFAULT 'holiday',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `holidays`
--

LOCK TABLES `holidays` WRITE;
/*!40000 ALTER TABLE `holidays` DISABLE KEYS */;
INSERT INTO `holidays` VALUES (1,'2014-05-01','Fête du Travail','holiday'),(2,'2014-01-14','Fête de la Révolution et de la Jeunesse','holiday'),(3,'2014-03-20','Fête de l\'Indépendance','holiday'),(4,'2014-01-01','Nouvel an','holiday'),(5,'2014-07-25','Fête de la République','holiday');
/*!40000 ALTER TABLE `holidays` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `media_duplicate`
--

DROP TABLE IF EXISTS `media_duplicate`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `media_duplicate` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `file_id` varchar(100) NOT NULL,
  `adapter` varchar(50) NOT NULL,
  `params` text NOT NULL,
  `dest` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `file_id` (`file_id`),
  CONSTRAINT `media_duplicate_ibfk_1` FOREIGN KEY (`file_id`) REFERENCES `media_file` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `media_duplicate`
--

LOCK TABLES `media_duplicate` WRITE;
/*!40000 ALTER TABLE `media_duplicate` DISABLE KEYS */;
/*!40000 ALTER TABLE `media_duplicate` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `media_file`
--

DROP TABLE IF EXISTS `media_file`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `media_file` (
  `id` varchar(100) NOT NULL,
  `file_id` varchar(32) DEFAULT NULL,
  `local_filename` varchar(255) DEFAULT NULL,
  `mime` varchar(255) NOT NULL,
  `filename` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `filesize` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `use_count` int(11) unsigned NOT NULL DEFAULT '0',
  `user_id` int(11) unsigned DEFAULT NULL,
  `proxy_model` varchar(150) DEFAULT NULL,
  `proxy_pk` int(11) unsigned DEFAULT NULL,
  `belong_model` varchar(150) DEFAULT NULL,
  `belong_pk` int(11) unsigned DEFAULT NULL,
  `description` text,
  `sha1` varchar(40) DEFAULT NULL,
  `delete_original` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `fk_file__user_id___user__id` (`user_id`),
  KEY `proxy_model` (`proxy_model`),
  KEY `proxy_pk` (`proxy_pk`),
  KEY `belong_model` (`belong_model`),
  KEY `belong_pk` (`belong_pk`),
  KEY `file_id` (`file_id`),
  KEY `local_filename` (`local_filename`),
  CONSTRAINT `fk_file__user_id___user__id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `media_file`
--

LOCK TABLES `media_file` WRITE;
/*!40000 ALTER TABLE `media_file` DISABLE KEYS */;
/*!40000 ALTER TABLE `media_file` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `media_image`
--

DROP TABLE IF EXISTS `media_image`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `media_image` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `width` int(11) unsigned NOT NULL,
  `height` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `media_image`
--

LOCK TABLES `media_image` WRITE;
/*!40000 ALTER TABLE `media_image` DISABLE KEYS */;
/*!40000 ALTER TABLE `media_image` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `media_multiupload_ticket`
--

DROP TABLE IF EXISTS `media_multiupload_ticket`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `media_multiupload_ticket` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ticket` varchar(32) NOT NULL,
  `expire` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `proxy_model_id` int(11) unsigned DEFAULT NULL,
  `proxy_pk` int(11) unsigned DEFAULT NULL,
  `form_class_model_id` int(11) unsigned NOT NULL,
  `element_name` varchar(255) NOT NULL,
  `values` text,
  PRIMARY KEY (`id`),
  KEY `proxy_model` (`proxy_model_id`),
  KEY `form_class_model` (`form_class_model_id`),
  KEY `proxy_pk` (`proxy_pk`),
  CONSTRAINT `media_multiupload_ticket_ibfk_1` FOREIGN KEY (`proxy_model_id`) REFERENCES `centurion_content_type` (`id`),
  CONSTRAINT `media_multiupload_ticket_ibfk_2` FOREIGN KEY (`form_class_model_id`) REFERENCES `centurion_content_type` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `media_multiupload_ticket`
--

LOCK TABLES `media_multiupload_ticket` WRITE;
/*!40000 ALTER TABLE `media_multiupload_ticket` DISABLE KEYS */;
/*!40000 ALTER TABLE `media_multiupload_ticket` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `media_video`
--

DROP TABLE IF EXISTS `media_video`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `media_video` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `width` int(11) unsigned NOT NULL,
  `height` int(11) unsigned NOT NULL,
  `duration` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `media_video`
--

LOCK TABLES `media_video` WRITE;
/*!40000 ALTER TABLE `media_video` DISABLE KEYS */;
/*!40000 ALTER TABLE `media_video` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `metier`
--

DROP TABLE IF EXISTS `metier`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `metier` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `metier` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `metier`
--

LOCK TABLES `metier` WRITE;
/*!40000 ALTER TABLE `metier` DISABLE KEYS */;
INSERT INTO `metier` VALUES (3,'Développement'),(4,'Consulting'),(5,'Aministration');
/*!40000 ALTER TABLE `metier` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `operation`
--

DROP TABLE IF EXISTS `operation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `operation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_candidat` int(11) DEFAULT NULL,
  `date_entretien` date NOT NULL,
  `id_operationnel` int(11) unsigned NOT NULL,
  `verdict` enum('Oui','Non','Neutre') COLLATE utf8_unicode_ci NOT NULL,
  `date_decision` date DEFAULT NULL,
  `points_positifs` text COLLATE utf8_unicode_ci,
  `points_negatifs` text COLLATE utf8_unicode_ci,
  `commentaire` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `identretien_operationnel_user_idx` (`id_candidat`),
  KEY `id_operationnel` (`id_operationnel`),
  CONSTRAINT `FK_operation_operationnel` FOREIGN KEY (`id_operationnel`) REFERENCES `auth_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `identretien_operationnel_user` FOREIGN KEY (`id_candidat`) REFERENCES `candidat` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `operation`
--

LOCK TABLES `operation` WRITE;
/*!40000 ALTER TABLE `operation` DISABLE KEYS */;
/*!40000 ALTER TABLE `operation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `operationnel`
--

DROP TABLE IF EXISTS `operationnel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `operationnel` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `operationnel` varchar(120) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `operationnel`
--

LOCK TABLES `operationnel` WRITE;
/*!40000 ALTER TABLE `operationnel` DISABLE KEYS */;
INSERT INTO `operationnel` VALUES (1,'Hatem Skik'),(2,'Zied Belhadj'),(3,'Mourad Benhamida'),(4,'Salah Elabidi');
/*!40000 ALTER TABLE `operationnel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `prequalif`
--

DROP TABLE IF EXISTS `prequalif`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `prequalif` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_operationnel` int(11) unsigned NOT NULL,
  `id_candidat` int(11) DEFAULT NULL,
  `date_entretien` date NOT NULL,
  `ponctualite` enum('Oui','Non') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Oui',
  `changer` text COLLATE utf8_unicode_ci,
  `recherche` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `infos_keyrus` text COLLATE utf8_unicode_ci,
  `info_societe_actuelle` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `piste` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mission_etranger` enum('Oui','Non') COLLATE utf8_unicode_ci NOT NULL,
  `id_mission_country` int(11) DEFAULT NULL,
  `duree_mission` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `visa` enum('Oui','Non') COLLATE utf8_unicode_ci NOT NULL,
  `date_exp_visa` date DEFAULT NULL,
  `id_test_technique` int(11) DEFAULT NULL,
  `note_test` int(11) DEFAULT NULL,
  `mobilite` enum('Oui','Non') COLLATE utf8_unicode_ci NOT NULL,
  `disponibilite` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_type_contrat` int(11) DEFAULT NULL,
  `preavis` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cin` int(8) DEFAULT NULL,
  `cin_date_livraison` date DEFAULT NULL,
  `salaire_actuel` int(11) DEFAULT NULL,
  `salaire_variable` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pretention_salariale_min` int(11) DEFAULT '0',
  `pretention_salariale_max` int(11) DEFAULT '0',
  `niveau_francais` enum('Notions','Moyen','Bon','Courant') COLLATE utf8_unicode_ci DEFAULT NULL,
  `niveau_anglais` enum('Notions','Moyen','Bon','Courant','Technique') COLLATE utf8_unicode_ci DEFAULT NULL,
  `adequation` enum('Oui','Non') COLLATE utf8_unicode_ci NOT NULL,
  `fluidite` enum('Bonne','Moyenne','Difficile') COLLATE utf8_unicode_ci DEFAULT NULL,
  `vocabulaire` enum('Faible','Correct','Riche') COLLATE utf8_unicode_ci DEFAULT NULL,
  `aisance` enum('Bonne','Moyenne','Difficile') COLLATE utf8_unicode_ci DEFAULT NULL,
  `ecoute` enum('Insuffisante','Modérée','Forte') COLLATE utf8_unicode_ci DEFAULT NULL,
  `motivation` enum('Insuffisante','Modérée','Forte') COLLATE utf8_unicode_ci DEFAULT NULL,
  `avis` enum('Favorable','Défavorable','Neutre') COLLATE utf8_unicode_ci NOT NULL,
  `commentaire` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `id_user_prequalif_idx` (`id_candidat`),
  KEY `id_mission_country` (`id_mission_country`),
  KEY `id_test_technique` (`id_test_technique`),
  KEY `id_type_contrat` (`id_type_contrat`),
  KEY `id_operationnel` (`id_operationnel`),
  CONSTRAINT `FK_Operationnel` FOREIGN KEY (`id_operationnel`) REFERENCES `auth_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `id_test_technique` FOREIGN KEY (`id_test_technique`) REFERENCES `test_technique` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `id_type_contrat` FOREIGN KEY (`id_type_contrat`) REFERENCES `type_contrat` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `id_user_prequalif` FOREIGN KEY (`id_candidat`) REFERENCES `candidat` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `prequalif`
--

LOCK TABLES `prequalif` WRITE;
/*!40000 ALTER TABLE `prequalif` DISABLE KEYS */;
INSERT INTO `prequalif` VALUES (1,7,119,'2014-05-16','Oui','A l’écoute du marché','Ouvert aux opportunités qui lui offrent l’environnement propice pour travailler avec les technologies innovantes.','Suite à notre démarche Linkedin','3B Entreprise : ensemble de sociétés / bureau d’études (il n’a pas su présenter la société et ses activités) ','il a refusé  la proposition d’une start up ( 2000dt/mois ) ','Non',NULL,NULL,'Non','2014-05-16',NULL,NULL,'Oui','2 mois',4,'2 mois',NULL,'2014-05-16',1500,NULL,2000,NULL,'Moyen','Notions','Non','Difficile','Faible','Difficile','Modérée','Insuffisante','Défavorable',NULL),(2,7,107,'2014-05-22','Oui',NULL,'Stage de PFE autour des technologies JAVA/JEE','Un groupe Facebook (PFE GL 5)a publié notre annonce Tanitjobs',NULL,'Excel Tech (tracking mobile) ','Non',NULL,NULL,'Non','2014-05-22',NULL,NULL,'Non','Fév 2014',NULL,'1 mois',NULL,'2014-05-22',NULL,NULL,0,NULL,'Moyen','Moyen','Oui','Bonne','Correct','Moyenne','Forte',NULL,'Favorable','je ne dirais pas un profil INSAT. Plutôt timide, pas de problème à mener une discussion mais du mal à bien expliquer les choses.'),(3,7,125,'2014-05-26','Oui',NULL,NULL,NULL,NULL,NULL,'Non',NULL,NULL,'Non','2014-05-26',NULL,NULL,'Non',NULL,NULL,'1 mois',NULL,'2014-05-26',NULL,NULL,0,NULL,NULL,NULL,'Oui',NULL,NULL,NULL,NULL,NULL,'Neutre','Désistement, il a signé avec une autre société.'),(4,7,132,'2014-05-26','Oui','cherche à évoluer : Création  et gestion du process qualité et gestion de projet, monotoring, Démarche qualité process/mise en place et standardisation des process, KPI','Cherche un poste similaire à celui qu\'elle occupe et trouver l\'environnement qui lui permettra de le développer',NULL,NULL,'Aucune','Non',NULL,NULL,'Non','2014-05-26',NULL,NULL,'Oui','2 mois',4,'3 mois',NULL,'2014-05-26',1400,'1400-2800',1400,NULL,'Courant','Bon','Oui','Bonne','Riche','Bonne','Forte','Forte','Favorable','ambitieuse, cherche à grimper les paliers. aime les challenges, cherche à développer le poste qu\'elle occupe '),(5,7,126,'2014-05-26','Oui','Pas de challenges qui permettront l\'évolution. Découvrir d’autres secteurs d’activités + Intérêt pour Keyrus','Consultant fonctionnel BI','Suite à une réunion dans les locaux Orange (Mehdi, Julie, Bruno)','Il est chef de service contrôle de gestion sous la direction financière','En recherche depuis Mars/ un entretien avec  Nel\'Armonia pour un poste au Maroc mais il n\'a pas accepté un poste de chef de projet technique Hypérion','Oui',NULL,'2 jours  pour une formation','Non','2014-05-26',NULL,NULL,'Oui','1 mois ',4,'2 mois',NULL,'2014-05-26',1930,'13 et 14ième + une prime variable ',2600,0,'Courant','Bon','Oui','Bonne','Riche','Bonne','Forte','Forte','Favorable','•	Conduite de projet BI Finance et Administration des outils SI Finance.\r\n•	Analyse et Conception des besoins fonctionnelles et rédaction des spécifications \r\n•	Planification des tâches et Planning de livraison.\r\n•	Contrôle de la qualité et Revue de code.\r\n•	Validation et déploiement.\r\n•	Administration et gestion des différentes applications de reporting et de montage de budget SAP BO.\r\nMise à jours dans les cubes Olap, gestion des bases de données, reporting SAP BO\r\n2009-2011 : Hypérion'),(6,7,124,'2014-05-26','Oui','Manque de stabilité. Pas de perspectives d\'évolution de carrière.\r\nIl cherche la stabilité + intégrer une grande société.','Développeur JAVAEE','Tanitjobs + il a entendu parler de Keyrus ','4 collaborateurs développeurs JAVAEE','En recherche depuis 2 mois. un entrtien fait aujourd\'hui chez l\'ISI pour un poste de Développeur JAVA','Non',NULL,NULL,'Non','2014-05-26',NULL,NULL,'Oui','1mois',3,'1 mois',NULL,'2014-05-26',1000,'0',1100,1200,'Moyen','Moyen','Oui','Moyenne','Correct','Moyenne','Modérée','Modérée','Neutre','Le CDD expire en septembre 2014'),(7,7,135,'2014-05-27','Oui','Le volet métier n’est pas intéressant chez Telnet + les tâches sont répétitives, pas d’évolution, pas de contact client…','Développeur JAVAEE','Tanitjobs',NULL,':   il a  commencé ses recherches depuis un mois. Il a eu une proposition de Owliance : 1400dt : il a refusé (c’est loin de chez lui)','Oui',NULL,'15 Jours','Non','2014-05-27',1,16,'Oui','1mois',4,'3 mois',NULL,'2014-05-27',1250,'0.5 à 1.5 du salaire',1400,1400,'Moyen','Bon','Oui','Moyenne','Faible','Difficile','Modérée','Forte','Neutre','Il fait beaucoup de fautes en parlant + un accent assez prononcé mais reste compréhensible. Dit avoir un bon niveau d’anglais.'),(8,7,131,'2014-05-29','Oui','Ambiance du groupe, il est le seul homme dans l\'équipe. L\'environnement du travail est très important, il doit se sentir à l\'aise pour être motivé (travaille jusqu\'à 00:00 voir 01:00) \r\nil se sent rejeter par ses collègues femmes.\r\nCherche à réaliser des missions à l\'étranger','Développeur JAVAEE',NULL,NULL,'Aucune','Non',NULL,NULL,'Non','2014-05-29',NULL,NULL,'Non','1mois',4,'1 mois',NULL,'2014-05-29',1400,'0',1800,2000,'Moyen',NULL,'Oui',NULL,NULL,NULL,NULL,NULL,'Défavorable',NULL),(9,7,129,'2014-05-29','Oui',NULL,NULL,'Hichem Ben said',NULL,NULL,'Non',NULL,NULL,'Non','2014-05-29',NULL,NULL,'Non','01/07/2014',NULL,'01/07/2014',NULL,'2014-05-29',NULL,NULL,0,NULL,'Courant','Bon','Oui','Bonne','Riche','Bonne','Forte',NULL,'Favorable','cherche un stage d\'été de 2 mois'),(10,7,134,'2014-05-30','Oui','Il a quitté Medianet en 2012: chez Midanet on ne respecte pas les normes de développement, ne travaille pas avec MVC  que du PHP 4/ un seul projet en Drupal (Tunisiana) / 1 développeur drupal a quitté et le 2ème va quitter bientôt.\r\nWezign : Chef de projet/ équipe (7:collaborateurs) :  pas vraiment un travail de développement plutôt des applications facebook + un peu de développement mobile\r\n','Développeur PHP ',NULL,'Wezign','Tanitweb : entretien pour lundi','Non',NULL,NULL,'Non','2014-05-30',NULL,NULL,'Oui','16/06/2014',4,'démissionnaire ',NULL,'2014-05-30',1700,'0',1800,2000,'Moyen','Technique','Oui','Moyenne','Correct','Moyenne','Forte','Modérée','Défavorable','Niveau de français très moyen + beaucoup de fautes. ');
/*!40000 ALTER TABLE `prequalif` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `prequalif_avantage`
--

DROP TABLE IF EXISTS `prequalif_avantage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `prequalif_avantage` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `prequalif_id` int(11) NOT NULL,
  `avantage_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `prequalif_id` (`prequalif_id`,`avantage_id`),
  KEY `avantage_id` (`avantage_id`),
  CONSTRAINT `prequalif_avantage` FOREIGN KEY (`avantage_id`) REFERENCES `avantage` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `prequalif_prequalif` FOREIGN KEY (`prequalif_id`) REFERENCES `prequalif` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `prequalif_avantage`
--

LOCK TABLES `prequalif_avantage` WRITE;
/*!40000 ALTER TABLE `prequalif_avantage` DISABLE KEYS */;
INSERT INTO `prequalif_avantage` VALUES (1,4,1),(2,5,1),(3,5,2),(4,5,4),(5,7,1),(6,7,2);
/*!40000 ALTER TABLE `prequalif_avantage` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `prequalif_country`
--

DROP TABLE IF EXISTS `prequalif_country`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `prequalif_country` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `prequalif_id` int(11) NOT NULL,
  `country_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `prequalif_id` (`prequalif_id`),
  KEY `country_id` (`country_id`),
  CONSTRAINT `FK_country_id` FOREIGN KEY (`country_id`) REFERENCES `country` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_prequalif_id` FOREIGN KEY (`prequalif_id`) REFERENCES `prequalif` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `prequalif_country`
--

LOCK TABLES `prequalif_country` WRITE;
/*!40000 ALTER TABLE `prequalif_country` DISABLE KEYS */;
INSERT INTO `prequalif_country` VALUES (1,5,67),(2,7,67);
/*!40000 ALTER TABLE `prequalif_country` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rate`
--

DROP TABLE IF EXISTS `rate`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rate` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_operationnel` int(10) unsigned NOT NULL,
  `candidat_id` int(11) NOT NULL,
  `value` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_operationnel` (`id_operationnel`),
  KEY `candidat_id` (`candidat_id`),
  CONSTRAINT `FK_candidat` FOREIGN KEY (`candidat_id`) REFERENCES `candidat` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_voting` FOREIGN KEY (`id_operationnel`) REFERENCES `auth_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rate`
--

LOCK TABLES `rate` WRITE;
/*!40000 ALTER TABLE `rate` DISABLE KEYS */;
INSERT INTO `rate` VALUES (1,3,99,1);
/*!40000 ALTER TABLE `rate` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `recommandations`
--

DROP TABLE IF EXISTS `recommandations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `recommandations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `recommandation` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `recommandations`
--

LOCK TABLES `recommandations` WRITE;
/*!40000 ALTER TABLE `recommandations` DISABLE KEYS */;
INSERT INTO `recommandations` VALUES (1,'A garder'),(2,'Présenter aux opérations');
/*!40000 ALTER TABLE `recommandations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sourcecv`
--

DROP TABLE IF EXISTS `sourcecv`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sourcecv` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `sourcecv` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sourcecv`
--

LOCK TABLES `sourcecv` WRITE;
/*!40000 ALTER TABLE `sourcecv` DISABLE KEYS */;
INSERT INTO `sourcecv` VALUES (3,'Linkedin'),(4,'Viadeo'),(5,'TanitJobs'),(6,'Cooptation'),(7,'Candidature spontanée'),(8,'CS'),(9,'Google'),(10,'Monster'),(11,'');
/*!40000 ALTER TABLE `sourcecv` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `technologie`
--

DROP TABLE IF EXISTS `technologie`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `technologie` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `technologie` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `technologie`
--

LOCK TABLES `technologie` WRITE;
/*!40000 ALTER TABLE `technologie` DISABLE KEYS */;
INSERT INTO `technologie` VALUES (4,'Hybris'),(5,'Exalead'),(6,'Drupal'),(7,'SAS '),(8,'Stat'),(9,'Java/J2ee'),(10,'BI '),(11,'Qlik'),(12,'Talend'),(13,'Tableau Software'),(14,'.Net'),(15,'MS BI'),(16,'Hyperion'),(17,'Business Object'),(18,'Python'),(19,'Django'),(20,'Oracle'),(24,'EPM'),(25,'QA'),(26,'SAP BO'),(27,'Prestashop'),(28,'Magento'),(29,'Joomla'),(30,'Dreamweaver cs4'),(31,'Zend'),(32,'PHP'),(33,'Linux'),(34,'Cisco');
/*!40000 ALTER TABLE `technologie` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `test_technique`
--

DROP TABLE IF EXISTS `test_technique`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `test_technique` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `test_technique` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `test_technique`
--

LOCK TABLES `test_technique` WRITE;
/*!40000 ALTER TABLE `test_technique` DISABLE KEYS */;
INSERT INTO `test_technique` VALUES (1,'Java'),(2,'Logique'),(3,'Php'),(4,'SAS'),(5,'STAT'),(6,'Anglais');
/*!40000 ALTER TABLE `test_technique` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `translation_language`
--

DROP TABLE IF EXISTS `translation_language`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `translation_language` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `locale` varchar(20) NOT NULL,
  `name` varchar(255) NOT NULL,
  `flag` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `translation_language`
--

LOCK TABLES `translation_language` WRITE;
/*!40000 ALTER TABLE `translation_language` DISABLE KEYS */;
INSERT INTO `translation_language` VALUES (1,'fr','Français','/layouts/backoffice/images/flags/fr.png'),(2,'en','English','/layouts/backoffice/images/flags/uk.png');
/*!40000 ALTER TABLE `translation_language` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `translation_tag`
--

DROP TABLE IF EXISTS `translation_tag`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `translation_tag` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `tag` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `translation_tag`
--

LOCK TABLES `translation_tag` WRITE;
/*!40000 ALTER TABLE `translation_tag` DISABLE KEYS */;
INSERT INTO `translation_tag` VALUES (1,'backoffice'),(2,'cms');
/*!40000 ALTER TABLE `translation_tag` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `translation_tag_uid`
--

DROP TABLE IF EXISTS `translation_tag_uid`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `translation_tag_uid` (
  `uid_id` int(11) unsigned NOT NULL,
  `tag_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`uid_id`,`tag_id`),
  KEY `FK_translation_tag_uid2` (`tag_id`),
  CONSTRAINT `FK_translation_tag_uid` FOREIGN KEY (`uid_id`) REFERENCES `translation_uid` (`id`),
  CONSTRAINT `FK_translation_tag_uid2` FOREIGN KEY (`tag_id`) REFERENCES `translation_tag` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `translation_tag_uid`
--

LOCK TABLES `translation_tag_uid` WRITE;
/*!40000 ALTER TABLE `translation_tag_uid` DISABLE KEYS */;
INSERT INTO `translation_tag_uid` VALUES (2,1),(3,1),(4,1),(6,1),(7,1),(10,1),(42,1),(43,1),(46,1),(47,1),(10,2),(46,2),(47,2);
/*!40000 ALTER TABLE `translation_tag_uid` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `translation_translation`
--

DROP TABLE IF EXISTS `translation_translation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `translation_translation` (
  `translation` text COLLATE utf8_bin NOT NULL,
  `uid_id` int(11) unsigned NOT NULL,
  `language_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`uid_id`,`language_id`),
  KEY `uid_id` (`uid_id`),
  KEY `language_id` (`language_id`),
  CONSTRAINT `translation_translation_ibfk_1` FOREIGN KEY (`uid_id`) REFERENCES `translation_uid` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `translation_translation_ibfk_2` FOREIGN KEY (`language_id`) REFERENCES `translation_language` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `translation_translation`
--

LOCK TABLES `translation_translation` WRITE;
/*!40000 ALTER TABLE `translation_translation` DISABLE KEYS */;
/*!40000 ALTER TABLE `translation_translation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `translation_uid`
--

DROP TABLE IF EXISTS `translation_uid`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `translation_uid` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `uid` text COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=240 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `translation_uid`
--

LOCK TABLES `translation_uid` WRITE;
/*!40000 ALTER TABLE `translation_uid` DISABLE KEYS */;
INSERT INTO `translation_uid` VALUES (1,'back'),(2,'Username'),(3,'Password'),(4,'Remember me'),(5,'Log in'),(6,'Log out'),(7,'View website'),(8,'Manage permissions'),(9,'Permissions'),(10,'Name'),(11,'Delete'),(12,'Are you sure? This operation can not be undone'),(13,'Description'),(14,'Users'),(15,'Groups'),(16,'Filters'),(17,'Submit'),(18,'Add a new %s'),(19,'<strong>%d-%d</strong> of <strong>%d</strong>'),(20,'Manage navigation'),(21,'navigation'),(22,'Language'),(23,'Title'),(24,'Is published'),(25,'At'),(26,'Actions'),(27,'Offline'),(28,'Online'),(29,'Edit'),(30,'Edit properties'),(31,'Label'),(32,'Module name'),(33,'Controller name'),(34,'Action name'),(35,'Params (json)'),(36,'Route name'),(37,'URI'),(38,'Visible?'),(39,'Stylesheet'),(40,'Proxy'),(41,'Translated from'),(42,'Save'),(43,'Save and add another'),(44,'Save and continue'),(45,'Manage translation'),(46,'View script'),(47,'Manage flatpage templates'),(48,'Edit flatpage template'),(49,'Created at'),(50,'Last login'),(51,'Is active'),(52,'Active'),(53,'Not active'),(54,'Status'),(55,'Yes'),(56,'No'),(57,'Activate'),(58,'Desactivate'),(59,'Manage users'),(60,'Email'),(61,'User parent'),(62,'Can be deleted'),(63,'Is staff'),(64,'Is super admin'),(65,'Special characters are not allowed'),(66,'Password confirmation'),(67,'Email confirmation'),(68,'Module:'),(69,'Controller:'),(70,'Ressource:'),(71,'Etudes'),(72,'Manage Etudes'),(73,'Edit Etude'),(74,'Etude'),(75,'Saving has been done.'),(76,'Ecole'),(77,'Ecoles'),(78,'Technologie'),(79,'Technologies'),(80,'Compétence'),(81,'Compétences'),(82,'Experience'),(83,'Experiences'),(84,'Poste'),(85,'Postes'),(86,'Metier'),(87,'Metiers'),(88,'Societe'),(89,'Société'),(90,'Sociétés'),(91,'Nationalité'),(92,'Natonalités'),(93,'Source CV'),(94,'Candidat'),(95,'Candidats'),(96,'Prénom'),(97,'Form hasn\'t been save. Maybe you have waiting to much time. Try again.'),(98,'Domaine'),(99,'Domaines'),(100,'Expériences'),(101,'Experiencess'),(102,'Expérience'),(103,'Métiers'),(104,'Métier'),(105,'Sources CV'),(106,'Civilité'),(107,'Nom'),(108,'Téléphone'),(109,'Résidence'),(110,'Début carrière'),(111,'Statut'),(112,'Société précédente'),(113,'Certifié'),(114,'Photo'),(115,'Commentaire'),(116,'Nickname'),(117,'Login'),(118,'user'),(119,'Type'),(120,'Nationalité Tunisienne'),(121,'Nationalité Française'),(122,'Compétences secondaires'),(123,'Certification'),(124,'CV'),(125,'Société Précédente'),(126,'Nationalités'),(127,'Comp. secondaires'),(128,'Commentaires'),(129,'Société actuelle'),(130,'Compétences principales'),(131,'An error occur when validating the form. See below.'),(132,'Reference text'),(133,'Translations'),(134,'Id'),(135,'Reference language'),(136,'Language to translate'),(137,'Tags'),(138,'Next'),(139,'Last'),(140,'Reference (%s)'),(141,'Translate'),(142,'Début Carrière'),(143,'Civilit&egrave;'),(144,'T&egrave;léphone'),(145,'Test Technique'),(146,'Tests Techniques'),(147,'Tests Technique'),(148,'Type Contrat'),(149,'Types Contrats'),(150,'Entretien Préqualif'),(151,'Avantages'),(152,'Avantage'),(153,'Date entretien'),(154,'Ponctualité'),(155,'Pourquoi changer?'),(156,'Poste recherché'),(157,'Infos Keyrus'),(158,'Infos société actuelle'),(159,'Piste en cours'),(160,'Missions réalisées à l’étranger ?'),(161,'Où ?'),(162,'Durée mission'),(163,'Visa'),(164,'Date Exp VISA'),(165,'Test technique'),(166,'Note test'),(167,'Mobilité internationale'),(168,'Disponibilité'),(169,'Type de contrat'),(170,'Durée du préavis'),(171,'CIN'),(172,'CIN Délivrée le'),(173,'Salaire actuel Fixe'),(174,'Variables'),(175,'Prétentions Salariales'),(176,'Niveau Français'),(177,'Niveau anglais'),(178,'Adéquation poste'),(179,'Fluidité du disours'),(180,'Vocabulaire/Syntaxe'),(181,'Aisance verbale'),(182,'Ecoute'),(183,'Motivation à changer de poste'),(184,'Avis'),(185,'Pays'),(186,'First'),(187,'Previous'),(188,'Operationnel'),(189,'Operationnels'),(190,'Entretien Opérationnel'),(191,'Opérationnel'),(192,'Verdict'),(193,'Date décision'),(194,'Motif refus'),(195,'Entretien Direction'),(196,'Salaire proposé'),(197,'<strong>%d</strong> of <strong>%d</strong>'),(198,'Entreprise'),(199,'Pré-Qual'),(200,'E.OP'),(201,'E.Dir'),(202,''),(203,'E.Op'),(204,'Mots clès'),(205,'Recherche par mots clés'),(206,'Dernier entretien'),(207,'From'),(208,'To'),(209,'All'),(210,'Mobilité'),(211,'Rechercher dans'),(212,'Salaire'),(213,'et'),(214,'Unauthorized access - '),(215,'Unauthorized action'),(216,'As a %s user, you don\'t have the permission to accomplish this action.'),(217,'Go back'),(218,'Téléphone 2'),(219,'Points positifs'),(220,'Points négatifs'),(221,'De'),(222,'à'),(223,'Recommandation'),(224,'Identifiant'),(225,'Nom de l\'événement'),(226,'Date de début'),(227,'Date de fin'),(228,'Recommandations'),(229,'Prétentions Salariales Min'),(230,'Prétentions Salariales Max'),(231,'Unauthorized action\nAs a %s user, you don\'t have the permission to accomplish this action.'),(232,'Pistes en cours'),(233,'Liked'),(234,'Titre'),(235,'Date'),(236,'Desc'),(237,'Jour férié'),(238,'Jours fériés'),(239,'Jours férié');
/*!40000 ALTER TABLE `translation_uid` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `type_contrat`
--

DROP TABLE IF EXISTS `type_contrat`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `type_contrat` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type_contrat` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `type_contrat`
--

LOCK TABLES `type_contrat` WRITE;
/*!40000 ALTER TABLE `type_contrat` DISABLE KEYS */;
INSERT INTO `type_contrat` VALUES (1,'SIVP1'),(2,'SIVP2'),(3,'CDD'),(4,'CDI'),(5,'Mission');
/*!40000 ALTER TABLE `type_contrat` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_profile`
--

DROP TABLE IF EXISTS `user_profile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_profile` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL,
  `nickname` varchar(50) DEFAULT NULL,
  `about` text,
  `website` varchar(150) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `avatar_id` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_profile__user_id___user__id` (`user_id`),
  KEY `fk_profile__avatar_id___file__id` (`avatar_id`),
  CONSTRAINT `fk_profile__avatar_id___file__id` FOREIGN KEY (`avatar_id`) REFERENCES `media_file` (`id`),
  CONSTRAINT `fk_profile__user_id___user__id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_profile`
--

LOCK TABLES `user_profile` WRITE;
/*!40000 ALTER TABLE `user_profile` DISABLE KEYS */;
INSERT INTO `user_profile` VALUES (2,1,'admin',NULL,NULL,'2014-02-12 08:08:02','2014-02-12 08:08:02',NULL),(3,4,'ZiedBelhadj',NULL,NULL,'2014-04-03 10:32:32','2014-04-03 10:32:32',NULL),(4,3,'HatemSkik',NULL,NULL,'2014-04-03 11:57:52','2014-04-03 11:57:52',NULL),(5,7,'AsmaAyari',NULL,NULL,'2014-04-18 17:43:24','2014-04-18 17:43:24',NULL),(6,8,'othmen.tahri',NULL,NULL,'2014-05-07 14:59:36','2014-05-07 14:59:36',NULL),(7,9,'MehdiSkik',NULL,NULL,'2014-05-08 15:29:36','2014-05-08 15:29:36',NULL),(8,6,'Mourad.Benhamida',NULL,NULL,'2014-05-16 15:41:47','2014-05-16 15:41:47',NULL),(9,10,'Talel.Kaabachi',NULL,NULL,'2014-05-16 15:56:48','2014-05-16 15:56:48',NULL),(10,11,'Ahmed.Sahli',NULL,NULL,'2014-05-21 11:25:16','2014-05-21 11:25:16',NULL),(11,5,'Salah.Elabidi',NULL,NULL,'2014-05-21 11:31:32','2014-05-21 11:31:32',NULL);
/*!40000 ALTER TABLE `user_profile` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-05-30 15:12:04
