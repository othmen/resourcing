$(document).ready(function() {
    var date = new Date();
    var d = date.getDate();
    var m = date.getMonth();
    var y = date.getFullYear();

    $('#profil-cv').tabs();
    //$("#prequalif-entretiens").tabs();
    $('input.date').datepicker({dateFormat: 'dd-mm-yy'});
    $('input.datepicker').datepicker({dateFormat: 'yy-mm-dd'});
    $('input.fromDate').datepicker({dateFormat: 'yy-mm-dd'});
    $('input.toDate').datepicker({dateFormat: 'yy-mm-dd'});
    $('#date_entretien').datepicker({dateFormat: 'yy-mm-dd'});
    
    $('.time-entretien').timepicker();
     $('.event-entretien').timepicker({
        minuteStep: 1,
        showSeconds: false,
        showMeridian: false,
        defaultTime: false
      });
//    $('#send-mail').click(function(){
//        //console.log($('#type_entretien').val());
//        if($('#type_entretien').val() == '' || $('#operationnel').val() == '' || $('#date_entretien').val() == '' ){
//            return false;
//        } else {
//            $('#send-mail').removeClass('disabled');
//            $('#send-mail').parent().removeClass('disabled');
//        }
//    });

    $(".planif-entretien").click(function() {
        $("#planif-entretien").dialog({
            resizable: true,
            minWidth: 500,
            minHeight: 350,
            modal: false,
            closeOnEscape: true,
            position: "center",
        });
    });
    
    var id_experience = $('#id_experience-input').val();
    if(id_experience != ''){
        $('#id_experience-input').removeAttr('disabled');
    }
    
    $('#filter-salaire-gt').removeClass("datepicker");
    $('#filter-salaire-gt').removeClass("hasDatepicker");
    $('#filter-salaire-lt').removeClass("datepicker");
    $('#filter-salaire-lt').removeClass("hasDatepicker");

    var data_select = $('#id_experience-input').html();
    $('#id_experience-input').html('<option value="" label=""></option>' + data_select);
    
    var countries_select = $('#countries-input').html();
    $('#countries-input').html('<option value="" label=""></option>' + countries_select);

    $('#form-candidat-button').click(function() {
        $('#form-candidat').parsley('validate');
    });
    $('#form-prequalif-button').click(function() {
        $('#form-prequalif').parsley('validate');
    });
    $('#form-operation-button').click(function() {
        $('#form-operation').parsley('validate');
    });
    $('#form-direction-button').click(function() {
        $('#form-direction').parsley('validate');
    });
    
    var mission = $('input[name=mission_etranger]:checked', '#form-prequalif').val();
    if(mission == "Non"){
        $('#countries-input').parent().parent().hide();
        $('#mission_etranger-input-Oui').parent().parent().addClass('float-none');
        $('#duree_mission-input').parent().parent().hide();
    }
    $('#mission_etranger-input-Non').click(function() {
        $(this).parent().parent().next('div.form-row').hide();
        $(this).parent().parent().addClass('float-none');
        $('#duree_mission-input').parent().parent().hide();
    });
    $('#mission_etranger-input-Oui').click(function() {
        $(this).parent().parent().removeClass('float-none');
        $(this).parent().parent().next('div.form-row').show();
        $('#duree_mission-input').parent().parent().show();
    });
    
    var certifie = $('input[name=certifie]:checked', '#form-candidat').val();
    if(certifie == "Non"){
        $('#certification-input').parent().parent().hide();
        $('#certifie-input-Non').parent().parent().addClass('float-none');
    }

    $('#certifie-input-Non').click(function() {
        $(this).parent().parent().next('div.form-row').hide();
        $(this).parent().parent().addClass('float-none');
    });
    $('#certifie-input-Oui').click(function() {
        $(this).parent().parent().removeClass('float-none');
        $(this).parent().parent().next('div.form-row').show();
    });
    
    var verdict = $('input[name=verdict]:checked', '#form-direction').val();
    if(verdict == "Oui"){
        $('#motifs_refus-input').parent().parent().hide();
    } else {
        $('#motifs_refus-input').parent().parent().show();
    }

    $('#verdict-input-Oui').click(function() {
        $('#motifs_refus-input').parent().parent().hide();
    });
    $('#verdict-input-Non').click(function() {
        $('#motifs_refus-input').parent().parent().show();
    });
    
   
    var visa = $('input[name=visa]:checked').val();
    if(visa == "Oui"){
        $('#date_exp_visa-input').parent().parent().parent().parent().show();
        $('#date_exp_visa-input').removeAttr('disabled');
    } else {
        $('#date_exp_visa-input').parent().parent().parent().parent().hide();
        $('#visa-input-Oui').parent().parent().addClass('float-none');
    }

    $('#visa-input-Non').click(function() {
        $(this).parent().parent().next('div.form-row').hide();
        $(this).parent().parent().addClass('float-none');
        $('#date_exp_visa-input').attr('disabled', 'disabled');
    });
    $('#visa-input-Oui').click(function() {
        $(this).parent().parent().removeClass('float-none');
        $(this).parent().parent().next('div.form-row').show();
        $('#date_exp_visa-input').removeAttr('disabled');
    });
    
    $('#remove-filters').click(function() {
        $('form#grid-filter-form').find("input:radio, input:checkbox").removeAttr("checked");
        $('form#grid-filter-form').find("input:text").val("");
        $('form#grid-filter-form').find("select").val("");
        return false;
    });
    
});


